/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export let Models = {
	/** @type {string[]} */
	extensionCodes: [],
	/** @type {Object.<string, Extension>} */
	extensions: {},
	/** @type {Extension[]} */
	extensionsArray: [],
	/** @type {Object.<string, CategoryModel>} */
	categories: {},
	/** @type {CategoryModel[]} */
	categoriesArray: [],
	/** @type {Object.<string, ScoreTileModel>} */
	scoreTiles: {},
	/** @type {ScoreTileModel[]} */
	scoreTilesArray: [],
	/** @type {Object.<string, AuthorModel>} */
	authors: {},
	/** @type {AuthorModel[]} */
	authorsArray: [],
	/** @type {Object.<string, CardModel>} */
	cards: {},
	/** @type {CardModel[]} */
	cardsArray: [],
	/**
	 * @param {string} code
	 * @returns {CategoryModel|null}
	 */
	getCategoryByCode(code) {
		if (this.categories[code]) {
			return this.categories[code];
		}
		console.error('[Models.getCategoryByCode] Unknown category model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {ScoreTileModel|null}
	 */
	getScoreTileByCode(code) {
		if (this.scoreTiles[code]) {
			return this.scoreTiles[code];
		}
		console.error('[Models.getScoreTileByCode] Unknown score tile model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {AuthorModel|null}
	 */
	getAuthorByCode(code) {
		if (this.authors[code]) {
			return this.authors[code];
		}
		console.error('[Models.getAuthorsByCode] Unknown author model', code);
		return null;
	},
	/**
	 * @param {string} code
	 * @returns {CardModel|null}
	 */
	getCardByCode(code) {
		if (this.cards[code]) {
			return this.cards[code];
		}
		console.error('[Models.getCardByCode] Unknown card model', code);
		return null;
	},
	/**
	 * @param {string[]} codes
	 * @returns {string[]}
	 */
	sortCategoryCodes(codes) {
		codes.sort((a, b) => {
			if (a === b) {
				return 0;
			}
			let categoryA = this.getCategoryByCode(a);
			let categoryB = this.getCategoryByCode(b);
			if (categoryA.type < categoryB.type) {
				return -1;
			}
			if (categoryA.type > categoryB.type) {
				return 1;
			}
			if (categoryA.code < categoryB.code) {
				return -1;
			}
			return 1;
		});
		return codes;
	}
};

/**
 * Set a tile to show the detail modal.
 */
Models.Details = {
	/** @type {CardModel|null} */
	card: null,
	/** @type {ScoreTileModel|null} */
	scoreTile: null
};