/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils } from "../core/tools/array.mjs";

/**
 * @typedef {Object} SerializedHand
 * @property {string[]} codes
 */

export class Hand {
	/**
	 * @param {string[]} codes
	 */
	constructor(codes) {
		this.codes = codes;
	}

	clear() {
		ArrayUtils.clear(this.codes);
	}

	/**
	 * @param {string} code
	 */
	addCode(code) {
		this.codes.push(code);
	}

	/**
	 * @param {string[]} codes
	 */
	addCodes(codes) {
		codes.forEach((code) => {
			this.addCode(code);
		});
	}

	/**
	 * @param {string} code
	 */
	removeCode(code) {
		ArrayUtils.remove(this.codes, code);
	}

	/**
	 * @param {number} index
	 * @returns {string|null}
	 */
	getCode(index) {
		return this.codes[index] || null;
	}

	/**
	 * @returns {SerializedHand}
	 */
	get data() {
		let data = {
			codes: []
		};

		this.codes.forEach((code) => {
			if (!code) {
				console.warn('[Hand.data] Unexpected empty item in codes!');
				return;
			}
			data.codes.push(code);
		});

		return data;
	}

	/**
	 * @param {SerializedHand} data
	 */
	set data(data) {
		ArrayUtils.clear(this.codes);

		data.codes.forEach((code) => {
			this.codes.push(code);
		});
	}
}