/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { Connection } from '../core/tools/connection.mjs';
import { System } from "../core/tools/system.mjs";
import { ArrayUtils } from '../core/tools/array.mjs';
import { LocalStorage } from '../core/tools/local-storage.mjs';
import { I18n } from '../core/tools/i18n.mjs';
import { Alerts } from '../core/tools/alerts.mjs';
import { PlayerColor, PlayerColors } from '../core/structures/player.mjs';
import { Models } from './models.mjs';
import { Player } from './players.mjs';
import { Sounds } from "./sounds.mjs";
import { Preferences } from "./preferences.mjs";

let document = window.document;

/**
 * @typedef {Object} ExtensionData
 * @property {string} code
 * @property {Extension} extension
 * @property {CardData[]} cards
 * @property {ScoreTileData[]} scoreTiles
 */

/**
 * @typedef {Object} CardData
 * @property {string} code
 * @property {CardModel} model
 * @property {boolean} enabled
 * @property {boolean} selected
 * @property {number} quantity
 */

/**
 * @typedef {Object} ScoreTileData
 * @property {string} code
 * @property {ScoreTileModel} model
 * @property {boolean} enabled
 * @property {boolean} selected
 * @property {number} quantity
 */

/**
 * @typedef {Object} CategoryData
 * @property {string} code
 * @property {CategoryModel} model
 * @property {boolean} enabled
 * @property {CardData[]} cards
 * @property {ScoreTileData[]} scoreTiles
 */

let increment = 1;

export let Setup = {
	/** @type {boolean} */
	initialized: false,
	/** @type {boolean} */
	waitingForServerInit: false,
	/** @type {Player[]} */
	players: [],
	/** @type {string} */
	poolType: 'selection',
	/** @type {number} */
	poolSize: 50,
	/** @type {string} */
	tilePoolType: 'selection',
	/** @type {number} */
	tilePoolSize: 50,
	/** @type {ExtensionData[]} */
	selection: [],
	/** @type {CategoryData[]} */
	categories: [],
	/** @type {OptionalRules} */
	optionalRules: {},
	collapse: {
		board: true,
		neutralPawns: true,
		tokens: true
	},
	init(onlyDefinitions) {
		if (this.initialized) {
			return;
		}
		this.initSelection();
		if (!onlyDefinitions) {
			this.addPlayer(Connection.id);
			if (Connection.id) {
				this.loadPlayerData();
			}
		}
		else {
			this.waitingForServerInit = true;
		}
		this.initialized = true;
	},
	/**
	 * @param {string=} peerId
	 */
	addPlayer(peerId) {
		let name = I18n.trans('c.setup.player') + ' ' + increment++;
		let color = null;
		for (let key in PlayerColors) {
			if (PlayerColors.hasOwnProperty(key) && PlayerColors[key].isAvailable(this.players)) {
				color = PlayerColors[key];
				break;
			}
		}
		if (name && color) {
			this.players.push(new Player(name, color, peerId));
		}
	},
	/**
	 * @param {Player} player
	 */
	removePlayer(player) {
		ArrayUtils.remove(this.players, player);
		if (player.peerId && Connection.instance.mode === 'server') {
			Connection.sendMessage('RemovePlayer', player.data);
			// If the connection is closed immediately, the message is not sent.
			setTimeout(() => {
				Connection.instance.closeConnection(player.peerId);
			}, 100);
		}
	},
	/**
	 * @param {string} peerId
	 * @returns {Player|null}
	 */
	getPlayerByPeerId(peerId) {
		let result = null;
		this.players.forEach((player) => {
			if (player.peerId === peerId) {
				result = player;
			}
		});
		return result;
	},
	initSelection() {
		Models.extensionsArray.forEach((extension) => {
			let item = {
				code: extension.code,
				extension: extension,
				cards: [],
				scoreTiles: []
			};

			extension.categories.forEach((category) => {
				this.categories.push({
					code: category.code,
					model: category,
					cards: [],
					scoreTiles: [],
					enabled: true
				});
			});

			extension.cards.forEach((card) => {
				/** @var {CardData} cardData */
				let cardData = {
					code: card.code,
					model: card,
					enabled: true,
					selected: true,
					quantity: 1
				};
				item.cards.push(cardData);

				this.categories.forEach((categoryItem) => {
					if (card.hasCategory(categoryItem.code)) {
						categoryItem.cards.push(cardData);
					}
				});
			});

			extension.scoreTiles.forEach((tile) => {
				/** @var {ScoreTileData} scoreTileData */
				let scoreTileData = {
					code: tile.code,
					model: tile,
					enabled: true,
					selected: true,
					quantity: 1
				};
				item.scoreTiles.push(scoreTileData);

				this.categories.forEach((categoryItem) => {
					if (tile.refersCategory(categoryItem.code)) {
						categoryItem.scoreTiles.push(scoreTileData);
					}
				});
			});

			this.selection.push(item);
		});
	},
	/**
	 * @param {CategoryData} category
	 */
	refreshCategory(category) {
		category.cards.forEach((cardData) => {
			let enabled = true;
			this.categories.forEach((categoryItem) => {
				if (cardData.model.hasCategory(categoryItem.code)) {
					enabled = enabled && categoryItem.enabled;
				}
			});
			cardData.enabled = enabled;
			if (!enabled) {
				cardData.selected = false;
			}
		});

		category.scoreTiles.forEach((scoreTileData) => {
			let enabled = true;
			this.categories.forEach((categoryItem) => {
				if (scoreTileData.model.refersCategory(categoryItem.code)) {
					enabled = enabled && categoryItem.enabled;
				}
			});
			scoreTileData.enabled = enabled;
			if (!enabled) {
				scoreTileData.selected = false;
			}
		});
	},
	//region Card pool selection.
	selectAllCards() {
		this.selection.forEach((extension) => {
			extension.cards.forEach((item) => {
				item.selected = true;
			});
		});
	},
	deselectAllCards() {
		this.selection.forEach((extension) => {
			extension.cards.forEach((item) => {
				item.selected = false;
			});
		});
	},
	/**
	 * @param {Object} extension
	 */
	selectExtensionCards(extension) {
		extension.cards.forEach((item) => {
			item.selected = true;
		});
	},
	/**
	 * @param {Object} extension
	 */
	deselectExtensionCards(extension) {
		extension.cards.forEach((item) => {
			item.selected = false;
		});
	},
	/**
	 * @param {CategoryModel} categoryModel
	 */
	selectCardsWithCategory(categoryModel) {
		this.selection.forEach((extension) => {
			extension.cards.forEach((item) => {
				if (item.model.hasCategory(categoryModel.code)) {
					item.selected = item.enabled;
				}
			});
		});
	},
	/**
	 * @param {CategoryModel} categoryModel
	 */
	deselectCardsWithCategory(categoryModel) {
		this.selection.forEach((extension) => {
			extension.cards.forEach((item) => {
				if (item.model.hasCategory(categoryModel.code)) {
					item.selected = false;
				}
			});
		});
	},
	//endregion
	//region Score tiles pool selection.
	selectAllScoreTiles() {
		this.selection.forEach((extension) => {
			extension.scoreTiles.forEach((item) => {
				item.selected = true;
			});
		});
	},
	deselectAllScoreTiles() {
		this.selection.forEach((extension) => {
			extension.scoreTiles.forEach((item) => {
				item.selected = false;
			});
		});
	},
	/**
	 * @param {Object} extension
	 */
	selectExtensionScoreTiles(extension) {
		extension.scoreTiles.forEach((item) => {
			item.selected = true;
		});
	},
	/**
	 * @param {Object} extension
	 */
	deselectExtensionScoreTiles(extension) {
		extension.scoreTiles.forEach((item) => {
			item.selected = false;
		});
	},
	/**
	 * @param {CategoryModel} categoryModel
	 */
	selectScoreTilesWithCategory(categoryModel) {
		this.selection.forEach((extension) => {
			extension.scoreTiles.forEach((item) => {
				if (item.model.refersCategory(categoryModel.code)) {
					item.selected = item.enabled;
				}
			});
		});
	},
	/**
	 * @param {CategoryModel} categoryModel
	 */
	deselectScoreTilesWithCategory(categoryModel) {
		this.selection.forEach((extension) => {
			extension.scoreTiles.forEach((item) => {
				if (item.model.refersCategory(categoryModel.code)) {
					item.selected = false;
				}
			});
		});
	},
	//endregion
	/**
	 * @param {string} code
	 * @returns {CategoryData|null}
	 */
	getCategory(code) {
		let result = null;
		this.categories.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {string} code
	 * @returns {ExtensionData|null}
	 */
	getExtension(code) {
		let result = null;
		this.selection.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {CardData|null}
	 */
	getCardData(extension, code) {
		let result = null;
		extension.cards.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @returns {string[]}
	 */
	get selectedCardModels() {
		let selectedModels = [];
		this.selection.forEach((item) => {
			item.cards.forEach((cardItem) => {
				if (cardItem.selected) {
					for (let i = 0; i < cardItem.quantity; i++) {
						selectedModels.push(cardItem.model.code);
					}
				}
			});
		});
		return selectedModels;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {ScoreTileData|null}
	 */
	getScoreTileData(extension, code) {
		let result = null;
		extension.scoreTiles.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @returns {string[]}
	 */
	get selectedScoreTileModels() {
		let selectedModels = [];
		this.selection.forEach((item) => {
			item.scoreTiles.forEach((scoreTileItem) => {
				if (scoreTileItem.selected) {
					for (let i = 0; i < scoreTileItem.quantity; i++) {
						selectedModels.push(scoreTileItem.model.code);
					}
				}
			});
		});
		return selectedModels;
	},
	/**
	 * @returns {boolean}
	 */
	get isValid() {
		let isValid = this.players.length > 1;

		let names = [];
		this.players.forEach((player) => {
			if (!player.name || !player.color || ArrayUtils.contains(names, player.name)) {
				isValid = false;
			}
			else {
				names.push(player.name);
			}
		});

		return !!(isValid && this.poolSize > 1 && this.selectedCardModels.length);
	},
	/**
	 * @returns {Object}
	 */
	toData() {
		let data = {
			poolSize: this.poolSize,
			poolType: this.poolType,
			players: [],
			categories: [],
			selection: []
		};

		this.players.forEach((player) => {
			data.players.push({
				name: player.name,
				color: player.color.code,
				peerId: player.peerId
			});
		});

		this.categories.forEach((category) => {
			data.categories.push({
				code: category.code,
				enabled: category.enabled
			});
		});

		this.selection.forEach((item) => {
			let itemData = {
				code: item.code,
				cards: [],
				scoreTiles: []
			};

			item.cards.forEach((cardItem) => {
				itemData.cards.push({
					code: cardItem.code,
					selected: cardItem.selected,
					quantity: cardItem.quantity
				});
			});

			item.scoreTiles.forEach((scoreTileItem) => {
				itemData.scoreTiles.push({
					code: scoreTileItem.code,
					selected: scoreTileItem.selected,
					quantity: scoreTileItem.quantity
				});
			});

			data.selection.push(itemData);
		});

		data.optionalRules = {};

		return data;
	},
	/**
	 * @param {Object|null} data
	 */
	fromData(data) {
		if (!data) {
			return;
		}

		if (data.hasOwnProperty('poolSize')) {
			this.poolSize = data.poolSize;
		}

		if (data.hasOwnProperty('poolType')) {
			this.poolType = data.poolType;
		}

		if (data.hasOwnProperty('players')) {
			ArrayUtils.clear(Setup.players);
			data.players.forEach((item) => {
				this.players.push(new Player(item.name, PlayerColors[item.color], item.peerId));
			});
		}

		if (data.hasOwnProperty('categories')) {
			data.categories.forEach((item) => {
				let category = this.getCategory(item.code);
				if (category) {
					category.enabled = item.enabled;
				}
			});
		}

		if (data.hasOwnProperty('selection')) {
			data.selection.forEach((extensionItem) => {
				let extensionData = this.getExtension(extensionItem.code);
				if (extensionData) {
					(extensionItem.cards || []).forEach((cardItem) => {
						let cardData = this.getCardData(extensionData, cardItem.code);
						if (cardData) {
							cardData.selected = cardItem.selected;
							if (cardItem.selected && cardItem.hasOwnProperty('quantity')) {
								cardData.quantity = cardItem.quantity;
							}
						}
					});

					(extensionItem.scoreTiles || []).forEach((scoreTileItem) => {
						let scoreTile = this.getScoreTileData(extensionData, scoreTileItem.code);
						if (scoreTile) {
							scoreTile.selected = scoreTileItem.selected;
							if (scoreTileItem.selected && scoreTileItem.hasOwnProperty('quantity')) {
								scoreTile.quantity = scoreTileItem.quantity;
							}
						}
					});
				}
			});
		}

		if (data.optionalRules) {
		}

		if (this.waitingForServerInit) {
			this.waitingForServerInit = false;
			this.loadPlayerData();
		}
	},
	/**
	 * @returns {boolean}
	 */
	hasLast() {
		return LocalStorage.isset('lastGameSetup');
	},
	saveLast() {
		let data = this.toData();
		delete data.players;
		LocalStorage.setObject('lastGameSetup', data);
	},
	loadLast() {
		this.fromData(LocalStorage.getObject('lastGameSetup'));
	},
	/**
	 * @param {Player} player
	 */
	savePlayerData(player) {
		LocalStorage.setObject('lastPlayerData', { name: player.name, color: player.color.code });
		alert(I18n.trans('c.setup.player_data_saved'));
	},
	loadPlayerData() {
		let playerData = LocalStorage.getObject('lastPlayerData');
		if (playerData && playerData.name) {
			let player = this.getPlayerByPeerId(Connection.id);
			if (player) {
				let name = playerData.name;
				let nameAvailable = true;
				this.players.forEach((item) => {
					if (item.peerId !== player.peerId && item.name === name) {
						nameAvailable = false;
					}
				});
				if (nameAvailable) {
					player.name = name;
				}
				else {
					let options = {
						substitutions: { name: name },
						icon: 'images/icons/conflict-same.svg'
					};
					if (Preferences.getInstance().playSounds) {
						options.sound = Sounds.newAlert;
					}
					Alerts.warning('c.setup.alert_name_already_used', options);
				}

				let color = PlayerColor.getByCode(playerData.color);
				if (color) {
					let colorAvailable = true;
					this.players.forEach((item) => {
						if (item.peerId !== player.peerId && item.color.code === color.code) {
							colorAvailable = false;
						}
					});
					if (colorAvailable) {
						player.color = color;
					}
					else {
						let options = {
							substitutions: { name: color.name },
							icon: 'images/icons/conflict-same.svg'
						};
						if (Preferences.getInstance().playSounds) {
							options.sound = Sounds.newAlert;
						}
						Alerts.warning('c.setup.alert_color_already_used', options);
					}
				}
			}
		}
	}
};

/**
 * @param {Function} callback
 */
function callIfInSetup(callback) {
	if (System.router.currentRoute.path === '/setup') {
		callback();
	}
	else {
		console.log('Not in setup, ignore the listener.');
	}
}

//region Event listeners.
document.addEventListener('coreConnectionNewClient', (event) => {
	callIfInSetup(() => {
		Setup.addPlayer(event.detail.peerId);
		Connection.sendMessage('SetupData', Setup.toData());

		setTimeout(() => {
			Setup.players.forEach((player) => {
				if (player.peerId === event.detail.peerId) {
					let options = {
						substitutions: { name: player.name },
						icon: 'images/icons/new-player.svg'
					};

					if (Preferences.getInstance().playSounds) {
						options.sound = Sounds.newAlert;
					}
					Alerts.info('c.setup.alert_player_joined_the_game', options);
				}
			});
		}, 3000);
	});
});

document.addEventListener('coreConnectionSetupData', (event) => {
	callIfInSetup(() => {
		let player = Setup.getPlayerByPeerId(Connection.id);
		if (player) {
			event.detail.data.players.forEach((playerData) => {
				if (playerData.peerId === Connection.id) {
					playerData.name = player.name;
					playerData.color = player.color.code;
				}
			});
		}
		Setup.fromData(event.detail.data);
	});
});

document.addEventListener('coreConnectionSetupUpdatePlayer', (event) => {
	callIfInSetup(() => {
		let player = Setup.getPlayerByPeerId(event.detail.data.peerId);
		if (player) {
			player.name = event.detail.data.name;
			player.color = PlayerColor.getByCode(event.detail.data.color);
		}
	});
});
//endregion