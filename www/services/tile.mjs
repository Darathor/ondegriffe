/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @typedef {Object} SerializedTile
 * @property {string} code
 */

export class Tile {
	/**
	 * @param {SerializedTile} data
	 * @return {Tile}
	 */
	static fromData(data) {
		let tile = new this(data.code);
		tile.data = data;
		return tile;
	}

	/**
	 * @param {string} code
	 */
	constructor(code) {
		this.code = code;
	}

	/**
	 * @returns {SerializedTile}
	 */
	get data() {
		return {
			code: this.code
		};
	}

	/**
	 * @param {SerializedTile} data
	 */
	set data(data) {
		this.code = data.code;
	}
}