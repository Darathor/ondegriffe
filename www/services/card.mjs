/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @typedef {Object} SerializedCard
 * @property {string} code
 */

export class Card {
	/**
	 * @param {SerializedCard} data
	 * @return {Card}
	 */
	static fromData(data) {
		let card = new this(data.code);
		card.data = data;
		return card;
	}

	/**
	 * @param {string} code
	 */
	constructor(code) {
		this.code = code;
	}

	/**
	 * @returns {SerializedCard}
	 */
	get data() {
		return {
			code: this.code
		};
	}

	/**
	 * @param {SerializedCard} data
	 */
	set data(data) {
		this.code = data.code;
	}
}