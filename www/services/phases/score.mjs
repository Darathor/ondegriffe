/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Phase } from "../phase.mjs";
import { Strategies } from "../strategies.mjs";
import { TimelineItemTrigger } from "../core.mjs";
import { Models } from "../models.mjs";

// Register phase type.
Phase.buildInstanceCallbacks[Phase.TYPE_SCORE] = (configuration) => {
	return new ScorePhase(configuration);
};

/**
 * @typedef {Object} ScorePhaseConfiguration
 * @property {number[]} scoreTileIndexes
 */

/**
 * @typedef {SerializedPhase} SerializedScorePhase
 * @property {ScorePhaseConfiguration} configuration
 */

export class ScorePhase extends Phase {
	/**
	 * @param {ScorePhaseConfiguration} configuration
	 */
	constructor(configuration) {
		super(Phase.TYPE_SCORE);
		this.configuration = configuration;
	}

	/**
	 * @returns {number[]}
	 */
	get scoreTileIndexes() {
		return this.configuration.scoreTileIndexes;
	}

	/**
	 * @param {Game} game
	 */
	start(game) {
		// Global score tiles.
		this.getScoreTiles(game).forEach((tile) => {
			let source = new TimelineItemTrigger('c.timeline.source_global_score_tile', null, { code: tile.code });
			tile.evaluation.forEach((strategyData) => {
				let strategy = Strategies.getEffect(strategyData.type);
				if (!strategy) {
					return;
				}
				game.players.forEach((player) => {
					strategy.apply(strategyData.configuration, game, player, source);
				});
			});
		});

		// Personal score tiles.
		game.players.forEach((player) => {
			player.tiles.forEach((tile) => {
				let source = new TimelineItemTrigger('c.timeline.source_personal_score_tile', null, { code: tile.code });
				Models.getScoreTileByCode(tile.code).evaluation.forEach((strategyData) => {
					let strategy = Strategies.getEffect(strategyData.type);
					if (strategy) {
						strategy.apply(strategyData.configuration, game, player, source);
					}
				});
			});
		});

		game.endPhase();
	}

	/**
	 * @param {Game} game
	 */
	end(game) {
	}

	/**
	 * @param {Game} game
	 * @returns {ScoreTileModel[]}
	 */
	getScoreTiles(game) {
		let tiles = [];
		this.scoreTileIndexes.forEach((index) => {
			let tile = game.scoreTiles[index] || null;
			if (!tile) {
				console.error('No score tile for index', index);
				return;
			}
			tiles.push(tile);
		});
		return tiles;
	}

	/**
	 * @returns {SerializedScorePhase}
	 */
	get data() {
		let data = super._data;
		data.configuration = this.configuration;
		return data;
	}

	/**
	 * @param {SerializedScorePhase} data
	 */
	set data(data) {
		super._data = data;
		this.configuration = data.configuration;

	}
}