/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Phase } from "../phase.mjs";
import { ArrayUtils } from "../../core/tools/array.mjs";
import { Player } from "../players.mjs";
import { PlayerDirectory } from "../../core/structures/player.mjs";
import { Connection } from "../../core/tools/connection.mjs";
import { Sounds } from "../sounds.mjs";
import { Preferences } from "../preferences.mjs";

// Register phase type.
Phase.buildInstanceCallbacks[Phase.TYPE_DRAFT] = (configuration) => {
	return new DraftPhase(configuration);
};

/**
 * @typedef {Object} DraftPhaseConfiguration
 * @property {string} itemType
 * @property {string} poolPropertyName
 * @property {string} applyChoiceMethodName
 * @property {number} handSize
 * @property {number} roundCount
 * @property {DirectionEnum} draftDirection
 */

/**
 * @typedef {SerializedPhase} SerializedDraftPhase
 * @property {DraftPhaseConfiguration} configuration
 * @property {number} passedRounds
 * @property {SerializedDraftRoundState} roundState
 */

export class DraftPhase extends Phase {
	static TYPE_CARD = 'card';
	static TYPE_TILE = 'tile';

	/**
	 * @type {number}
	 */
	passedRounds = 0;

	/**
	 * @type {DraftRoundState}
	 */
	roundState = new DraftRoundState();

	/**
	 * @param {DraftPhaseConfiguration} configuration
	 */
	constructor(configuration) {
		super(Phase.TYPE_DRAFT);
		this.configuration = configuration;
	}

	/**
	 * @returns {string}
	 */
	get itemType() {
		return this.configuration.itemType;
	}

	/**
	 * @returns {string}
	 */
	get poolPropertyName() {
		return this.configuration.poolPropertyName;
	}

	/**
	 * @returns {string}
	 */
	get applyChoiceMethodName() {
		return this.configuration.applyChoiceMethodName;
	}

	/**
	 * @returns {number}
	 */
	get handSize() {
		return this.configuration.handSize;
	}

	/**
	 * @returns {number}
	 */
	get roundCount() {
		return this.configuration.roundCount;
	}

	/**
	 * @returns {DirectionEnum}
	 */
	get draftDirection() {
		return this.configuration.draftDirection;
	}

	/**
	 * @returns {boolean}
	 */
	isFinished() {
		return this.passedRounds >= this.roundCount;
	}

	/**
	 * @param {Game} game
	 */
	start(game) {
		let pool = game[this.poolPropertyName];
		if (!pool) {
			console.warn('[DraftPhase.start] No card pool found!');
			this.end(game);
			return;
		}

		for (let i = 0; i < this.handSize; i++) {
			game.players.forEach((player) => {
				player.hand.addCode(pool.draw());
			});
		}

		this.startRound(game);
	}

	/**
	 * @param {Game} game
	 */
	startRound(game) {
		this.roundState.init(game.players);

		if (Preferences.getInstance().playSounds) {
			Sounds.startPhase.play();
		}
	}

	/**
	 * @param {Game} game
	 */
	endRound(game) {
		// Play the selected card.
		let hands = [];
		game.players.forEach((player) => {
			let choice = this.roundState.getChoice(player);
			if (choice) {
				player[this.applyChoiceMethodName](choice.selectedIndex);
			}

			hands.push(player.hand.data);
		});

		// If the phase is not finished, pass hands.
		this.passedRounds++;
		if (!this.isFinished()) {
			hands = ArrayUtils.circularPermutation(hands, this.draftDirection);

			game.players.forEach((player, index) => {
				console.log(player.name, index)
				player.hand.data = hands[index];
			});

			this.startRound(game);
		}
		else {
			game.endPhase();
		}
	}

	/**
	 * @param {Game} game
	 */
	end(game) {
		game.players.forEach((player) => {
			player.hand.clear();
		});
	}

	/**
	 * @param {Game} game
	 * @param {number} index
	 */
	setCurrentPlayerChoice(game, index) {
		let player = PlayerDirectory.getByPeerId(Connection.id);
		if (player instanceof Player) {
			if (Connection.isServer()) {
				this.roundState.setChoice(player, index);
				if (this.roundState.isFinished()) {
					this.endRound(game);
				}
				game.dispatch();
			}
			else {
				Connection.sendMessage('DraftPhaseUpdateSelection', { selectedIndex: index });
			}
		}
		else {
			console.warn('[DraftPhase.setCurrentPlayerChoice] No current player.');
		}
	}

	/**
	 * @param {Game} game
	 * @param {number} index
	 * @param {string} peerId
	 */
	setClientPlayerChoice(game, index, peerId) {
		let player = PlayerDirectory.getByPeerId(peerId);
		if (player instanceof Player) {
			this.roundState.setChoice(player, index);
			if (this.roundState.isFinished()) {
				this.endRound(game);
			}
			game.dispatch();
		}
		else {
			console.warn('[DraftPhase.setClientPlayerChoice] No player for peerId ' + peerId + '.');
		}
	}

	/**
	 * @returns {SerializedDraftPhase}
	 */
	get data() {
		let data = super._data;
		data.configuration = this.configuration;
		data.passedRounds = this.passedRounds;
		data.roundState = this.roundState;
		return data;
	}

	/**
	 * @param {SerializedDraftPhase} data
	 */
	set data(data) {
		super._data = data;
		this.configuration = data.configuration;
		this.passedRounds = data.passedRounds;
		this.roundState = DraftRoundState.fromData(data.roundState);

	}
}

/**
 * @typedef {Object} SerializedDraftRoundState
 * @property {Object.<string, SerializedDraftPlayerChoice|null>} playerChoices
 */

class DraftRoundState {
	/**
	 * @param {SerializedDraftRoundState} data
	 * @returns {DraftRoundState}
	 */
	static fromData(data) {
		let state = new this();
		state.data = data;
		return state;
	}

	playerChoices = {};

	/**
	 * @param {Player[]} players
	 */
	init(players) {
		players.forEach((player) => {
			this.playerChoices[player.name] = null;
		});
	}

	/**
	 * @returns {boolean}
	 */
	isFinished() {
		let finished = true;
		Object.keys(this.playerChoices).forEach((key) => {
			if (this.playerChoices[key] === null) {
				finished = false;
			}
		});
		return finished;
	}

	/**
	 * @param {Player} player
	 * @param {number|null} index
	 */
	setChoice(player, index) {
		if (index === null) {
			this.playerChoices[player.name] = null;
		}
		else {
			this.playerChoices[player.name] = new DraftPlayerChoice(index);
		}
		this.playerChoices = Object.assign({}, this.playerChoices); // Silly trick to make reactivity work...
	}

	/**
	 * @param {Player} player
	 */
	getChoice(player) {
		return this.playerChoices[player.name];
	}

	/**
	 * @returns {SerializedDraftRoundState}
	 */
	get data() {
		let data = {
			playerChoices: {}
		}
		Object.keys(this.playerChoices).forEach((key) => {
			if (this.playerChoices[key] === null) {
				data.playerChoices[key] = null;
			}
			else {
				data.playerChoices[key] = this.playerChoices[key].data;
			}
		});
		return data;
	}

	/**
	 * @param {SerializedDraftRoundState} data
	 */
	set data(data) {
		this.playerChoices = {};
		Object.keys(data.playerChoices).forEach((key) => {
			if (data.playerChoices[key] === null) {
				this.playerChoices[key] = null;
			}
			else {
				this.playerChoices[key] = DraftPlayerChoice.fromData(data.playerChoices[key]);
			}
		});
	}
}

/**
 * @typedef {Object} SerializedDraftPlayerChoice
 * @property {number} selectedIndex
 */

class DraftPlayerChoice {
	/**
	 * @param {SerializedDraftPlayerChoice} data
	 * @returns {DraftPlayerChoice}
	 */
	static fromData(data) {
		return new this(data.selectedIndex);
	}

	/**
	 * @param {number} selectedIndex
	 */
	constructor(selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	/**
	 * @returns {SerializedDraftPlayerChoice}
	 */
	get data() {
		return {
			selectedIndex: this.selectedIndex
		};
	}

	/**
	 * @param {SerializedDraftPlayerChoice} data
	 */
	set data(data) {
		this.selectedIndex = data.selectedIndex;
	}
}