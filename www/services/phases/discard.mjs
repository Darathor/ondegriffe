/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Phase } from "../phase.mjs";
import { ArrayUtils } from "../../core/tools/array.mjs";

// Register phase type.
Phase.buildInstanceCallbacks[Phase.TYPE_DISCARD] = (configuration) => {
	return new DiscardPhase(configuration);
};

/**
 * @typedef {Object} DiscardPhaseConfiguration
 * @property {string} itemType
 */

/**
 * @typedef {SerializedPhase} SerializedDiscardPhase
 * @property {ScorePhaseConfiguration} configuration
 */

export class DiscardPhase extends Phase {
	static TYPE_CARD = 'card';
	static TYPE_TILE = 'tile';

	/**
	 * @param {DiscardPhaseConfiguration} configuration
	 */
	constructor(configuration) {
		super(Phase.TYPE_DISCARD);
		this.configuration = configuration;
	}

	/**
	 * @returns {string}
	 */
	get itemType() {
		return this.configuration.itemType;
	}

	/**
	 * @param {Game} game
	 */
	start(game) {
		game.players.forEach((player) => {
			if (this.itemType === DiscardPhase.TYPE_CARD) {
				ArrayUtils.clear(player.cards);
			}
			else if (this.itemType === DiscardPhase.TYPE_TILE) {
				ArrayUtils.clear(player.tiles);
			}
		});

		game.endPhase();
	}

	/**
	 * @param {Game} game
	 */
	end(game) {
	}

	/**
	 * @returns {SerializedDiscardPhase}
	 */
	get data() {
		let data = super._data;
		data.configuration = this.configuration;
		return data;
	}

	/**
	 * @param {SerializedDiscardPhase} data
	 */
	set data(data) {
		super._data = data;
		this.configuration = data.configuration;

	}
}