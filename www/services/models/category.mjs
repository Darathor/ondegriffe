/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../../core/tools/i18n.mjs";

export class CategoryModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string} type
	 */
	constructor(code, src, type) {
		this.code = code;
		this.src = src;
		this.type = type;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('category_' + this.code);
	}
}