/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../../core/tools/i18n.mjs";
import { ArrayUtils } from "../../core/tools/array.mjs";
import { Models } from "../models.mjs";

export class CardModel {
	static ARTWORK = 'artwork';

	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string|null} position
	 * @param {string} fullSrc
	 * @param {string} type
	 * @param {Object} data
	 */
	static buildInstance(code, src, position, fullSrc, type, data) {
		switch (type) {
			case this.ARTWORK:
				// noinspection JSUnresolvedVariable
				return new ArtworkCardModel(code, src, position, fullSrc, type, data.artworkData || {});
		}
		console.error('Unknown card type ' + type + ' (' + code + ')');
	}

	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string|null} position
	 * @param {string} fullSrc
	 * @param {string} type
	 */
	constructor(code, src, position, fullSrc, type) {
		this.code = code;
		this.src = src;
		this.position = position;
		this.fullSrc = fullSrc;
		this.type = type;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('card_' + this.code);
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	hasCategory(code) {
		return false;
	}
}

/**
 * @typedef {Object} ArtworkCardModelData
 * @property {int} year
 * @property {string[]} authors
 * @property {string[]} categories
 */

export class ArtworkCardModel extends CardModel {
	type = CardModel.ARTWORK;

	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string|null} position
	 * @param {string} fullSrc
	 * @param {string} type
	 * @param {ArtworkCardModelData} data
	 */
	constructor(code, src, position, fullSrc, type, data) {
		super(code, src, position, fullSrc, type);

		this.year = data.year || [];
		this.authors = data.authors || [];
		this.categories = Models.sortCategoryCodes(data.categories || []);
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	hasCategory(code) {
		return ArrayUtils.contains(this.categories, code);
	}
}

