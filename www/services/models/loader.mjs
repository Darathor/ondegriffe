/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../../core/tools/i18n.mjs";
import { FileLoader } from "../../core/tools/file-loader.mjs";
import { ArrayUtils } from "../../core/tools/array.mjs";
import { Models } from "../models.mjs";
import { Extension } from "./extension.mjs";
import { CategoryModel } from "./category.mjs";
import { CardModel } from "./card.mjs";
import { AuthorModel } from "./author.mjs";
import { ScoreTileModel } from "./scoreTile.mjs";

export let ModelsLoader = {
	/**
	 * @param {string[]} extensions
	 * @param {function} onDone
	 */
	load(extensions, onDone) {
		Models.extensionCodes = extensions;
		new ExtensionLoader(I18n.LCID, extensions, onDone);
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addCategories(modelsData, extension) {
		modelsData.forEach((data) => {
			let categoryModel = new CategoryModel(data.code, data.src, data.type);
			Models.categories[data.code] = categoryModel;
			Models.categoriesArray.push(categoryModel);
			extension.categories.push(categoryModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addScoreTiles(modelsData, extension) {
		modelsData.forEach((data) => {
			let scoreTileModel = new ScoreTileModel(data.code, data.src, data.evaluation, data.categoryReferences || []);
			Models.scoreTiles[data.code] = scoreTileModel;
			Models.scoreTilesArray.push(scoreTileModel);
			extension.scoreTiles.push(scoreTileModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addAuthors(modelsData, extension) {
		modelsData.forEach((data) => {
			let authorModel = new AuthorModel(
				data.code, data.src, data.name, data.aliases || [], data.birthYear, data.deathYear, data.biography, data.links || []
			);
			Models.authors[data.code] = authorModel;
			Models.authorsArray.push(authorModel);
			extension.authors.push(authorModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addCards(modelsData, extension) {
		modelsData.forEach((data) => {
			let cardModel = CardModel.buildInstance(data.code, data.src, data.position || null, data.fullSrc || null, data.type, data);
			Models.cards[data.code] = cardModel;
			Models.cardsArray.push(cardModel);
			extension.cards.push(cardModel);
		});
	},

	/**
	 * @param {string} name
	 * @param {Object} response
	 */
	addExtension(name, response) {
		let extension = new Extension(response.code, response.symbolSrc || null, response.author, response.descriptionKey);
		ArrayUtils.replace(Models.extensionCodes, name, extension.code);
		Models.extensions[extension.code] = extension;
		this.rebuildExtensionsArray();

		// New models.
		if ('categories' in response) {
			this.addCategories(response.categories, extension);
		}
		if ('scoreTiles' in response) {
			this.addScoreTiles(response.scoreTiles, extension);
		}
		if ('authors' in response) {
			this.addAuthors(response.authors, extension);
		}
		if ('cards' in response) {
			this.addCards(response.cards, extension);
		}
	},
	rebuildExtensionsArray() {
		ArrayUtils.clear(Models.extensionsArray);
		Models.extensionCodes.forEach((code) => {
			if (Models.extensions[code]) {
				Models.extensionsArray.push(Models.extensions[code]);
			}
		})
	}
};

class ExtensionLoader {
	/**
	 * @param {string} LCID
	 * @param {string[]} extensions
	 * @param {function} onDone
	 */
	constructor(LCID, extensions, onDone) {
		let loader = new FileLoader();
		let extensionsData = {};
		let updatedOnDone = () => {
			extensions.forEach((extension) => {
				ModelsLoader.addExtension(extension, extensionsData[extension]);
			});
			onDone();
		};
		extensions.forEach((extension) => {
			loader.loadFile('extensions/' + extension + '/extension.json', (response) => { extensionsData[extension] = response; }, updatedOnDone);
			loader.loadFile('extensions/' + extension + '/i18n/' + LCID + '.json', (data) => { I18n.importPackage(data, ''); }, updatedOnDone);
			I18n.registerFilePath('', 'extensions/' + extension + '/i18n/');
		});
	}
}