/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../../core/tools/i18n.mjs";

export class Extension {
	/** @type {CategoryModel[]} */
	categories = [];
	/** @type {ScoreTileModel[]} */
	scoreTiles = [];
	/** @type {CardModel[]} */
	cards = [];
	/** @type {AuthorModel[]} */
	authors = [];

	/**
	 * @param {string} code
	 * @param {string} symbolSrc
	 * @param {string} author
	 * @param {string} descriptionKey
	 */
	constructor(code, symbolSrc, author, descriptionKey) {
		this.code = code;
		this.symbolSrc = symbolSrc;
		this.author = author;
		this.descriptionKey = descriptionKey;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('extension_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('extension_' + this.code + '_description');
	}
}
