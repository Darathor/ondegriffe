/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../../core/tools/i18n.mjs";
import { ArrayUtils } from "../../core/tools/array.mjs";

/**
 * @typedef {Object} ScoreTileEvaluationData
 * @property {string} type
 * @property {Object} configuration
 */

export class ScoreTileModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {ScoreTileEvaluationData[]} evaluation
	 * @param {string[]} categoryReferences
	 */
	constructor(code, src, evaluation, categoryReferences) {
		this.code = code;
		this.src = src;
		this.evaluation = evaluation;
		this.categoryReferences = categoryReferences;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('score_tile_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('score_tile_' + this.code + '_description');
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	refersCategory(code) {
		return ArrayUtils.contains(this.categoryReferences, code);
	}
}