/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export class AuthorModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string} name
	 * @param {string[]} aliases
	 * @param {string} birthYear
	 * @param {string} deathYear
	 * @param {string} biography
	 * @param {string[]} links
	 */
	constructor(code, src, name, aliases, birthYear, deathYear, biography, links) {
		this.code = code;
		this.src = src;
		this.name = name;
		this.birthYear = birthYear;
		this.deathYear = deathYear;
		this.biography = biography;
		this.links = links;
	}
}