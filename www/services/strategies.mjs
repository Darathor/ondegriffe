/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export let Strategies = {
	conditions: {},
	effects: {},
	playerEffects: {},
	/**
	 * @param {string} code
	 * @returns {ConditionStrategy}
	 */
	getCondition(code) {
		let strategy = this.conditions[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getCondition] Unknown strategy:', code);
		return this.conditions['null'];
	},
	/**
	 * @param {string} code
	 * @returns {EvaluationStrategy}
	 */
	getEvaluation(code) {
		let strategy = this.evaluation[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getEvaluation] Unknown strategy:', code);
		return this.conditions['null'];
	},
	/**
	 * @param {string} code
	 * @returns {EffectStrategy}
	 */
	getEffect(code) {
		let strategy = this.effects[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getEffect] Unknown strategy:', code);
		return this.effects['null'];
	},
	/**
	 * @param {string} code
	 * @returns {PlayerEffectStrategy}
	 */
	getPlayerEffect(code) {
		let strategy = this.playerEffects[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getPlayerEffect] Unknown strategy:', code);
		return this.playerEffects['null'];
	},
	/**
	 * @param {ConditionData[]} conditionsData
	 * @param {Structure} structure
	 * @param {Player=} player
	 * @param {Game} game
	 * @param {PlayerPawn|NeutralPawn=} pawn
	 * @returns {boolean}
	 */
	checkCondition(conditionsData, structure, player, game, pawn) {
		let conditionsVerified = true;
		conditionsData.forEach((conditionData) => {
			let condition = Strategies.getCondition(conditionData.type);
			let result = condition.evaluate(structure, conditionData.configuration || {}, player, game, pawn);
			if ((result && conditionData.not) || (!result && !conditionData.not)) {
				conditionsVerified = false;
			}
		});
		return conditionsVerified;
	}
}