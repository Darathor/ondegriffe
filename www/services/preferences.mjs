/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { BasePreferences } from "../core/structures/preferences.mjs";

export class Preferences extends BasePreferences {
	static CARD_DISPLAY_FULL = 'full';
	static CARD_DISPLAY_HALF = 'half';
	static CARD_DISPLAY_NONE = 'none';

	static PHASES_DISPLAY_ASIDE = 'aside';
	static PHASES_DISPLAY_FOOTER = 'footer';

	/**
	 * @returns {Preferences}
	 */
	static getInstance() {
		if (!this.instance) {
			this.instance = new this();
		}
		return this.instance;
	}

	/** @type {boolean} */
	_setupHideDisabledItems = false;
	/** @type {boolean} */
	_collapseAsidePlayers = false;
	/** @type {string}  */
	_boardCardsDisplay = Preferences.CARD_DISPLAY_FULL;
	/** @type {string}  */
	_phasesDisplay = Preferences.PHASES_DISPLAY_ASIDE;

	get data() {
		let data = super.data;

		data._setupHideDisabledItems = this._setupHideDisabledItems;
		data._collapseAsidePlayers = this._collapseAsidePlayers;
		data._boardCardsDisplay = this._boardCardsDisplay;
		data._phasesDisplay = this._phasesDisplay;

		return data;
	}

	/**
	 * @returns {boolean}
	 */
	get setupHideDisabledItems() {
		return this._setupHideDisabledItems;
	}

	/**
	 * @param {boolean} value
	 */
	set setupHideDisabledItems(value) {
		this._setupHideDisabledItems = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsidePlayers() {
		return this._collapseAsidePlayers;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsidePlayers(value) {
		this._collapseAsidePlayers = value;
		this.save();
	}

	/**
	 * @returns {string}
	 */
	get boardCardsDisplay() {
		return this._boardCardsDisplay;
	}

	/**
	 * @param {string} value
	 */
	set boardCardsDisplay(value) {
		this._boardCardsDisplay = value;
		this.save();
	}
	/**
	 * @returns {string}
	 */
	get phasesDisplay() {
		return this._phasesDisplay;
	}

	/**
	 * @param {string} value
	 */
	set phasesDisplay(value) {
		this._phasesDisplay = value;
		this.save();
	}
}

// Build the instance with de final class.
Preferences.getInstance();