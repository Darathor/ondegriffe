/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Connection } from "../core/tools/connection.mjs";
import { ArrayUtils } from "../core/tools/array.mjs";
import { Models } from './models.mjs';
import { Board } from './board.mjs';
import { Player } from './players.mjs';
import { Setup } from './setup.mjs';
import { Preferences } from "./preferences.mjs";
import { Ranking } from "./ranking.mjs";
import { Sounds } from "./sounds.mjs";
import { DateUtils } from "../core/tools/date.mjs";
import { Alerts } from "../core/tools/alerts.mjs";
import { Timeline } from "./core.mjs";
import { Phase } from "./phase.mjs";
import { DraftPhase } from "./phases/draft.mjs";
import { ScorePhase } from "./phases/score.mjs";
import { DiscardPhase } from "./phases/discard.mjs";
import { DirectionEnum } from "../core/structures/enums.mjs";

let document = window.document;

/**
 * @typedef {Object} PoolSetup
 * @property {string[]} codes
 * @property {string} poolType
 * @property {number|null} poolSize
 */

/**
 * @typedef {Object} SerializedPool
 * @property {string} type
 * @property {string[]} codes
 * @property {PoolSetup} setup
 */

export class Pool {
	/** @type {PoolSetup} */
	setup = { codes: [], poolType: '', poolSize: null };
	/** @type {string[]} */
	codes = [];

	/**
	 * @param {string[]=} codes
	 * @param {string=} poolType - 'selection' or 'random'
	 * @param {number|null=} poolSize
	 */
	constructor(codes, poolType, poolSize) {
		this.setup.codes = codes;
		this.setup.poolType = poolType;
		this.setup.poolSize = poolSize;
		this.init();
	}

	/**
	 * @returns {SerializedPool}
	 */
	get data() {
		return {
			codes: this.codes,
			setup: this.setup
		};
	}

	/**
	 * @param {SerializedPool} data
	 */
	set data(data) {
		this.codes = data.codes;
		this.setup = data.setup;
	}

	/**
	 * @param {string} code
	 */
	addItem(code) {
		this.codes.push(code);
	}

	get length() {
		return this.codes.length;
	}

	/**
	 * @returns {boolean}
	 */
	isEmpty() {
		return this.length === 0;
	}

	init() {
		if (this.setup.codes && this.setup.poolType) {
			if (this.setup.poolType === 'selection') {
				this.setup.codes.forEach((item) => {
					this.addItem(item);
				});
			}
			else if (this.setup.poolType === 'random') {
				for (let i = 0; i < this.setup.poolSize; i++) {
					this.addItem(ArrayUtils.randomItem(this.setup.codes));
				}
			}
			else {
				console.error('[Pool.init] Unknown poolType:', this.setup.poolType);
			}
		}
	}

	/**
	 * @returns {string}
	 */
	draw() {
		if (this.isEmpty()) {
			this.init();
		}
		return this.codes.splice(ArrayUtils.randomIndex(this.codes), 1)[0];
	}

	/**
	 * @param {string} item
	 * @returns {string}
	 */
	redraw(item) {
		let newItem = this.draw();
		this.codes.push(item);
		return newItem;
	}
}

/**
 * @typedef {Object} SerializedGame
 * @property {SerializedPlayer[]} players
 * @property {OptionalRules} optionalRules
 * @property {SerializedPool} cardPool
 * @property {SerializedPool} tilePool
 * @property {string[]} scoreTiles
 * @property {SerializedBoard} board
 * @property {boolean} ended
 * @property {TimelineData} timeline
 * @property {SerializedPhase[]} phases
 * @property {number} currentPhaseIndex
 */

/**
 * @typedef {Object} OptionalRules
 */

class Game {
	/** @type {Date|null} */
	startDate = null;
	/** @type {number} */
	lastHourAlert = 0;
	/** @type {boolean} */
	initialized = false;
	/** @type {boolean} */
	announced = false;
	/** @type {boolean} */
	ended = false;
	/** @type {boolean} */
	restoringData = false;
	/** @type {SerializedGame|null} */
	dataToRestore = null;
	/** @type {Player[]} */
	players = [];
	/** @type {Player} */
	me = null;
	/** @type {OptionalRules} */
	optionalRules = {};
	/** @type {Phase[]} */
	phases = [];
	/** @type {Phase} */
	currentPhase = null;
	/** @type {number} */
	currentPhaseIndex = 0;
	/** @type {Pool} */
	cardPool = null;
	/** @type {Pool} */
	tilePool = null;
	/** @type {ScoreTileModel[]} */
	scoreTiles = null;
	modals = {
		/** @type {boolean} */
		timeline: false,
	};
	preferences = Preferences.getInstance();

	constructor() {
		setInterval(() => {
			if (this.startDate) {
				let minutes = DateUtils.getMinutesDifference(this.startDate, new Date());
				if (Math.floor(minutes / 60) > this.lastHourAlert) {
					this.lastHourAlert++;
					let options = {
						substitutions: { hours: this.lastHourAlert },
						icon: 'images/icons/clock.svg'
					};
					if (this.preferences.playSounds) {
						options.sound = Sounds.newAlert;
					}
					if (this.lastHourAlert === 1) {
						Alerts.info('c.main.alert_playing_since_one_hour', options);
					}
					else {
						Alerts.info('c.main.alert_playing_since_n_hours', options);
					}
				}
			}
		}, 1000);
	}

	/**
	 * @param {string=} excludedPeerId
	 */
	dispatch(excludedPeerId) {
		Connection.sendMessage('GameUpdate', this.data, excludedPeerId);
	}

	/**
	 * @param {string} key
	 * @param {Object} options
	 */
	dispatchAlert(key, options) {
		Connection.sendMessage('GameAlert', { key: key, options: options });
	}

	init() {
		Setup.saveLast();

		// Init card pool.
		this.cardPool = new Pool(Setup.selectedCardModels, Setup.poolType, Setup.poolSize);

		// Init tile pool.
		this.tilePool = new Pool(Setup.selectedScoreTileModels, Setup.tilePoolType, Setup.tilePoolSize);

		// Init score tiles.
		let availableTiles = ArrayUtils.shuffle(Setup.selectedScoreTileModels);
		let tileCount = 5;
		this.scoreTiles = [];
		do {
			let codes = availableTiles.slice(0, Math.min(tileCount, availableTiles.length));
			codes.forEach((code) => {
				this.scoreTiles.push(Models.getScoreTileByCode(code));
			});
		}
		while (this.scoreTiles.length < tileCount);

		// Init players.
		this.players = Setup.players;
		this.players.forEach((player) => {
			if (player.peerId === Connection.id) {
				this.me = player;
			}
		});
		this.optionalRules = Setup.optionalRules;

		// Init phases.
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_TILE, poolPropertyName: 'tilePool', applyChoiceMethodName: 'playTileByIndex',
			handSize: 3, roundCount: 2, draftDirection: DirectionEnum.LEFT
		}));
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_CARD, poolPropertyName: 'cardPool', applyChoiceMethodName: 'playCardByIndex',
			handSize: 6, roundCount: 5, draftDirection: DirectionEnum.LEFT
		}));
		this.phases.push(new ScorePhase({ scoreTileIndexes: [0, 1] }));
		this.phases.push(new DiscardPhase({ itemType: DiscardPhase.TYPE_TILE }));
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_TILE, poolPropertyName: 'tilePool', applyChoiceMethodName: 'playTileByIndex',
			handSize: 3, roundCount: 2, draftDirection: DirectionEnum.RIGHT
		}));
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_CARD, poolPropertyName: 'cardPool', applyChoiceMethodName: 'playCardByIndex',
			handSize: 6, roundCount: 5, draftDirection: DirectionEnum.RIGHT
		}));
		this.phases.push(new ScorePhase({ scoreTileIndexes: [2, 4] }));
		this.phases.push(new DiscardPhase({ itemType: DiscardPhase.TYPE_TILE }));
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_TILE, poolPropertyName: 'tilePool', applyChoiceMethodName: 'playTileByIndex',
			handSize: 3, roundCount: 2, draftDirection: DirectionEnum.LEFT
		}));
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_CARD, poolPropertyName: 'cardPool', applyChoiceMethodName: 'playCardByIndex',
			handSize: 6, roundCount: 5, draftDirection: DirectionEnum.LEFT
		}));
		this.phases.push(new ScorePhase({ scoreTileIndexes: [0, 3, 1] }));
		this.phases.push(new DiscardPhase({ itemType: DiscardPhase.TYPE_TILE }));
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_TILE, poolPropertyName: 'tilePool', applyChoiceMethodName: 'playTileByIndex',
			handSize: 3, roundCount: 2, draftDirection: DirectionEnum.RIGHT
		}));
		this.phases.push(new DraftPhase({
			itemType: DraftPhase.TYPE_CARD, poolPropertyName: 'cardPool', applyChoiceMethodName: 'playCardByIndex',
			handSize: 6, roundCount: 5, draftDirection: DirectionEnum.RIGHT
		}));
		this.phases.push(new ScorePhase({ scoreTileIndexes: [3, 2, 4] }));
		this.phases.push(new DiscardPhase({ itemType: DiscardPhase.TYPE_TILE }));

		this.initialized = true;
		this.startDate = new Date();
		Timeline.log('c.timeline.game_begins', 'global');

		this.startPhase();
	}

	startPhase() {
		this.currentPhase = this.phases[this.currentPhaseIndex];

		if (this.currentPhase) {
			let substitutions = { index: this.currentPhaseIndex + 1 };
			Timeline.log('c.timeline.start_phase_n', 'global', null, null, substitutions);
			let options = {
				substitutions: substitutions,
				icon: 'images/icons/clock.svg'
			};
			if (this.preferences.playSounds) {
				options.sound = Sounds.newAlert;
			}
			Alerts.info('c.timeline.start_phase_n', options);

			options.sound = 'newAlert';
			this.dispatchAlert('c.timeline.start_phase_n', options);

			this.currentPhase.start(game);
			this.dispatch();
		}
		else {
			this.endGame();
		}
	}

	endPhase() {
		this.currentPhase.end(game);
		this.currentPhase = null;

		this.currentPhaseIndex++;
		this.startPhase();
	}

	endGame() {
		Timeline.log('c.timeline.final_points_count', 'global');

		// Final ranking.
		let globalRanking = new Ranking();
		this.players.forEach((player) => {
			globalRanking.setPlayerScore(player, player.points);
		});
		let winners = globalRanking.getPlayersByPosition(1);

		if (winners.length === 1) {
			Timeline.log('c.timeline.game_winner', 'global', null, null, { playerName: winners[0].name });
		}
		else {
			let playerNames = [];
			winners.forEach((player) => { playerNames.push(player.name); });
			let last = playerNames.pop();
			Timeline.log('c.timeline.game_winners', 'global', null, null, { playerNames: playerNames.join(', '), lastPlayerName: last });
		}
		this.ended = true;

		Timeline.log('c.timeline.game_over', 'global');
		this.dispatch();
	}

	forceEnd() {
		// Clear data.
		// TODO clear phase data

		Timeline.log('c.timeline.forced_end_game', 'global');
		this.endGame();
	}

	/**
	 * @returns {SerializedGame|null}
	 */
	get data() {
		if (!this.initialized) {
			return null;
		}

		let data = {
			players: [],
			optionalRules: this.optionalRules,
			cardPool: this.cardPool.data,
			scoreTiles: [],
			board: Board.data,
			ended: this.ended,
			timeline: Timeline.data,
			phases: [],
			currentPhaseIndex: this.currentPhaseIndex
		};

		this.players.forEach((player) => {
			data.players.push(player.data);
		});

		this.scoreTiles.forEach((scoreTile) => {
			data.scoreTiles.push(scoreTile.code);
		});

		this.phases.forEach((phase) => {
			// noinspection JSUnresolvedVariable
			data.phases.push(phase.data);
		});

		return data;
	}

	/**
	 * @param {SerializedGame|null} data
	 */
	set data(data) {
		if (!data) {
			return;
		}
		if (this.restoringData) {
			console.warn('Already restoring data, wait 50ms.');
			this.dataToRestore = data;
			setTimeout(() => {
				if (this.dataToRestore) {
					let dataToRestore = this.dataToRestore;
					this.dataToRestore = null;
					this.data = dataToRestore;
				}
			}, 50);
			return;
		}
		this.restoringData = true;

		this.initialized = true;
		this.startDate = new Date();

		//region Clear previous data.

		this.players.forEach((player) => {
			player.free();
		});
		ArrayUtils.clear(this.players);

		//endregion.

		if (data.hasOwnProperty('players')) {
			data.players.forEach((item) => {
				this.players.push(Player.fromData(item));
			});

			this.players.forEach((player) => {
				if (player.peerId === Connection.id) {
					this.me = player;
				}
			});
		}

		if (data.hasOwnProperty('optionalRules')) {
			this.optionalRules = data.optionalRules;
		}

		if (data.hasOwnProperty('cardPool')) {
			if (!this.cardPool) {
				this.cardPool = new Pool();
			}
			this.cardPool.data = data.cardPool;
		}

		if (data.hasOwnProperty('scoreTiles')) {
			this.scoreTiles = [];
			data.scoreTiles.forEach((code) => {
				this.scoreTiles.push(Models.getScoreTileByCode(code));
			});
		}

		if (data.hasOwnProperty('ended')) {
			this.ended = data.ended;
		}

		if (data.hasOwnProperty('timeline')) {
			Timeline.data = data.timeline;
		}

		if (data.hasOwnProperty('phases')) {
			ArrayUtils.clear(this.phases);
			data.phases.forEach((phaseData) => {
				let phase = Phase.buildInstance(phaseData.type, []);
				phase.data = phaseData;
				this.phases.push(phase);
			});
		}

		if (data.hasOwnProperty('currentPhaseIndex')) {
			this.currentPhaseIndex = data.currentPhaseIndex;
			this.currentPhase = this.phases[data.currentPhaseIndex];
		}

		this.restoringData = false;
	}
}

export let game = new Game();

//region Event listeners on board events.
document.addEventListener('coreConnectionStartGame', (event) => {
	game.data = event.detail.data;
});

document.addEventListener('coreConnectionGameUpdate', (event) => {
	game.data = event.detail.data;

	if (Connection.isServer()) {
		game.dispatch(event.detail.peerId);
	}
});

document.addEventListener('coreConnectionGameAlert', (event) => {
	let alertData = event.detail.data;
	if (alertData.options.sound) {
		if (game.preferences.playSounds) {
			alertData.options.sound = Sounds[alertData.options.sound];
		}
		else {
			delete alertData.options.sound;
		}
	}
	Alerts.info(alertData.key, alertData.options);
});

document.addEventListener('coreConnectionRequestReconnection', (event) => {
	if (!Connection.isServer()) {
		console.error('[on coreConnectionRequestReconnection] Not server!');
		return;
	}

	let previousId = event.detail.data.previousId;
	console.log('[on coreConnectionRequestReconnection] Try to reconnect', previousId);

	let found = false;
	game.players.forEach((player) => {
		console.log('[on coreConnectionRequestReconnection] Look for player', player.peerId, previousId);
		if (player.peerId === previousId) {
			player.peerId = event.detail.peerId;
			console.log('[on coreConnectionRequestReconnection] send message', event.detail.connection);
			Connection.sendMessageToTarget(event.detail.connection, 'Reconnected', game.data);
			found = true;
		}
	});

	if (!found) {
		console.error('[on coreConnectionRequestReconnection] No player found for this id!');
	}
	else {
		Connection.instance.closeConnection(previousId);
	}
});

document.addEventListener('coreConnectionDraftPhaseUpdateSelection', (event) => {
	let phase = game.currentPhase;
	if (!(phase instanceof DraftPhase)) {
		console.warn('[on coreConnectionDraftPhaseUpdateSelection] Not in a draft phase, message ignored.');
	}
	else {
		console.log(event.detail);
		phase.setClientPlayerChoice(game, event.detail.data.selectedIndex, event.detail.peerId);
	}
});

//endregion