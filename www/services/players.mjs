/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Timeline, TimelineItemTrigger } from './core.mjs';
import { BasePlayer } from "../core/structures/player.mjs";
import { Hand } from "./hand.mjs";
import { ArrayUtils } from "../core/tools/array.mjs";
import { Card } from "./card.mjs";
import { Tile } from "./tile.mjs";
import { Models } from "./models.mjs";
import { ArtworkCardModel } from "./models/card.mjs";

/**
 * @typedef {Object} SerializedPlayer
 * @property {string} name
 * @property {string} color - The color code.
 * @property {string} peerId
 * @property {number} points
 * @property {SerializedHand} hand
 * @property {SerializedCard[]} cards
 * @property {SerializedTile[]} tiles
 */

export class Player extends BasePlayer {
	//region Static stuff.
	/**
	 * @param {SerializedPlayer} data
	 * @returns {Player}
	 */
	static fromData(data) {
		let player = new Player();
		player.data = data;
		return player;
	}

	//endregion

	/** @type {Card[]} */
	cards = [];
	/** @type {Tile[]} */
	tiles = [];

	/**
	 * @param {string=} name
	 * @param {PlayerColor=} color
	 * @param {string=} peerId
	 */
	constructor(name, color, peerId) {
		super(name, color, peerId);
		this.points = 0;
		this.hand = new Hand([]);
	}

	/**
	 * @param {number} index
	 */
	playCardByIndex(index) {
		let code = this.hand.getCode(index);
		if (code) {
			this.cards.push(new Card(code));
			this.hand.removeCode(code);
		}
	}

	/**
	 * @param {number} index
	 */
	playTileByIndex(index) {
		let code = this.hand.getCode(index);
		if (code) {
			this.tiles.push(new Tile(code));
			this.hand.removeCode(code);
		}
	}

	/**
	 * @param {number} points
	 * @param {TimelineItemTrigger} source
	 */
	addPoints(points, source) {
		points = Math.ceil(points);
		this.points += points;
		if (points < -1) {
			Timeline.log('c.timeline.player_looses_points', 'point-loose', source, this.name, { points: points });
		}
		else if (points === -1) {
			Timeline.log('c.timeline.player_looses_point', 'point-loose', source, this.name, { points: points });
		}
		else if (points === 0) {
			Timeline.log('c.timeline.player_scores_no_point', 'point-none', source, this.name, { points: points });
		}
		else if (points === 1) {
			Timeline.log('c.timeline.player_scores_point', 'point-score', source, this.name, { points: points });
		}
		else if (points > 1) {
			Timeline.log('c.timeline.player_scores_points', 'point-score', source, this.name, { points: points });
		}
	}

	/**
	 * @returns {Object.<string, Object.<string, number>>}
	 */
	get cardCategories() {
		let data = {};

		Models.categoriesArray.forEach((category) => {
			if (!data[category.type]) {
				data[category.type] = {};
			}
			data[category.type][category.code] = 0;
		});

		this.cards.forEach((card) => {
			let model = Models.getCardByCode(card.code);
			if (!(model instanceof ArtworkCardModel)) {
				return;
			}
			model.categories.forEach((code) => {
				let category = Models.getCategoryByCode(code);
				data[category.type][code]++;
			});
		});

		return data;
	}

	/**
	 * @return {SerializedPlayer}
	 */
	get data() {
		let data = super.data;
		data.points = this.points;
		data.hand = this.hand.data;
		data.cards = [];
		data.tiles = [];

		this.cards.forEach((card) => {
			data.cards.push(card.data);
		});

		this.tiles.forEach((tile) => {
			data.tiles.push(tile.data);
		});

		return data;
	}

	/**
	 * @param {SerializedPlayer} data
	 */
	set data(data) {
		super.data = data;
		this.points = data.points;
		this.hand.data = data.hand;

		ArrayUtils.clear(this.cards);
		data.cards.forEach((cardData) => {
			this.cards.push(Card.fromData(cardData));
		});

		ArrayUtils.clear(this.tiles);
		data.tiles.forEach((tileData) => {
			this.tiles.push(Tile.fromData(tileData));
		});
	}
}