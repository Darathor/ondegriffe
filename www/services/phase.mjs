/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

import { I18n } from "../core/tools/i18n.mjs";

/**
 * @typedef {Object} SerializedPhase
 * @property {string} type
 */

export class Phase {
	static TYPE_DRAFT = 'draft';
	static TYPE_SCORE = 'score';
	static TYPE_DISCARD = 'discard';

	/**
	 * @type {Object.<string, function>}
	 */
	static buildInstanceCallbacks = {};

	/**
	 * @param {string} type
	 * @param {Object} configuration
	 * @returns {Phase|null}
	 */
	static buildInstance(type, configuration) {
		if (this.buildInstanceCallbacks[type]) {
			return this.buildInstanceCallbacks[type](configuration);
		}
		console.error('[Phase.buildInstance] Unknown phase type: ' + type);
		return null;
	}

	/**
	 * @param {string} type
	 */
	constructor(type) {
		this.type = type;
	}

	/**
	 * @param {Game} game
	 */
	start(game) {
		// Should be overridden in each phase class.
	}

	/**
	 * @param {Game} game
	 */
	end(game) {
		// Should be overridden in each phase class.
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('c.main.phase_' + this.type);
	}

	/**
	 * @returns {DirectionEnum|null}
	 */
	get draftDirection() {
		return null;
	}

	/**
	 * @returns {SerializedPhase}
	 */
	get _data() {
		return {
			type: this.type
		};
	}

	/**
	 * @param {SerializedPhase} data
	 */
	set _data(data) {
		this.type = data.type;
	}
}