/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @typedef {Object} SerializedBoard
 */

export let Board = Vue.observable({

	/**
	 * @returns {SerializedBoard|null}
	 */
	get data() {
		let data = {
		};

		return data;
	},
	/**
	 * @param {SerializedBoard|null} data
	 */
	set data(data) {
		if (!data) {
			return;
		}

	}
});
