/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core/tools/i18n.mjs";

/**
 * @typedef {Object} TimelineItemTriggerData
 * @property {string} key
 * @property {TimelineItemTriggerData|null} trigger
 * @property {Object|null} custom
 */

export class TimelineItemTrigger {
	/**
	 * @param {string} key
	 * @param {TimelineItemTrigger|null=} trigger
	 * @param {Object|null=} customData Serializable custom data.
	 */
	constructor(key, trigger, customData) {
		this.key = key;
		this.trigger = trigger;
		this.customData = customData;
	}

	/**
	 * @param {TimelineItemTriggerData|null} data
	 * @return {TimelineItemTrigger|null}
	 */
	static fromData(data) {
		if (!data || !data.hasOwnProperty('key')) {
			return null;
		}
		let trigger = data.trigger ? TimelineItemTrigger.fromData(data.trigger) : null;
		return new TimelineItemTrigger(data.key, trigger, data.custom);
	}

	/**
	 * @returns {TimelineItemTriggerData}
	 */
	toJSON() {
		return {
			key: this.key,
			trigger: this.trigger ? this.trigger.toJSON() : null,
			custom: this.customData
		};
	}

	/**
	 * @returns {Object}
	 */
	get substitutions() {
		let substitutions = this.customData ? JSON.parse(JSON.stringify(this.customData)) : {};

		if (this.trigger) {
			substitutions.trigger = I18n.trans(this.trigger.key, [], this.trigger.substitutions);
		}
		else {
			substitutions.trigger = substitutions.trigger || '';
		}

		return substitutions;
	}
}

/**
 * @typedef {Object} TimelineItemData
 * @property {string} message
 * @property {string} date
 * @property {string} category
 * @property {TimelineItemTriggerData|null} trigger
 * @property {string|null} player
 * @property {Object|null} custom
 */

export class TimelineItem {
	/**
	 * @param {string} message
	 * @param {Date} date
	 * @param {string|null=} category
	 * @param {TimelineItemTrigger|null=} trigger
	 * @param {string|null=} playerName
	 * @param {Object|null=} customData Serializable custom data.
	 */
	constructor(message, date, category, trigger, playerName, customData) {
		this.message = message;
		this.date = date;
		this.trigger = trigger;
		this.playerName = playerName;
		this.customData = customData;
		this.category = category || 'default';
	}

	/**
	 * @returns {Object}
	 */
	get substitutions() {
		let substitutions = this.customData ? JSON.parse(JSON.stringify(this.customData)) : {};

		if (this.trigger) {
			substitutions.trigger = I18n.trans(this.trigger.key, [], this.trigger.substitutions);
		}
		else {
			substitutions.trigger = substitutions.trigger || '';
		}

		if (this.playerName) {
			substitutions.player = this.playerName;
		}

		return substitutions;
	}

	/**
	 * @param {TimelineItemData} data
	 * @return
	 */
	static fromData(data) {
		return new TimelineItem(
			data.message, new Date(data.date), data.category, TimelineItemTrigger.fromData(data.trigger), data.player, data.custom
		);
	}

	/**
	 * @returns {TimelineItemData}
	 */
	get data() {
		return {
			message: this.message,
			date: this.date.toJSON(),
			category: this.category,
			trigger: this.trigger ? this.trigger.toJSON() : null,
			player: this.playerName,
			custom: this.customData
		};
	}

	/**
	 * @returns {boolean}
	 */
	get clickable() {
		return false;
	}

	/**
	 * @returns {string|null}
	 */
	get tooltip() {
		return null;
	}

	onClick() {
	}
}

/**
 * @typedef {Object} TimelineData
 * @property {TimelineItemData[]} items
 */

export let Timeline = {
	/** @type {TimelineItem[]} */
	items: [],
	/**
	 * @param {string} messageKey
	 * @param {string|null=} category
	 * @param {TimelineItemTrigger|null=} trigger
	 * @param {string|null=} playerName
	 * @param {Object|null=} customData
	 */
	log(messageKey, category, trigger, playerName, customData) {
		this.items.unshift(new TimelineItem(messageKey, new Date(), category, trigger, playerName, customData));
	},
	/**
	 * @param {string} categoryName
	 */
	clearCategory(categoryName) {
		let toRemove = [];
		this.items.forEach((item) => {
			if (item.category === categoryName) {
				toRemove.push(item);
			}
		});
		toRemove.forEach((item) => {
			Array.remove(this.items, item);
		});
	},
	/**
	 * @returns {TimelineData}
	 */
	get data() {
		let data = {
			items: []
		};

		this.items.forEach((item) => {
			data.items.push(item.data);
		});

		return data;
	},
	/**
	 * @param {TimelineData} data
	 */
	set data(data) {
		this.items = [];

		data.items.forEach((item) => {
			this.items.push(TimelineItem.fromData(item));
		});
	}
};
