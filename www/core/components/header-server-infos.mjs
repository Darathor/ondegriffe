/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../tools/i18n.mjs";
import { Connection } from "../tools/connection.mjs";

let Vue = window.Vue;

Vue.component('core-header-server-info', {
	data() {
		return {
			Connection: Connection,
			I18n: I18n,
			rootUrl: window.location.origin,
			copyServerIdentifierToClipboard() {
				document.getElementById('server_identifier').select();
				document.execCommand('copy');
			},
			copyServerLinkToClipboard() {
				document.getElementById('server_link').select();
				document.execCommand('copy');
			}
		}
	},
	template: `<div class="core-header-server-info">
			<div class="header-item">
				{{ 'c.main.server_identifier' | trans(['lab']) }} <strong>{{ Connection.id }}</strong>
				<input type="text" :value="Connection.id" id="server_identifier" style="position: fixed; top: -500px;" />
				<button type="button" class="btn btn-icon" @click="copyServerIdentifierToClipboard()"
					v-tooltip="I18n.trans('c.main.copy_to_clipboard')">
					<img src="images/icons/copy.svg" class="icon" :alt="'c.main.copy_to_clipboard' | trans" />
				</button>
				<input type="text" :value="rootUrl + '/#/join/' + Connection.id" id="server_link" style="position: fixed; top: -500px;" />
				<button type="button" class="btn btn-icon" @click="copyServerLinkToClipboard()"
					v-tooltip="I18n.trans('c.main.copy_link_to_clipboard')">
					<img src="images/icons/copy-link.svg" class="icon" :alt="'c.main.copy_to_clipboard' | trans" />
				</button>
			</div>
		</div>`
});