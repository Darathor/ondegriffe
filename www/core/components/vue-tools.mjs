/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { StringUtils } from "../tools/string.mjs";
import { DateUtils } from "../tools/date.mjs";
import { I18n } from "../tools/i18n.mjs";

let Vue = window.Vue;

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
// Taken from https://davidwalsh.name/function-debounce
export function debounce(func, wait, immediate) {
	let timeout;
	return function() {
		let context = this, args = arguments;
		let later = function() {
			timeout = null;
			if (!immediate) {
				func.apply(context, args);
			}
		};
		let callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait || 500);
		if (callNow) {
			func.apply(context, args);
		}
	};
}

//region Filters.

Vue.filter('ucfirst', (value) => {
	return StringUtils.ucfirst(value);
});

Vue.filter('time', (value) => {
	return DateUtils.time(value);
});

Vue.filter('trans', (value, transformers, substitutions) => {
	return I18n.trans(value, transformers, substitutions);
});

//endregion