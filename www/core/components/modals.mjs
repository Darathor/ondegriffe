/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
let Vue = window.Vue;

Vue.component('core-modal', {
	props: ['title', 'subtitle', 'modalStyle'],
	mounted: function() {
		this.$refs.modal.focus();
	},
	template: `<transition name="modal">
			<div class="modal-mask" @click.stop="$emit(\'close\')">
				<div class="modal-wrapper">
					<div class="modal-container" :style="modalStyle" @click.stop="" @keyup.esc="$emit(\'close\')" tabindex="0" ref="modal">
						<div class="modal-header">
							<button class="btn" type="button" @click="$emit(\'close\')">
								<img src="images/icons/close.svg" class="icon" alt="" />
							</button>
							<h3>{{ title }} <small class="text-muted" v-if="subtitle">{{ subtitle }}</small></h3>
						</div>
						<div class="modal-body"><slot name="body">default body</slot></div>
					</div>
				</div>
			</div>
		</transition>`
});

Vue.component('core-confirm', {
	props: ['title', 'okLabel', 'koLabel'],
	template: `<transition name="modal">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<div class="modal-header">{{ title }} <button class="btn" type="button" @click="$emit(\'close\')"></button></div>
						<div class="modal-body">
							<slot name="body">
								default body
							</slot>
						</div>
						<div class="modal-footer">
							<slot name="footer">
								<button class="btn modal-ko-button" @click="$emit(\'close\')">{{ okLabel }}</button>
								<button class="btn modal-ok-button" @click="$emit(\'close\')">{{ koLabel }}</button>
							</slot>
						</div>
					</div>
				</div>
			</div>
		</transition>`
});