/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Alerts } from "../tools/alerts.mjs";
import { I18n } from "../tools/i18n.mjs";

let Vue = window.Vue;

Vue.component('core-alert-toaster', {
	data() {
		return {
			Alerts: Alerts,
			I18n: I18n
		}
	},
	template: `<div id="core-alert-toaster">
			<transition-group name="toaster-items" tag="div">
				<div class="toaster-item alert" v-for="item in Alerts.items" :key="item.id" :class="'alert--' + item.level">
					<img v-if="item.icon" :src="item.icon" class="toaster-item-icon" alt="" />
					<div class="toaster-item-content">
						{{ item.message | trans(['ucf'], item.substitutions) }}
					</div>
					<img src="images/icons/close.svg" class="toaster-item-close icon clickable" @click="Alerts.remove(item)" alt="x"
						v-tooltip="I18n.trans('c.main.remove_alert')" />
				</div>
			</transition-group>
		</div>`
});