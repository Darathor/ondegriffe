/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../tools/i18n.mjs";

export class Color {
	/**
	 * @param {string} code
	 * @param {string} cssCode
	 * @param {string} cssSecondary
	 */
	constructor(code, cssCode, cssSecondary) {
		this.code = code;
		this.cssCode = cssCode
		this.cssSecondary = cssSecondary;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('c.colors.' + this.code);
	}
}