/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Color } from "./color.mjs";

export class PlayerColor extends Color {
	/**
	 * @returns {Color|null}
	 */
	static getByCode(code) {
		for (let key in PlayerColors) {
			if (PlayerColors[key].code === code) {
				return PlayerColors[key];
			}
		}
		return null;
	}

	/**
	 * @param {BasePlayer[]} players
	 * @param {BasePlayer=} ignorePlayer
	 */
	isAvailable(players, ignorePlayer) {
		let available = true;
		players.forEach((player) => {
			if (player.color === this && ignorePlayer !== player) {
				available = false;
			}
		});
		return available;
	}
}

export let PlayerColors = {
	black: new PlayerColor('black', 'black', '#fff'),
	blue: new PlayerColor('blue', '#22e', '#fff'),
	green: new PlayerColor('green', 'green', '#fff'),
	red: new PlayerColor('red', '#dc143c', '#000'),
	grey: new PlayerColor('grey', 'grey', '#000'),
	violet: new PlayerColor('violet', '#9932cc', '#fff'),
	orange: new PlayerColor('orange', '#ff8c00', '#000'),
	pink: new PlayerColor('pink', '#ffc0cb', '#000'),
	white: new PlayerColor('white', 'white', '#000'),
	yellow: new PlayerColor('yellow', 'yellow', '#000')
};

/**
 * @typedef {Object} SerializedBasePlayer
 * @property {string} name
 * @property {string} color - The color code.
 * @property {string} peerId
 */

export class BasePlayer {
	/**
	 * @param {string=} name
	 * @param {PlayerColor=} color
	 * @param {string=} peerId
	 */
	constructor(name, color, peerId) {
		this.setNameAndPeerId(name, peerId);
		this.color = color;
	}

	/**
	 * Prepare object destruction, by deleting all references to other objects and calling free() method on sub-object that should be destroyed to.
	 */
	free() {
		delete this.color;
		PlayerDirectory.remove(this);
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return this._name;
	}

	/**
	 * @param {string} name
	 */
	set name(name) {
		PlayerDirectory.remove(this);
		this._name = name;
		PlayerDirectory.add(this);
	}

	/**
	 * @returns {string}
	 */
	get peerId() {
		return this._peerId;
	}

	/**
	 * @param {string|null} peerId
	 */
	set peerId(peerId) {
		PlayerDirectory.remove(this);
		this._peerId = peerId;
		PlayerDirectory.add(this);
	}

	/**
	 * @param {string} name
	 * @param {string|null} peerId
	 */
	setNameAndPeerId(name, peerId) {
		PlayerDirectory.remove(this);
		this._name = name;
		this._peerId = peerId;
		PlayerDirectory.add(this);
	}

	/**
	 * @return {SerializedPlayer}
	 */
	get data() {
		return {
			name: this.name,
			color: this.color.code,
			peerId: this.peerId
		};
	}

	/**
	 * @param {SerializedPlayer} data
	 */
	set data(data) {
		this.setNameAndPeerId(data.name, data.peerId);
		this.color = PlayerColors[data.color];
	}
}

export class PlayerDirectory {
	/** @type {Object} */
	static byName = {};

	/** @type {Object} */
	static byPeerId = {};

	/**
	 * @param {string} name
	 * @returns {BasePlayer|null}
	 */
	static getByName(name) {
		return this.byName[name] || null;
	}

	/**
	 * @param {string} peerId
	 * @returns {BasePlayer|null}
	 */
	static getByPeerId(peerId) {
		return this.byPeerId[peerId] || null;
	}

	/**
	 * @param {BasePlayer} player
	 */
	static add(player) {
		if (player.name) {
			this.byName[player.name] = player;
		}
		if (player.peerId) {
			this.byPeerId[player.peerId] = player;
		}
	}

	/**
	 * @param {BasePlayer} player
	 */
	static remove(player) {
		if (player.name) {
			delete this.byName[player.name];
		}
		if (player.peerId) {
			delete this.byPeerId[player.peerId];
		}
	}
}