/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

import { LocalStorage } from "../tools/local-storage.mjs";

export class BasePreferences {
	static storageKey = 'preferences';
	static instance;

	/**
	 * @returns {BasePreferences}
	 */
	static getInstance() {
		if (!this.instance) {
			throw new Error('Preferences are not instanced!');
		}
		return this.instance;
	}

	/** @type {boolean} */
	_playSounds = true;

	/** @type {boolean} */
	_collapseLeftSidebar = false;

	/** @type {boolean} */
	_collapseRightSidebar = false;

	/** @type {boolean} */
	_collapseAsideGameAdmin = false;

	/** @type {boolean} */
	_collapseAsidePreferences = false;

	/** @type {boolean} */
	_collapseAsideGameDebug = true;

	/** @type {boolean} */
	_collapseAsideTimeline = false;

	constructor() {
		this.load();
	}

	load() {
		if (LocalStorage.isReady()) {
			let data = LocalStorage.getObject(this.constructor.storageKey);
			if (data) {
				Object.keys(data).forEach((key) => {
					this[key] = data[key];
				});
			}
		}
		else {
			setTimeout(() => { this.load(); }, 10);
		}
	}

	save() {
		LocalStorage.setObject(this.constructor.storageKey, this.data);
	}

	/**
	 * @returns {Object}
	 */
	get data() {
		return {
			_playSounds: this._playSounds,
			_collapseLeftSidebar: this._collapseLeftSidebar,
			_collapseRightSidebar: this._collapseRightSidebar,
			_collapseAsideGameAdmin: this._collapseAsideGameAdmin,
			_collapseAsidePreferences: this._collapseAsidePreferences,
			_collapseAsideGameDebug: this._collapseAsideGameDebug,
			_collapseAsideTimeline: this._collapseAsideTimeline
		};
	}

	/**
	 * @returns {boolean}
	 */
	get playSounds() {
		return this._playSounds;
	}

	/**
	 * @param {boolean} value
	 */
	set playSounds(value) {
		this._playSounds = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseLeftSidebar() {
		return this._collapseLeftSidebar;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseLeftSidebar(value) {
		this._collapseLeftSidebar = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseRightSidebar() {
		return this._collapseRightSidebar;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseRightSidebar(value) {
		this._collapseRightSidebar = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsideGameAdmin() {
		return this._collapseAsideGameAdmin;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsideGameAdmin(value) {
		this._collapseAsideGameAdmin = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsidePreferences() {
		return this._collapseAsidePreferences;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsidePreferences(value) {
		this._collapseAsidePreferences = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsideGameDebug() {
		return this._collapseAsideGameDebug;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsideGameDebug(value) {
		this._collapseAsideGameDebug = value;
		this.save();
	}

	/**
	 * @returns {boolean}
	 */
	get collapseAsideTimeline() {
		return this._collapseAsideTimeline;
	}

	/**
	 * @param {boolean} value
	 */
	set collapseAsideTimeline(value) {
		this._collapseAsideTimeline = value;
		this.save();
	}
}