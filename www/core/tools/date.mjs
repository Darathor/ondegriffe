/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export let DateUtils = {
	timeFormat: new Intl.DateTimeFormat('fr-FR', { hour: 'numeric', minute: 'numeric', second: 'numeric' }),
	/**
	 * @param {Date} date
	 * @returns {string}
	 */
	time(date) {
		return this.timeFormat.format(date);
	},
	/**
	 * @param {Date} from
	 * @param {Date} to
	 * @returns number
	 */
	getSecondsDifference(from, to) {
		return Math.floor((to.getTime() - from.getTime()) / 1000);
	},
	/**
	 * @param {Date} from
	 * @param {Date} to
	 * @returns number
	 */
	getMinutesDifference(from, to) {
		return this.getSecondsDifference(from, to) / 60;
	}
};