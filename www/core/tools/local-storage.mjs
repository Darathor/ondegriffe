/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export let LocalStorage = {
	namespace: null,
	/**
	 * @param namespace
	 */
	setNamespace(namespace) {
		this.namespace = namespace;
	},
	/**
	 * @param {string} key
	 * @returns {boolean}
	 */
	isset(key) {
		return this.get(key) !== null;
	},
	/**
	 * @param {string} key
	 * @param {string} value
	 */
	set(key, value) {
		localStorage.setItem(this.namespace + '.' + key, value)
	},
	/**
	 * @param {string} key
	 * @param {Object} value
	 */
	setObject(key, value) {
		this.set(key, JSON.stringify(value));
	},
	/**
	 * @param {string} key
	 * @returns {string|null}
	 */
	get(key) {
		return localStorage.getItem(this.namespace + '.' + key);
	},
	/**
	 * @param {string} key
	 * @returns {Object|null}
	 */
	getObject(key) {
		let value = this.get(key);
		return value ? JSON.parse(value) : null;
	},
	/**
	 * @returns {boolean}
	 */
	isReady() {
		return !!this.namespace;
	}
};