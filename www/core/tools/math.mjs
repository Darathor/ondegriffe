/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export let MathUtils = {
	/**
	 * @param {number} min
	 * @param {number} max
	 * @returns {number}
	 */
	randomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	},

	/**
	 * @param {number} a
	 * @param {number} b
	 * @returns {number} a <=> b
	 */
	spaceship(a, b) {
		if (typeof(a) !== 'number') {
			a = 0;
		}
		if (typeof(b) !== 'number') {
			b = 0;
		}
		return Math.sign(a - b);
	}
};