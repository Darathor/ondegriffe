/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { MathUtils } from "./math.mjs";
import { DirectionEnum } from "../structures/enums.mjs";

export let ArrayUtils = {
	/**
	 * @param {Array} array
	 * @param {number} index
	 * @returns {number}
	 */
	nextIndex(array, index) {
		return (index + 1) < array.length ? (index + 1) : 0;
	},
	/**
	 * @param {Array} array
	 * @returns {number}
	 */
	randomIndex(array) {
		return MathUtils.randomInt(0, array.length);
	},
	/**
	 * @param {Array} array
	 * @returns {*}
	 */
	randomItem(array) {
		return array[MathUtils.randomInt(0, array.length)];
	},
	/**
	 * @param {Array} array1
	 * @param {Array} array2
	 * @returns {Array}
	 */
	merge(array1, array2) {
		for (let i = 0; i < array2.length; i++) {
			array1.push(array2[i]);
		}
		return array1;
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @returns {boolean}
	 */
	contains(array, item) {
		return array.indexOf(item) > -1;
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @returns {Array}
	 */
	remove(array, item) {
		let index = array.indexOf(item);
		if (index > -1) {
			array.splice(index, 1);
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @param {*} toReplace
	 * @param {*} newItem
	 * @returns {Array}
	 */
	replace(array, toReplace, newItem) {
		let index = array.indexOf(toReplace);
		if (index > -1) {
			array.splice(index, 1, [newItem]);
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	clear(array) {
		array.splice(0, array.length);
		return array;
	},
	/**
	 * @param {Array} array
	 * @param {number} i
	 * @param {number} j
	 * @returns {Array}
	 */
	switchItems: function(array, i, j) {
		let x = array[i];
		array[i] = array[j];
		array[j] = x;
		return array;
	},
	/**
	 * @param {Array} array
	 * @param {DirectionEnum} direction
	 * @returns {Array}
	 */
	circularPermutation(array, direction) {
		if (direction === DirectionEnum.LEFT) {
			for (let i = 0; i < (array.length - 1); i++) {
				this.switchItems(array, i, i + 1);
			}
		}
		else {
			for (let i = array.length - 1; i > 0; i--) {
				this.switchItems(array, i, i - 1);
			}
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	shuffle(array) {
		for (let i = array.length - 1; i > 0; i--) {
			this.switchItems(array, i, Math.floor(Math.random() * (i + 1)));
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	sortNumber(array) {
		array.sort(function(a, b) { return a - b; });
		return array;
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @param {boolean} circular
	 * @return {*|null}
	 */
	getLeftNeighbour(array, item, circular) {
		let length = array.length;
		if (length < 2) {
			return null;
		}

		let index = array.indexOf(item);
		if (index === -1) {
			return null;
		}

		if (index === 0) {
			return circular ? array[length - 1] : null;
		}
		else {
			return array[index - 1];
		}
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @param {boolean} circular
	 * @return {*|null}
	 */
	getRightNeighbour(array, item, circular) {
		let length = array.length;
		if (length < 2) {
			return null;
		}

		let index = array.indexOf(item);
		if (index === -1) {
			return null;
		}

		if (index === length - 1) {
			return circular ? array[0] : null;
		}
		else {
			return array[index + 1];
		}
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @param {boolean} circular
	 * @return {*[]}
	 */
	getNeighbours(array, item, circular) {
		let neighbours = [];
		let left = this.getLeftNeighbour(array, item, circular);
		if (left) {
			neighbours.push(left);
		}
		let right = this.getRightNeighbour(array, item, circular);
		if (right) {
			neighbours.push(right);
		}
		return neighbours;
	}
};