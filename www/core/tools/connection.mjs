/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Configuration } from '../../configuration.mjs';

export class Connection {
	static CODE_PING = 'Ping';
	static CODE_PONG = 'Pong';

	/** @type {string|null} */
	static id = null;

	/** @type {Connection} */
	static instance = null;

	/** @type {Promise} */
	static promise = null;

	/**
	 * @returns {Promise}
	 */
	static startServer() {
		initPeer().then(() => {
			Connection.instance = new Server();
		});
		return Connection.promise;
	}

	/**
	 * @param {number} serverId
	 * @returns {Promise}
	 */
	static startClient(serverId) {
		initPeer().then(() => {
			Connection.instance = new Client(serverId);
		});
		return Connection.promise;
	}

	/**
	 * @returns {boolean}
	 */
	static isReady() {
		return !!(Connection.instance && Connection.instance.ready);
	}

	/**
	 * @returns {boolean}
	 */
	static isServer() {
		return !!(Connection.instance && Connection.instance.mode === 'server');
	}

	/**
	 * @returns {boolean}
	 */
	static isClient() {
		return !!(Connection.instance && Connection.instance.mode === 'client');
	}

	/**
	 * @param {string} code
	 * @param {Object} data
	 * @param {string=} excludedPeerId
	 */
	static sendMessage(code, data, excludedPeerId) {
		if (Connection.instance) {
			Connection.instance.sendMessage(code, data, excludedPeerId);
		}
	}

	/**
	 * @param {DataConnection} connection
	 * @param {string} code
	 * @param {Object} data
	 */
	static sendMessageToTarget(connection, code, data) {
		if (!Connection.isReady()) {
			console.log('[Connection.sendMessageToTarget] Connection not ready, wait for 200ms.');
			setTimeout(() => {
				this.sendMessageToTarget(connection, code, data);
			}, 200);
			return;
		}
		console.log('[Connection.sendMessageToTarget] Connection ready, send message:', connection.peer, code, data);
		connection.send(JSON.stringify({ code: code, data: data }));
	}

	/** @type boolean */
	ready = false;

	/** @type string */
	mode;

	/**
	 * @param {string} code
	 * @param {Object} data
	 * @param {string=} excludedPeerId
	 */
	sendMessage(code, data, excludedPeerId) {
		// To be overridden.
	}

	/**
	 * @param {DataConnection} connection
	 * @param {string} mode
	 * @param {string} data
	 */
	onMessageReceived(connection, mode, data) {
		data = JSON.parse(data);
		console.log('[Connection.onMessageReceived]', connection, mode, data);
		if (data.code === Connection.CODE_PING) {
			Connection.sendMessageToTarget(connection, Connection.CODE_PONG, {});
		}
		else if (data.code === Connection.CODE_PONG) {
			console.log('[Connection.onMessageReceived]', 'PONG received from peer', connection.peer, data.data);
		}
		else {
			document.dispatchEvent(new CustomEvent('coreConnection' + data.code, {
				detail: { mode: 'client', data: data.data, peerId: connection.peer, connection: connection }
			}));
		}
	}

	ping() {
		this.sendMessage(Connection.CODE_PING, {});
	}
}

let peer;

function initPeer() {
	return Connection.promise = new Promise((resolve) => {
		peer = new peerjs.Peer(null, Configuration.peerjs);
		peer.on('open', function(id) {
			console.log('My peer ID is:', id);
			Connection.id = id;
			resolve(id);
		});
	});
}

class Server extends Connection {
	/** @type DataConnection[] */
	clients = [];

	constructor() {
		super();
		this.mode = 'server';

		peer.on('connection', (connection) => {
			this.clients.push(connection);
			console.log('[Server.start]', 'Client connection:', connection.peer, connection);

			connection.on('data', (data) => {
				this.onMessageReceived(connection, 'server', data);
			});

			setTimeout(() => {
				document.dispatchEvent(new CustomEvent('coreConnectionNewClient', { detail: { peerId: connection.peer, connection: connection } }));
			}, 500);
		});

		this.ready = true;
	}

	/**
	 * @param {string} code
	 * @param {Object} data
	 * @param {string=} excludedPeerId
	 */
	sendMessage(code, data, excludedPeerId) {
		super.sendMessage(code, data);
		this.clients.forEach((client) => {
			if (client.peer !== excludedPeerId) {
				Connection.sendMessageToTarget(client, code, data);
			}
		});
	}

	/**
	 * @param {string} peerId
	 */
	closeConnection(peerId) {
		this.clients.forEach((client, index) => {
			if (client.peer === peerId) {
				client.close();
				this.clients.splice(index, 1);
			}
		});
	}
}

class Client extends Connection {
	/** @type DataConnection */
	server = null;

	/**
	 * @param {number} serverId
	 */
	constructor(serverId) {
		super();
		this.mode = 'client';
		this.server = peer.connect(serverId);
		console.log('[Client.start]', 'Connected to:', serverId);

		this.server.on('data', (data) => {
			this.onMessageReceived(this.server, 'client', data);
		});

		this.server.on('open', () => {
			this.ready = true;
		});
	}

	/**
	 * @param {string} code
	 * @param {Object} data
	 * @param {string=} excludedPeerId
	 */
	sendMessage(code, data, excludedPeerId) {
		super.sendMessage(code, data);
		Connection.sendMessageToTarget(this.server, code, data);
	}
}