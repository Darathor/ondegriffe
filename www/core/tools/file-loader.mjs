/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export class FileLoader {
	constructor() {
		this.requests = 0;
	}

	/**
	 * @param {string} filePath
	 * @param {function} fileCallback
	 * @param {function} endCallback
	 */
	loadFile(filePath, fileCallback, endCallback) {
		this.requests++;
		fetch(filePath).then((response) => {
			if (response.ok) {
				response.json().then((data) => {
					fileCallback(data);
					this.requests--;
					if (!this.requests) {
						endCallback();
					}
				});
			}
			else {
				console.warn('filePath not found:', filePath);
				this.requests--;
				if (!this.requests) {
					endCallback();
				}
			}
		});
	}
}