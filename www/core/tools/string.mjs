/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export let StringUtils = {
	/**
	 * @param {string} value
	 * @returns {string}
	 */
	ucfirst(value) {
		if (!value) {
			return '';
		}
		value = value.toString();
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
};