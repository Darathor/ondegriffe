/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { FileLoader } from "./file-loader.mjs";

export let I18n = {
	LCID: 'fr_FR',
	keys: {},
	keysByLCID: {},
	registeredFilePaths: [
		{ prefix: 'c.colors.', path: 'core/i18n/colors/' }
	],
	/**
	 * @param {Object[]} filePaths
	 */
	registerFilePaths(filePaths) {
		filePaths.forEach((item) => {
			this.registeredFilePaths.push(item);
		});
	},
	/**
	 * @param {string} LCID
	 * @param {function} endCallback
	 */
	init(LCID, endCallback) {
		this.LCID = LCID;
		if (this.keysByLCID[LCID]) {
			this.keys = this.keysByLCID[LCID];
			console.log('Language set to ' + this.LCID + ' (from cache)');
			return;
		}

		this.keysByLCID[LCID] = {};
		let loader = new FileLoader();
		let _endCallback = () => {
			this.keys = this.keysByLCID[LCID];
			console.log('Language set to ' + this.LCID);
			endCallback();
		}
		this.registeredFilePaths.forEach((item) => {
			loader.loadFile(item.path + LCID + '.json', (data) => { this.importPackage(data, item.prefix); }, _endCallback);
		});
	},
	switchLCID() {
		this.init(this.LCID === 'fr_FR' ? 'en_US' : 'fr_FR', () => {});
	},
	/**
	 * @param {string} prefix
	 * @param {string} path
	 */
	registerFilePath(prefix, path) {
		this.registeredFilePaths.push({ prefix: prefix, path: path });
	},
	/**
	 * @param {Object} data
	 * @param {string} prefix
	 */
	importPackage(data, prefix) {
		Object.keys(data).map((key) => {
			if (!('message' in data[key])) {
				console.warn('Ignore key declaration without message:', key);
				return;
			}
			let fullKey = prefix + key;
			if (fullKey in this.keysByLCID[this.LCID]) {
				console.warn('Ignore duplicate key declaration:', key);
				return;
			}
			this.keysByLCID[this.LCID][fullKey] = data[key].message;
		});
	},
	/**
	 * @param {string} key
	 * @param {string[]=} transformers
	 * @param {Object=} substitutions
	 * @returns {string}
	 */
	trans(key, transformers, substitutions) {
		let text = this.keys[key] || key;
		if (substitutions) {
			Object.keys(substitutions).forEach((key) => {
				text = text.replace('$' + key + '$', substitutions[key]);
			});
		}
		if (transformers) {
			transformers.forEach((transformer) => {
				if (transformer === 'etc') {
					text += '…';
				}
				else if (transformer === 'lab') {
					text += this.LCID === 'fr_FR' ? ' :' : ':';
				}
			});
		}
		return text;
	}
};