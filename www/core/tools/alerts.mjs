/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

export class Alert {
	//region static stuff
	static INFO = 'info';
	static SUCCESS = 'success';
	static WARNING = 'warning';
	static DANGER = 'danger';
	//endregion

	/**
	 * @param {string} id
	 * @param {string} level
	 * @param {string} message The message's i18n key.
	 * @param {Object=} substitutions
	 * @param {string=} icon
	 */
	constructor(id, level, message, substitutions,icon) {
		this.id = id;
		this.level = level;
		this.message = message;
		this.substitutions = substitutions;
		this.icon = icon;
	}
}

export let Alerts = {
	/** @var Object[] */
	items: [],
	/** @var number */
	defaultTimeout: 5000,

	success(message, option = {}) {
		option.level = Alert.SUCCESS;
		this.add(message, option);
	},
	info(message, option = {}) {
		option.level = Alert.INFO;
		this.add(message, option);
	},
	warning(message, option = {}) {
		option.level = Alert.WARNING;
		this.add(message, option);
	},
	danger(message, option = {}) {
		option.level = Alert.DANGER;
		this.add(message, option);
	},
	add(message, { level, substitutions, timeout, sound, icon }) {
		let item = new Alert(`${Date.now()}-${Math.random()}`, level, message, substitutions, icon);
		this.items.push(item);
		setTimeout(() => this.remove(item), timeout || this.defaultTimeout);
		if (sound) {
			sound.play();
		}
	},
	remove(item) {
		let i = this.items.indexOf(item);
		if (i >= 0) {
			this.items.splice(i, 1);
		}
	}
};