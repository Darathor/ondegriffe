/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

class Sound {
	constructor(url) {
		this.url = url;
		this.audio = new Audio(url);
	}

	/**
	 * @returns {Promise<void>}
	 */
	play() {
		this.audio.pause();
		this.audio.currentTime = 0;
		return this.audio.play().catch((e) => {
			console.warn('Can\'t play sound', e);
		});
	}
}

export class BaseSounds {
	static startTurn = new Sound('../../sounds/start-turn.mp3');
	static startPhase = new Sound('../../sounds/start-phase.mp3');
	static startGame = new Sound('../../sounds/start-game.mp3');
	static endGame = new Sound('../../sounds/end-game.mp3');
	static newAlert = new Sound('../../sounds/new-alert.mp3');
}