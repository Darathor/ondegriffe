/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Setup } from '../services/setup.mjs';
import { Models } from '../services/models.mjs';
import { Preferences } from "../services/preferences.mjs";
import { I18n } from "../core/tools/i18n.mjs";
import { PlayerColors } from "../core/structures/player.mjs";

let Vue = window.Vue;

Vue.component('tc-setup-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Preferences: Preferences.getInstance()
		}
	},
	template: `<div class="tc-setup-setup panel">
			<div class="panel-body">
				<p v-if="Setup.hasLast && !client">
					<button type="button" class="btn btn--block" @click="Setup.loadLast()">{{ 'c.setup.load_last_configuration' | trans }}</button>
				</p>
				<label class="no-margin-bottom">
					<input type="checkbox" class="switch" v-model="Preferences.setupHideDisabledItems" />
					{{ 'c.setup.hide_disabled_items' | trans }}
				</label>
			</div>
		</div>`
});

Vue.component('tc-players-setup', {
	props: ['peerId', 'server'],
	data: function() {
		return {
			Setup: Setup,
			Colors: PlayerColors,
			I18n: I18n
		}
	},
	template: `<div class="tc-players-setup panel">
			<h2 class="panel-header">{{ 'c.setup.players' | trans }}</h2>
			<div class="panel-body">
				<table>
					<thead>
						<tr>
							<th class="w150p">{{ 'c.setup.player_name' | trans }}</th>
							<th class="w150p">{{ 'c.setup.player_color' | trans }}</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr class="player-item" v-for="player in Setup.players">
							<td><input type="text" v-model.trim="player.name" :disabled="player.peerId && player.peerId !== peerId" maxlength="30" /></td>
							<td>
								<select v-model="player.color" :disabled="player.peerId && player.peerId !== peerId">
									<option v-for="color in Colors" :value="color" :style="{ color: color.cssCode, background: color.cssSecondary }"
										v-if="color.isAvailable(Setup.players, player)">{{ color.name }}</option>
								</select>
							</td>
							<td>
								<button v-if="!peerId || (server && peerId !== player.peerId)" type="button" class="btn" @click="Setup.removePlayer(player)"
									:disabled="Setup.players.length === 1 || (peerId && peerId === player.peerId)">
									<img src="images/icons/delete.svg" class="icon-sm" :alt="'c.setup.delete' | trans"
										v-tooltip="I18n.trans('c.setup.delete_player')" />
								</button>
								<button v-if="peerId && peerId === player.peerId" type="button" class="btn" @click="Setup.savePlayerData(player)">
									<img src="images/icons/save.svg" class="icon-sm" :alt="'c.setup.save' | trans"
										v-tooltip="I18n.trans('c.setup.save_data_for_next_games')" />
								</button>
							</td>
						</tr>
					</tbody>
				</table>
				<button type="button" class="btn" @click="Setup.addPlayer()" v-if="!peerId">
					<img src="images/icons/add.svg" class="icon-sm" alt="" /> {{ 'c.setup.add_player' | trans }}
				</button>
			</div>
		</div>`
});

Vue.component('tc-pool-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-pool-setup panel">
			<h2 class="panel-header">{{ 'c.setup.cards_pool' | trans }}</h2>
			<div class="panel-body">
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.poolType" value="selection" :disabled="client" />
						{{ 'c.setup.selected_quantities' | trans }} ({{ Setup.selectedCardModels.length }})
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.poolType" value="random" :disabled="client" />
						{{ 'c.setup.random_from_selection' | trans }}
					</label>
					<label>
						({{ 'c.setup.quantity' | trans }}
						<input type="number" v-model.number="Setup.poolSize" min="2" max="999" step="1" size="3" :disabled="client || Setup.poolType !== 'random'" />)
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('og-tile-pool-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="og-tile-pool-setup panel">
			<h2 class="panel-header">{{ 'c.setup.tiles_pool' | trans }}</h2>
			<div class="panel-body">
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.tilePoolType" value="selection" :disabled="client" />
						{{ 'c.setup.selected_quantities' | trans }} ({{ Setup.selectedScoreTileModels.length }})
					</label>
				</div>
				<div>
					<label>
						<input type="radio" class="radio" v-model="Setup.tilePoolType" value="random" :disabled="client" />
						{{ 'c.setup.random_from_selection' | trans }}
					</label>
					<label>
						({{ 'c.setup.quantity' | trans }}
						<input type="number" v-model.number="Setup.tilePoolSize" min="2" max="999" step="1" size="3" :disabled="client || Setup.tilePoolSize !== 'random'" />)
					</label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-optional-rules-setup', {
	props: ['client'],
	data: function() {
		return {
			Rules: Setup.optionalRules,
		}
	},
	template: `<div class="tc-optional-rules-setup panel">
			<h2 class="panel-header">{{ 'c.setup.optional_rules' | trans }}</h2>
			<div class="panel-body">
				<label>
					<input type="checkbox" class="switch" v-model="Rules.extraTurnWhenFillHole" :disabled="client" />
					{{ 'c.setup.extra_turn_when_fill_hole' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.allowToGetBackPawn" :disabled="client" />
					{{ 'c.setup.allow_to_take_back_pawn' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.allowAddCardToHand" :disabled="client" />
					{{ 'c.setup.allow_add_card_to_hand' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.allowPoolDetailView" :disabled="client" />
					{{ 'c.setup.allow_pool_detail_view' | trans }}
				</label>
				<label>
					<input type="checkbox" class="switch" v-model="Rules.showMessagesEstimation" :disabled="client" />
					{{ 'c.setup.show_messages_estimation' | trans }}
				</label>
			</div>
		</div>`
});

Vue.component('tc-setup-card-item', {
	props: ['client', 'item'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			Preferences: Preferences.getInstance()
		}
	},
	template: `<div class="tc-setup-card-item item" v-show="item.enabled || !Preferences.setupHideDisabledItems" :class="{ selected: item.selected }">
			<tc-card-thumbnail :model="item.model"></tc-card-thumbnail>
			<div class="item-configurator">
				<label class="item-selector text-muted">
					<input type="checkbox" class="checkbox" v-model="item.selected" :disabled="!item.enabled || client" /> {{ item.model.code }}
				</label>
				<input class="selected-quantity" type="number" v-model.number="item.quantity" min="1" max="99" step="1" size="2"
					:disabled="!item.selected || client" />
			</div>
		</div>`
});

Vue.component('tc-setup-category-item', {
	props: ['client', 'item', 'mode'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n,
			selectAll: () => {
				switch (this.mode) {
					case 'cards':
						Setup.selectCardsWithCategory(this.item.model);
						break;
					case 'scoreTiles':
						Setup.selectScoreTilesWithCategory(this.item.model);
						break;
				}
			},
			deselectAll: () => {
				switch (this.mode) {
					case 'cards':
						Setup.deselectCardsWithCategory(this.item.model);
						break;
					case 'scoreTiles':
						Setup.deselectScoreTilesWithCategory(this.item.model);
						break;
				}
			}
		}
	},
	computed: {
		selectedCount() {
			let count = 0;
			if (this.item.enabled) {
				this.item[this.mode].forEach((itemData) => {
					if (itemData.selected) {
						count += itemData.quantity;
					}
				});
			}
			return count;
		},
		totalCount() {
			return this.item[this.mode].length;
		}
	},
	template: `<div class="item" :class="{ selected: item.enabled }">
			<tc-category-model-summary :model="item.model"></tc-category-model-summary>
			<label class="item-selector">
				<input class="checkbox" type="checkbox" v-model="item.enabled" :change="Setup.refreshCategory(item)" :disabled="client"
					v-tooltip="I18n.trans('c.setup.uncheck_to_disable_category')" />
				<span>{{ selectedCount }}<small class="text-muted">/{{ totalCount }}</small></span>
			</label>
			<div v-if="!client" class="mass-selection">
				<button type="button" class="btn" @click="selectAll()" :disabled="!item.enabled"
					v-tooltip="I18n.trans('c.setup.select_all_this_category')">
					<img src="images/icons/select-all.svg" class="icon-sm" alt="" />
				</button>
				<button type="button" class="btn" @click="deselectAll()" :disabled="!item.enabled"
					v-tooltip="I18n.trans('c.setup.deselect_all_this_category')">
					<img src="images/icons/deselect-all.svg" class="icon-sm" alt="" />
				</button>
			</div>
		</div>`
});

Vue.component('tc-cards-list-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-cards-list-setup setup-list panel">
			<h2 class="panel-header">
				{{ 'c.setup.choose_cards' | trans }} ({{ Setup.selectedCardModels.length }})
				<span v-if="!client">
					<button type="button" class="btn btn-icon" @click="Setup.selectAllCards()" v-tooltip="I18n.trans('c.setup.select_all_cards')">
						<img src="images/icons/select-all.svg" class="icon" alt="" />
					</button>
					<button type="button" class="btn btn-icon" @click="Setup.deselectAllCards()" v-tooltip="I18n.trans('c.setup.deselect_all_cards')">
						<img src="images/icons/deselect-all.svg" class="icon" alt="" />
					</button>
				</span>
			</h2>
			<div class="panel-body">
				<div>
					<h3>{{ 'c.setup.selection_by_category_type' | trans }}</h3>
					<div class="categories">
						<tc-setup-category-item v-for="item in Setup.categories" :key="item.code" :client="client" :item="item" :mode="'cards'">
						</tc-setup-category-item>
					</div>
				</div>
				<div v-for="extensionData in Setup.selection" v-if="extensionData.cards.length" class="extension">
					<h3>
						<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
						{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
						<span v-if="!client">
							<button type="button" class="btn btn-icon" @click="Setup.selectExtensionCards(extensionData)"
								v-tooltip="I18n.trans('c.setup.select_all_this_extension')">
								<img src="images/icons/select-all.svg" class="icon" alt="" />
							</button>
							<button type="button" class="btn btn-icon" @click="Setup.deselectExtensionCards(extensionData)"
								v-tooltip="I18n.trans('c.setup.deselect_all_this_extension')">
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />
							</button>
						</span>
					</h3>
					<div class="cards">
						<tc-setup-card-item v-for="item in extensionData.cards" :key="item.code" :item="item" :client="client"></tc-setup-card-item>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-setup-score-tile-item', {
	props: ['client', 'item'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			Preferences: Preferences.getInstance()
		}
	},
	template: `<div class="tc-setup-score-tile-item item" v-show="item.enabled || !Preferences.setupHideDisabledItems"
			:class="{ selected: item.selected }">
			<og-score-tile-thumbnail :model="item.model"></og-score-tile-thumbnail>
			<div class="item-configurator">
				<label class="item-selector text-muted">
					<input type="checkbox" class="checkbox" v-model="item.selected" :disabled="!item.enabled || client" /> {{ item.model.code }}
				</label>
				<input class="selected-quantity" type="number" v-model.number="item.quantity" min="1" max="99" step="1" size="2"
					:disabled="!item.selected || client" />
			</div>
		</div>`
});

Vue.component('tc-score-tiles-list-setup', {
	props: ['client'],
	data: function() {
		return {
			Setup: Setup,
			Models: Models,
			I18n: I18n
		}
	},
	template: `<div class="tc-score-tiles-list-setup setup-list panel">
			<h2 class="panel-header">
				{{ 'c.setup.choose_score_tiles' | trans }} ({{ Setup.selectedScoreTileModels.length }})
				<span v-if="!client">
					<button type="button" class="btn btn-icon" @click="Setup.selectAllScoreTiles()" 
						v-tooltip="I18n.trans('c.setup.select_all_score_tiles')">
						<img src="images/icons/select-all.svg" class="icon" alt="" />
					</button>
					<button type="button" class="btn btn-icon" @click="Setup.deselectAllScoreTiles()" 
						v-tooltip="I18n.trans('c.setup.deselect_all_score_tiles')">
						<img src="images/icons/deselect-all.svg" class="icon" alt="" />
					</button>
				</span>
			</h2>
			<div class="panel-body">
				<div>
					<h3>{{ 'c.setup.selection_by_category_type' | trans }}</h3>
					<div class="categories">
						<tc-setup-category-item v-for="item in Setup.categories" v-if="item.scoreTiles.length" :key="item.code" :client="client"
							:item="item" :mode="'scoreTiles'">
						</tc-setup-category-item>
					</div>
				</div>
				<div v-for="extensionData in Setup.selection" v-if="extensionData.scoreTiles.length" class="extension">
					<h3>
						<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
						{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
						<span v-if="!client">
							<button type="button" class="btn btn-icon" @click="Setup.selectExtensionScoreTiles(extensionData)"
								v-tooltip="I18n.trans('c.setup.select_all_this_extension')">
								<img src="images/icons/select-all.svg" class="icon" alt="" />
							</button>
							<button type="button" class="btn btn-icon" @click="Setup.deselectExtensionScoreTiles(extensionData)"
								v-tooltip="I18n.trans('c.setup.deselect_all_this_extension')">
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />
							</button>
						</span>
					</h3>
					<div class="score-tiles">
						<tc-setup-score-tile-item v-for="item in extensionData.scoreTiles" :key="item.code" :item="item" :client="client">
						</tc-setup-score-tile-item>
					</div>
				</div>
			</div>
		</div>`
});