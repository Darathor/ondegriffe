/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core/tools/i18n.mjs";
import { Preferences } from "../services/preferences.mjs";

let Vue = window.Vue;

Vue.component('tc-header-preferences', {
	data() {
		return {
			Preferences: Preferences.getInstance(),
			I18n: I18n
		};
	},
	template: `<div id="tc-header-preferences">
			<div class="header-item header-item-clickable">
				<img src="images/icons/language.svg" class="icon clickable" alt="'c.main.language' | trans"
				v-tooltip="I18n.trans('c.main.language_button_tooltip')" @click="I18n.switchLCID()" />
			</div>
			<div class="header-item header-item-clickable">
				<img v-if="Preferences.playSounds" src="images/icons/volume-up.svg" class="icon clickable" :alt="'c.main.sounds_enabled' | trans"
					v-tooltip="I18n.trans('c.main.disable_sounds')" @click="Preferences.playSounds = false" />
				<img v-if="!Preferences.playSounds" src="images/icons/volume-mute.svg" class="icon clickable" :alt="'c.main.sounds_disabled' | trans"
					v-tooltip="I18n.trans('c.main.enable_sounds')" @click="Preferences.playSounds = true" />
			</div>
		</div>`
});

Vue.component('tc-current-time', {
	data() {
		setInterval(() => {
			let date = new Date();
			this.currentTime = ('' + date.getHours()).padStart(2, '0') + ':' + ('' + date.getMinutes()).padStart(2, '0') + ':' +
				('' + date.getSeconds()).padStart(2, '0');
		}, 1000);
		return {
			currentTime: ''
		}
	},
	template: `<div id="tc-current-time">
			<div class="header-item"><img src="images/icons/clock.svg" class="icon" alt="" /> {{ currentTime }}</div>
		</div>`
});