/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core/tools/i18n.mjs";
import { Models } from '../services/models.mjs';
import { StringUtils } from "../core/tools/string.mjs";

let Vue = window.Vue;

Vue.component('tc-models-infos-modals', {
	data: function() {
		return {
			Details: Models.Details
		}
	},
	template: `<div class="tc-models-infos-modals">
			<core-modal v-if="Details.card" :title="'c.main.card_details' | trans" :subtitle="Details.card.code"
				modal-style="width: 1010px;" @close="Details.card = null">
				<div slot="body">
					<tc-card-description :card="Details.card"></tc-card-description>
				</div>
			</core-modal>
			<core-modal v-if="Details.scoreTile" :title="'c.main.score_tile_details' | trans" :subtitle="Details.scoreTile.code"
				modal-style="width: 810px;" @close="Details.scoreTile = null">
				<div slot="body">
					<tc-score-tile-description :tile="Details.scoreTile"></tc-score-tile-description>
				</div>
			</core-modal>
		</div>`
});

Vue.component('og-category-thumbnail', {
	props: ['code', 'model'],
	data() {
		return {
			item: this.model ? this.model : Models.getCategoryByCode(this.code)
		};
	},
	watch: {
		code() {
			this.item = this.model ? this.model : Models.getCategoryByCode(this.code);
		},
		model() {
			this.item = this.model ? this.model : Models.getCategoryByCode(this.code);
		}
	},
	computed: {
		tooltip() {
			return `<div class="og-category-thumbnail-tooltip">
					<div class="category category-100"><img src="` + this.item.src + `" alt="` + this.item.code + `" /></div>
					<div class="item-name">` + StringUtils.ucfirst(this.item.name) + `</div>
					<small class="item-code text-muted">` + this.item.code + `</small>
				</div>`;
		}
	},
	template: `<span class="og-category-thumbnail">
			<img :src="item.src" :alt="item.code" v-tooltip="tooltip" />
		</span>`
});

Vue.component('tc-category-model-summary', {
	props: ['code', 'model'],
	data() {
		return {
			item: this.model ? this.model : Models.getCategoryByCode(this.code)
		};
	},
	watch: {
		code() {
			this.item = this.model ? this.model : Models.getCategoryByCode(this.code);
		},
		model() {
			this.item = this.model ? this.model : Models.getCategoryByCode(this.code);
		}
	},
	template: `<span class="tc-category-model-summary">
			<span class="category category-30"><img :src="item.src" :alt="item.code" /></span>
			<span class="item-name">{{ item.name | ucfirst }}</span>
			<small class="item-code text-muted">{{ item.code }}</small>
		</span>`
});

Vue.component('tc-card-thumbnail', {
	props: ['code', 'model', 'showCode', 'showDetails'],
	data: function() {
		return {
			I18n: I18n,
			card: this.model ? this.model : Models.getCardByCode(this.code),
			showCard() {
				Models.Details.card = this.card;
			}
		}
	},
	watch: {
		code() {
			this.card = this.model ? this.model : Models.getCardByCode(this.code);
		},
		model() {
			this.card = this.model ? this.model : Models.getCardByCode(this.code);
		}
	},
	computed: {
		categories() {
			let categories = [];
			if (this.card) {
				this.card.categories.forEach((code) => {
					categories.push(Models.getCategoryByCode(code));
				});
			}
			return categories;
		},
		authors() {
			let authors = [];
			if (this.card) {
				this.card.authors.forEach((code) => {
					let author = Models.getAuthorByCode(code);
					if (author) {
						authors.push(author.name);
					}
				});
			}
			return authors.join(' & ');
		}
	},
	template: `<div class="tc-card-thumbnail">
			<div class="card" :style="{ 'background-image': 'url(' + card.src + ')', 'background-position': card.position }">
				<div class="actions">
					<button class="btn btn-icon-sm" @click="showCard()" v-tooltip="I18n.trans('c.main.click_to_show_card_details')">
						<img src="images/icons/info.svg" class="icon" alt="?" />
					</button>
				</div>
				<div class="card-categories">
					<span v-for="category in categories" class="category category-30">
						<og-category-thumbnail :model="category"></og-category-thumbnail>
					</span>
				</div>
				<div class="meta-data">
					<span calss="card-authors" v-tooltip="I18n.trans('c.main.authors')">{{ authors }}</span>
					<span class="card-code">{{ card.code }}</span>
				</div>
			</div>
		</div>`
});

Vue.component('tc-card-description', {
	props: ['card'],
	data: function() {
		return {
			feature: null,
			I18n: I18n
		}
	},
	computed: {
		categories() {
			let categories = [];
			if (this.card) {
				this.card.categories.forEach((code) => {
					categories.push(Models.getCategoryByCode(code));
				});
			}
			return categories;
		},
		authors() {
			let authors = [];
			if (this.card) {
				this.card.authors.forEach((code) => {
					authors.push(Models.getAuthorByCode(code));
				});
			}
			return authors;
		}
	},
	template: `<div class="tc-card-description flex-horizontal">
			<div class="visual-container" v-if="card">
				<img :src="card.src" alt="" />
			</div>
			<div class="data-container" v-if="card">
				<h2>{{ card.name }} <small class="text-muted">{{ card.year }}</small></h2>
				<dl>
					<dt>{{ 'c.main.authors' | trans }}</dt>
					<dd v-for="author in authors">
						{{ author.name }} <small class="text-muted">{{ author.birthYear || '?' }} - {{ author.deathYear || '?' }}</small>
					</dd>
					<dt>{{ 'c.main.categories' | trans }}</dt>
					<dd v-for="category in categories">
						<tc-category-model-summary :model="category"></tc-category-model-summary>
					</dd>
				</dl>
			</div>
		</div>`
});

Vue.component('og-score-tile-thumbnail', {
	props: ['code', 'model', 'type'],
	data() {
		return {
			item: this.model ? this.model : Models.getScoreTileByCode(this.code),
			I18n: I18n,
			showTile() {
				Models.Details.scoreTile = this.item;
			}
		};
	},
	watch: {
		code() {
			this.item = this.model ? this.model : Models.getScoreTileByCode(this.code);
		},
		model() {
			this.item = this.model ? this.model : Models.getScoreTileByCode(this.code);
		}
	},
	template: `<div class="og-score-tile-thumbnail">
			<div class="container" :class="'type-' + (this.type || 'global')">
				<div class="content" :style="{ 'background-image': 'url(' + item.src + ')' }">
					<div class="actions">
						<button class="btn btn-icon-sm" @click="showTile()" v-tooltip="I18n.trans('c.main.click_to_show_score_tile_details')">
							<img src="images/icons/info.svg" class="icon" alt="?" />
						</button>
					</div>
					<div class="meta-data">
						<small class="item-code text-muted">{{ item.code }}</small>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-score-tile-description', {
	props: ['tile'],
	data: function() {
		return {
			feature: null,
			I18n: I18n
		}
	},
	template: `<div class="tc-score-tile-description flex-horizontal">
			<div class="visual-container" v-if="tile">
				<img :src="tile.src" alt="" />
			</div>
			<div class="data-container" v-if="tile">
				<h2>{{ 'c.main.effect' | trans }}</h2>
				<div>{{ tile.description }}</div>
			</div>
		</div>`
});