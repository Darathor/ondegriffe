/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Connection } from '../core/tools/connection.mjs';
import { System } from "../core/tools/system.mjs";
import { I18n } from "../core/tools/i18n.mjs";
import { Models } from '../services/models.mjs';
import { game } from '../services/game.mjs';
import { Timeline } from "../services/core.mjs";
import { Phase } from "../services/phase.mjs";
import { DraftPhase } from "../services/phases/draft.mjs";
import { ScorePhase } from "../services/phases/score.mjs";
import { DirectionEnum } from "../core/structures/enums.mjs";
import { ArrayUtils } from "../core/tools/array.mjs";
import { MathUtils } from "../core/tools/math.mjs";

let Vue = window.Vue;

Vue.component('tc-players', {
	data() {
		return {
			Connection: Connection,
			game: game,
			I18n: I18n,
			Preferences: game.preferences
		};
	},
	template: `<div class="tc-players aside">
			<div class="aside-title">
				{{ 'c.main.players' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsidePlayers" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsidePlayers = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsidePlayers" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsidePlayers = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsidePlayers ? 'collapsed': ''">
				<ul>
					<li v-for="(player, index) in game.players" :class="{ current: index === game.currentPlayerIndex, self: Connection.id && Connection.id === player.peerId }">
						<tc-player :player="player" :index="index"></tc-player>
					</li>
				</ul>
			</div>
		</div>`
});

Vue.component('tc-player', {
	props: ['player', 'index'],
	data() {
		return {
			game: game,
			Models: Models,
			I18n: I18n,
			showTileDetails(tile) {
				Models.Details.tile = tile.model;
			},
			waitingForActionTooltip: I18n.trans('c.main.waiting_for_action')
		};
	},
	computed: {
		waitingForAction() {
			return (game.currentPhase instanceof DraftPhase) && !game.currentPhase.roundState.playerChoices[this.player.name];
		}
	},
	template: `<div class="tc-player">
			<span class="bullet" :style="{background: player.color.cssCode, border: '1px solid ' + player.color.cssSecondary }"></span>
			<span class="name">{{ player.name }}</span>
			<span class="text-muted">–</span>
			{{ player.points }} <img src="../images/elements/point.svg" class="icon-points" :alt="'c.main.point_s' | trans" />
			<img v-if="waitingForAction" src="../images/elements/waiting-for-action.svg" class="icon-waiting"
				:alt="waitingForActionTooltip" v-tooltip="waitingForActionTooltip" />
		</div>`
});

Vue.component('tc-timeline', {
	data() {
		return {
			Timeline: Timeline,
			I18n: I18n,
			Preferences: game.preferences,
			showFullHistory() {
				game.modals.timeline = true;
			}
		};
	},
	template: `<div class="tc-timeline aside">
			<div class="aside-title">
				{{ 'c.main.history' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsideTimeline" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsideTimeline = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsideTimeline" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsideTimeline = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsideTimeline ? 'collapsed': ''">
				<ul>
					<li v-for="item in Timeline.items.slice(0, 15)">
						<tc-timeline-item :item="item"></tc-timeline-item>
					</li>
				</ul>
				<p class="u-txt-center">
					<button v-if="Timeline.items.length > 15" type="button" class="btn" @click="showFullHistory()">
						{{ 'c.main.view_full_history' | trans }}
					</button>
				</p>
			</div>
		</div>`
});

Vue.component('tc-timeline-item', {
	props: ['item'],
	template: `<div class="tc-timeline-item" :class="['category-' + item.category, item.clickable ? 'clickable' : '']"
			@click="item.onClick()" v-tooltip="item.tooltip">
			<span class="time">{{ item.date | time }}</span>
			<span class="message">{{ item.message | trans(['ucf'], item.substitutions) }}</span>
		</div>`
});

Vue.component('tc-timeline-modal', {
	data() {
		return {
			items: Timeline.items,
			close() {
				game.modals.timeline = false;
			}
		}
	},
	template: `<core-modal :title="'c.main.history' | trans" @close="close()" class="tc-timeline-modal">
			<div slot="body">
				<ul>
					<li v-for="item in items">
						<tc-timeline-item :item="item"></tc-timeline-item>
					</li>
				</ul>
			</div>
		</core-modal>`
});

Vue.component('tc-ranking', {
	data() {
		return {
			I18n: I18n,
			Preferences: game.preferences,
			game: game
		};
	},
	computed: {
		ranking() {
			let ranking = [];
			game.players.forEach((player) => { ranking.push({ player: player, score: player.points, rank: null }); });
			ranking.sort((a, b) => { return b.score - a.score });

			// Calculate ranks.
			let lastScore = null;
			let currentRank = 0;
			let tie = 1;
			ranking.forEach((item) => {
				if (item.score !== lastScore) {
					currentRank += tie;
					lastScore = item.score;
					tie = 1;
				}
				else {
					tie++;
				}
				item.rank = currentRank;
			})
			return ranking;
		}
	},
	template: `<div class="tc-ranking aside" v-if="game.ended">
			<div class="aside-title">
				{{ 'c.main.ranking' | trans }}
			</div>
			<div class="aside-content">
				<ul>
					<li v-for="item in ranking">
						<span class="player-container">
							<span class="bullet" :style="{background: item.player.color.cssCode, border: '1px solid ' + item.player.color.cssSecondary }"></span>
							<span class="name">{{ item.player.name }}</span>
							<span class="text-muted">–</span>
							{{ item.score }} <img src="../images/elements/point.svg" class="icon-points" :alt="'c.main.point_s' | trans" />
						</span>
						<span class="rank-container" :class="'rank-level-' + item.rank">
							<span class="rank">
								<span class="rank-content">{{ item.rank }}</span>
							</span>
						</span>
					</li>
				</ul>
			</div>
		</div>`
});

Vue.component('tc-game-admin', {
	props: ['client', 'server'],
	data() {
		return {
			game: game,
			Preferences: game.preferences,
			I18n: I18n
		};
	},
	template: `<div class="tc-game-admin aside" v-if="!client">
			<div class="aside-title">
				{{ 'c.main.game_admin' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsideGameAdmin" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsideGameAdmin = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsideGameAdmin" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsideGameAdmin = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsideGameAdmin ? 'collapsed' : ''">
				<p>
					<button type="button" class="btn" @click="game.forceEnd()" :disabled="game.ended">{{ 'c.main.force_end' | trans }}</button>
				</p>
			</div>
		</div>`
});

Vue.component('tc-game-debug', {
	props: ['client', 'server'],
	data() {
		return {
			game: game,
			Preferences: game.preferences,
			System: System,
			I18n: I18n
		};
	},
	template: `<div class="tc-game-debug aside" v-if="client || server">
			<div class="aside-title">
				{{ 'c.main.game_debug' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsideGameDebug" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsideGameDebug = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsideGameDebug" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsideGameDebug = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsideGameDebug ? 'collapsed' : ''">
				<p v-if="server">
						<button type="button" class="btn" @click="game.dispatch()">{{ 'c.main.broadcast_client_data' | trans }}</button>
				</p>
				<p v-if="client">
					<button type="button" class="btn" @click="System.reload()">{{ 'c.main.reload_page' | trans }}</button>
				</p>
				<p v-if="client || server">
					<button type="button" class="btn" @click="System.ping()">{{ 'c.main.send_ping' | trans }}</button>
				</p>
			</div>
		</div>`
});

Vue.component('tc-preferences', {
	data() {
		return {
			Preferences: game.preferences,
			I18n: I18n
		};
	},
	template: `<div class="tc-preferences aside">
			<div class="aside-title">
				{{ 'c.main.preferences' | trans }}
				<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseAsidePreferences" v-tooltip="I18n.trans('c.main.collapse')"
					@click="Preferences.collapseAsidePreferences = true">
					<img src="images/icons/toggle-up.svg" alt="" class="icon-sm" />
				</button>
				<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseAsidePreferences" v-tooltip="I18n.trans('c.main.expand')"
					@click="Preferences.collapseAsidePreferences = false">
					<img src="images/icons/toggle-down.svg" alt="" class="icon-sm" />
				</button>
			</div>
			<div class="aside-content" :class="Preferences.collapseAsidePreferences ? 'collapsed' : ''">
				<p>
					{{ 'c.main.phases_display' | trans(['lab']) }}
					<button type="button" class="btn btn-icon-sm" :title="'c.main.phases_display_aside_left'|trans"
						@click="Preferences.phasesDisplay = 'aside'" :disabled="Preferences.phasesDisplay == 'aside'">
						<img src="images/icons/aside-left.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" :title="'c.main.phases_display_footer'|trans"
						@click="Preferences.phasesDisplay = 'footer'" :disabled="Preferences.phasesDisplay == 'footer'">
						<img src="images/icons/footer.svg" alt="" class="icon-sm" />
					</button>
				</p>
				<p>
					{{ 'c.main.board_card_size' | trans(['lab']) }}
					<button type="button" class="btn btn-icon-sm" :title="'c.main.board_card_full_size'|trans"
						@click="Preferences.boardCardsDisplay = 'full'" :disabled="Preferences.boardCardsDisplay == 'full'">
						<img src="images/icons/full-cards.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" :title="'c.main.board_card_half_size'|trans"
						@click="Preferences.boardCardsDisplay = 'half'" :disabled="Preferences.boardCardsDisplay == 'half'">
						<img src="images/icons/half-cards.svg" alt="" class="icon-sm" />
					</button>
					<button type="button" class="btn btn-icon-sm" :title="'c.main.board_card_none_size'|trans"
						@click="Preferences.boardCardsDisplay = 'none'" :disabled="Preferences.boardCardsDisplay == 'none'">
						<img src="images/icons/close.svg" alt="" class="icon-sm" />
					</button>
				</p>
				<p>
					<label class="no-margin-bottom">
						<input type="checkbox" class="switch" v-model="Preferences.playSounds" value="selection" />
						{{ 'c.main.play_sounds' | trans }}
					</label>
				</p>
			</div>
		</div>`
});

Vue.component('tc-board', {
	data() {
		return {
			preferences: game.preferences,
			Phase: Phase,
			game: game,
		};
	},
	template: `<div class="tc-board" :class="'board-cards-display-' + preferences.boardCardsDisplay">
			<div class="tc-board-content">
				<div v-if="game.currentPhase">
					<tc-draft-phase v-if="game.currentPhase.type === Phase.TYPE_DRAFT" :phase="game.currentPhase"></tc-draft-phase>
				</div>
				<tc-own-board></tc-own-board>
				<tc-other-boards></tc-other-boards>
			</div>
		</div>`
});

Vue.component('tc-draft-phase', {
	props: ['phase'],
	data() {
		if (this.phase instanceof DraftPhase) {
			return {
				Preferences: game.preferences,
				game: game,
				selection: { index: null },
				okTooltip: I18n.trans('c.main.action_ok_description')
			};
		}
		else {
			throw new Error('[draft-phase] Invalid phase type!');
		}
	},
	computed: {
		titleKey() {
			if (this.phase instanceof DraftPhase) {
				return this.phase.itemType === DraftPhase.TYPE_CARD ? 'c.main.choose_card_to_play' : 'c.main.choose_tile_to_play';
			}
			return null;
		},
		waitingForAction() {
			return !this.phase.roundState.playerChoices[game.me.name];
		}
	},
	watch: {
		'selection.index'() {
			if (this.phase instanceof DraftPhase) {
				this.phase.setCurrentPlayerChoice(game, this.selection.index);
			}
		},
		'game.currentPhase.passedRounds'() {
			this.selection.index = null;
		}
	},
	template: `<div class="tc-draft-phase">
			<div v-if="phase">
				<h2>{{ titleKey | trans }} <img v-if="!waitingForAction" src="../images/icons/green/confirm.svg" class="icon-ok"
				:alt="'c.main.action_ok' | trans" v-tooltip="okTooltip" /></h2>
				<tc-hand :selection.sync="selection" :type="phase.itemType"></tc-hand>
			</div>
		</div>`
});

Vue.component('tc-hand', {
	props: ['selection', 'type'],
	data() {
		return {
			Preferences: game.preferences,
			game: game
		};
	},
	template: `<div class="tc-hand">
			<div class="tc-hand-content" :class="type + '-hand'">
				<div v-for="(code, index) in game.me.hand.codes" :key="index" class="hand-item-container"
					:class="{ selected: index === selection.index }">
					<div v-if="type === 'card'" class="hand-card">
						<tc-card-thumbnail :code="code" :showDetails="true"></tc-card-thumbnail>
						<div class="toolbar">
							<button type="button" class="btn" @click="selection.index = index" v-if="index !== selection.index">
								{{ 'c.main.select' | trans }}
							</button>
							<button type="button" class="btn" @click="selection.index = null" v-if="index === selection.index">
								{{ 'c.main.deselect' | trans }}
							</button>
						</div>
					</div>
					<div v-if="type === 'tile'" class="hand-tile">
						<og-score-tile-thumbnail :code="code" :showDetails="true" :type="'personal'"></og-score-tile-thumbnail>
						<div class="toolbar">
							<button type="button" class="btn" @click="selection.index = index" v-if="index !== selection.index">
								{{ 'c.main.select' | trans }}
							</button>
							<button type="button" class="btn" @click="selection.index = null" v-if="index === selection.index">
								{{ 'c.main.deselect' | trans }}
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-player-board', {
	props: ['player'],
	data() {
		return {
			Preferences: game.preferences,
			game: game
		};
	},
	computed: {
		cardCategories() {
			let leftNeighbour = ArrayUtils.getLeftNeighbour(game.players, this.player, true);
			let rightNeighbour = ArrayUtils.getRightNeighbour(game.players, this.player, true);
			if (!leftNeighbour || !rightNeighbour) {
				console.error('[tc-player-board] Missing neighbour!');
			}

			let playerCategories = this.player.cardCategories;
			let result = [];
			Object.keys(playerCategories).sort().forEach((type) => {
				let typeCategories = playerCategories[type];

				let leftPlayerCategories = leftNeighbour.cardCategories[type] || {};
				let rightPlayerCategories = rightNeighbour.cardCategories[type] || {};

				let resultCategories = [];
				Object.keys(typeCategories).sort().forEach((code) => {
					let item = {
						code: code,
						count: typeCategories[code],
						leftStatus: MathUtils.spaceship(typeCategories[code], leftPlayerCategories[code]),
						leftStatusTooltip: null,
						rightStatus: MathUtils.spaceship(typeCategories[code], rightPlayerCategories[code]),
						rightStatusTooltip: null,
					};

					if (item.count) {
						if (item.leftStatus === 1) {
							item.leftStatusTooltip = I18n.trans('c.main.category_has_more_left', [], { name: leftNeighbour.name });
						}
						else if (item.leftStatus === 0) {
							item.leftStatusTooltip = I18n.trans('c.main.category_has_same_left', [], { name: leftNeighbour.name });
						}
						else if (item.leftStatus === -1) {
							item.leftStatusTooltip = I18n.trans('c.main.category_has_less_left', [], { name: leftNeighbour.name });
						}

						if (item.rightStatus === 1) {
							item.rightStatusTooltip = I18n.trans('c.main.category_has_more_right', [], { name: rightNeighbour.name });
						}
						else if (item.rightStatus === 0) {
							item.rightStatusTooltip = I18n.trans('c.main.category_has_same_right', [], { name: rightNeighbour.name });
						}
						else if (item.rightStatus === -1) {
							item.rightStatusTooltip = I18n.trans('c.main.category_has_less_right', [], { name: rightNeighbour.name });
						}
					}

					resultCategories.push(item);
				});
				result.push({ type: type, categories: resultCategories });
			});
			return result;
		}
	},
	template: `<div class="tc-player-board">
			<div class="panel">
				<div class="panel-header">
					<h2>
						<tc-player :player="player"></tc-player>
					</h2>
				</div>
				<div class="panel-body">
					<div v-if="player.cards.length" class="tc-player-board-cards">
						<div v-for="card in player.cards" :key="card.code" class="board-card-container">
							<div class="board-card">
								<tc-card-thumbnail :code="card.code" :showDetails="true"></tc-card-thumbnail>
							</div>
						</div>
					</div>
					<div class="tc-player-board-stats">
						<div v-for="categoryType in cardCategories" class="board-category-type-container">
							<span v-for="item in categoryType.categories">
								<span class="no-wrap">
									<span class="category category-30" :class="item.count ? 'possessed' : 'not-possessed'">
										<og-category-thumbnail :code="item.code"></og-category-thumbnail>
										<span class="category-majority-statuses">
											<span v-tooltip="item.leftStatusTooltip"
												:class="{ 'category-has-more': item.leftStatus === 1, 'category-has-same': item.leftStatus === 0, 'category-has-less': item.leftStatus === -1 }"></span>
											<span v-tooltip="item.rightStatusTooltip"
												:class="{ 'category-has-more': item.rightStatus === 1, 'category-has-same': item.rightStatus === 0, 'category-has-less': item.rightStatus === -1 }"></span>
										</span>
									</span>
									<span v-if="item.count > 1">x{{ item.count }}</span>
								</span>
							</span>
						</div>
					</div>
					<div v-if="player.tiles.length" class="tc-player-board-tiles tiles-small">
						<div v-for="tile in player.tiles" class="board-tile-container">
							<div class="board-tile">
								<og-score-tile-thumbnail :code="tile.code" :showDetails="true" :type="'personal'"></og-score-tile-thumbnail>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-own-board', {
	data() {
		return {
			Preferences: game.preferences,
			DirectionEnum: DirectionEnum,
			game: game
		};
	},
	computed: {
		draftDirection: function() {
			return game.currentPhase ? game.currentPhase.draftDirection : null;
		}
	},
	template: `<div class="tc-own-board">
			<tc-player-board :player="game.me"></tc-player-board>
			<div class="draft-direction" v-if="draftDirection">
				<div class="draft-direction-left">
					<img v-if="draftDirection === DirectionEnum.LEFT" src="images/icons/white/draft-top.svg" alt="" class="icon-lg" />
					<img v-if="draftDirection === DirectionEnum.RIGHT" src="images/icons/white/draft-bottom.svg" alt="" class="icon-lg" />
				</div>
				<div class="draft-direction-right">
					<img v-if="draftDirection === DirectionEnum.LEFT" src="images/icons/white/draft-bottom.svg" alt="" class="icon-lg" />
					<img v-if="draftDirection === DirectionEnum.RIGHT" src="images/icons/white/draft-top.svg" alt="" class="icon-lg" />
				</div>
			</div>
		</div>`
});

Vue.component('tc-other-boards', {
	data() {
		return {
			Preferences: game.preferences,
			game: game
		};
	},
	computed: {
		players: function() {
			let players = [];

			let currentPlayerFound = false;
			game.players.forEach((player) => {
				if (player === game.me) {
					currentPlayerFound = true;
				}
				else if (currentPlayerFound) {
					players.push(player);
				}
			});

			currentPlayerFound = false;
			game.players.forEach((player) => {
				if (player === game.me) {
					currentPlayerFound = true;
				}
				else if (!currentPlayerFound) {
					players.push(player);
				}
			});

			return players;
		}
	},
	template: `<div class="tc-other-boards">
			<div v-for="(player, index) in players" class="other-board"
				:style="{order: (index < (players.length / 2)) ? (1 + 2*index) : (2 * (players.length - index - 1)) }">
				<tc-player-board :player="player"></tc-player-board>
			</div>
		</div>`
});

Vue.component('tc-game-phases', {
	data() {
		return {
			Phase: Phase,
			Preferences: game.preferences,
			game: game
		};
	},
	template: `<div class="tc-game-phases aside">
			<div class="aside-title">
				{{ 'c.main.game_phases' | trans }}
			</div>
			<div class="aside-content">
				<div v-for="(phase, index) in game.phases" class="phase" :class="{'phase-past': index < game.currentPhaseIndex, 'phase-current': index === game.currentPhaseIndex, 'phase-future': index > game.currentPhaseIndex }">
					<div class="phase-number"><span class="badge">{{ index + 1 }}</span></div>
					<div class="phase-description">
						<div class="phase-name">
							{{ phase.name }}
						</div>
						<div v-if="phase.type === Phase.TYPE_DRAFT">
							<tc-game-phase-draft :phase="phase"></tc-game-phase-draft>
						</div>
						<div v-if="phase.type === Phase.TYPE_SCORE">
							<tc-game-phase-score :phase="phase"></tc-game-phase-score>
						</div>
						<div v-if="phase.type === Phase.TYPE_DISCARD">
							<og-game-phase-discard :phase="phase"></og-game-phase-discard>
						</div>
					</div>
				</div>
			</div>
		</div>`
});

Vue.component('tc-game-phase-draft', {
	props: ['phase'],
	data() {
		return {
			game: game
		};
	},
	computed: {
		description() {
			let description = '';

			if (this.phase.itemType === DraftPhase.TYPE_CARD) {
				description += I18n.trans('c.main.n_cards_among_m', [], { N: this.phase.roundCount, M: this.phase.handSize });
			}
			else if (this.phase.itemType === DraftPhase.TYPE_TILE) {
				description += I18n.trans('c.main.n_tiles_among_m', [], { N: this.phase.roundCount, M: this.phase.handSize });
			}

			if (this.phase.draftDirection === DirectionEnum.RIGHT) {
				description += I18n.trans('c.main.to_next');
			}
			else {
				description += I18n.trans('c.main.to_previous');
			}
			return description;
		}
	},
	template: `<div class="tc-game-phase-draft">
			<div class="u-small" v-html="description"></div>
		</div>`
});

Vue.component('tc-game-phase-score', {
	props: ['phase'],
	data() {
		return {
			game: game
		};
	},
	computed: {
		tiles() {
			if (this.phase instanceof ScorePhase) {
				return this.phase.getScoreTiles(game);
			}
			return [];
		}
	},
	template: `<div class="tc-game-phase-score">
			<div class="tiles tiles-small">
				<div v-for="tile in tiles">
					<og-score-tile-thumbnail :model="tile"></og-score-tile-thumbnail>
				</div>
			</div>
		</div>`
});

Vue.component('og-game-phase-discard', {
	props: ['phase'],
	data() {
		return {
			game: game
		};
	},
	computed: {
		description() {
			let description = '';

			if (this.phase.itemType === DraftPhase.TYPE_CARD) {
				description += I18n.trans('c.main.discard_all_cards');
			}
			else if (this.phase.itemType === DraftPhase.TYPE_TILE) {
				description += I18n.trans('c.main.discard_all_tiles');
			}

			return description;
		}
	},
	template: `<div class="tc-game-phase-discard">
			<div class="u-small" v-html="description"></div>
		</div>`
});