/**
 * Copyright (C) 2021 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { System } from "../core/tools/system.mjs";
import { game } from "../services/game.mjs";

export default {
	data() {
		System.requestReconnection({ selfId: this.$route.params.selfId, serverId: this.$route.params.serverId });
		return {};
	},
	template: `<div id="view-reconnect">
			<div class="view-content">
				<div class="panel">
					<h2 class="panel-header">{{ 'c.main.reconnecting' | trans }}</h2>
					<div class="panel-body">
						{{ 'c.main.reconnecting_description' | trans }}
					</div>
				</div>
			</div>
		</div>`
}

document.addEventListener('coreConnectionReconnected', (event) => {
	game.data = event.detail.data;
	setTimeout(() => {
		System.router.push('/play');
	}, 100);
});


