/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { Models } from '../services/models.mjs';

export default {
	data: function() {
		return {
			Models: Models
		}
	},
	template: `<div id="view-extensions">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.main.the_extensions' | trans }}</h1>
				</div>
				<div v-for="extension in Models.extensionsArray" class="extension panel">
					<div class="panel-header">
						<div class="extension-symbol-container" v-if="extension.symbolSrc">
							<img :src="extension.symbolSrc" class="extension-symbol" alt="" />
						</div>
						<h2>
							{{ extension.name }} <small class="text-muted">{{ extension.code }}</small>
						</h2>
						<p>{{ extension.description }}</p>
						<p v-if="extension.about">{{ 'c.main.about' | trans(['lab']) }} {{ extension.about }}</p>
					</div>
					<div class="panel-body flex-container">
						<div class="w250p">
							<div v-if="extension.categories.length">
								<h3>{{ 'c.main.categories' | trans }} <span class="badge">{{ extension.categories.length }}</span></h3>
								<ul class="little-items pawns">
									<li class="item" v-for="category in extension.categories">
										<tc-category-model-summary :model="category"></tc-category-model-summary>
									</li>
								</ul>
							</div>
						</div>
						<div class="w5"></div>
						<div class="item-fluid">
							<div v-if="extension.cards.length"> 
								<h3>{{ 'c.main.cards' | trans }} <span class="badge">{{ extension.cards.length }}</span></h3>
								<div class="cards">
									<div class="items">
										<tc-card-thumbnail v-for="card in extension.cards" :model="card" :showDetails="true"
											:key="card.code"></tc-card-thumbnail>
									</div>
								</div>
							</div>
							<div v-if="extension.scoreTiles.length"> 
								<h3>{{ 'c.main.score_tiles' | trans }} <span class="badge">{{ extension.scoreTiles.length }}</span></h3>
								<div class="tiles">
									<div class="items">
										<og-score-tile-thumbnail v-for="tile in extension.scoreTiles" :model="tile" :showDetails="true"
											:key="tile.code"></og-score-tile-thumbnail>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>`
}