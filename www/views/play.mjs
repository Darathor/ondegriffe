/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Connection } from "../core/tools/connection.mjs";
import { I18n } from "../core/tools/i18n.mjs";
import { game } from '../services/game.mjs';
import { Sounds } from "../services/sounds.mjs";

export default {
	data() {
		return {
			game: game,
			server: Connection.isServer(),
			client: Connection.isClient(),
			peerId: Connection.id,
			Preferences: game.preferences,
			I18n: I18n
		}
	},
	mounted() {
		this.$nextTick(() => {
			// Code that will run only after the entire view has been rendered.
			if (!game.initialized) {
				this.$router.push('/');
			}
			else if (game.preferences.playSounds && !game.announced) {
				game.announced = true;
				Sounds.startGame.play();
			}
		});
	},
	watch: {
		'game.ended': {
			handler: () => {
				if (game.preferences.playSounds && game.ended) {
					Sounds.endGame.play();
				}
			}
		}
	},
	template: `<div id="view-play" class="flex-vertical" v-if="game.initialized">
			<div id="view-play-body" class="flex-horizontal">
				<aside v-if="Preferences.phasesDisplay === 'aside'"
					class="sidebar sidebar-left" :class="Preferences.collapseLeftSidebar ? 'collapsed': ''">
					<div class="sidebar-toggle">
						<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseLeftSidebar" v-tooltip="I18n.trans('c.main.collapse')"
							@click="Preferences.collapseLeftSidebar = true">
							<img src="images/icons/toggle-left.svg" alt="" class="icon-sm" />
						</button>
						<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseLeftSidebar" v-tooltip="I18n.trans('c.main.expand')"
							@click="Preferences.collapseLeftSidebar = false">
							<img src="images/icons/toggle-right.svg" alt="" class="icon-sm" />
						</button>
					</div>
					<tc-game-phases></tc-game-phases>
				</aside>
				<div class="board">
					<tc-board></tc-board>
				</div>
				<aside class="sidebar sidebar-right" :class="Preferences.collapseRightSidebar ? 'collapsed': ''">
					<div class="sidebar-toggle">
						<button type="button" class="btn btn-icon-sm" v-if="!Preferences.collapseRightSidebar" v-tooltip="I18n.trans('c.main.collapse')"
							@click="Preferences.collapseRightSidebar = true">
							<img src="images/icons/toggle-right.svg" alt="" class="icon-sm" />
						</button>
						<button type="button" class="btn btn-icon-sm" v-if="Preferences.collapseRightSidebar" v-tooltip="I18n.trans('c.main.expand')"
							@click="Preferences.collapseRightSidebar = false">
							<img src="images/icons/toggle-left.svg" alt="" class="icon-sm" />
						</button>
					</div>
					<tc-game-admin :client="client" :server="server"></tc-game-admin>
					<tc-preferences></tc-preferences>
					<tc-game-debug :client="client" :server="server"></tc-game-debug>
					<tc-players v-if="!game.ended"></tc-players>
					<tc-ranking></tc-ranking>
					<tc-timeline></tc-timeline>
				</aside>
				<tc-choose-pawn-modal v-if="game.modals.choosePawn"></tc-choose-pawn-modal>
				<tc-timeline-modal v-if="game.modals.timeline"></tc-timeline-modal>
			</div>
			<div v-if="Preferences.phasesDisplay === 'footer'" id="view-play-footer">
				<tc-game-phases></tc-game-phases>
			</div>
		</div>`
}


