/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Connection } from '../core/tools/connection.mjs';
import { debounce } from '../core/components/vue-tools.mjs';
import { Setup } from '../services/setup.mjs';
import { game } from '../services/game.mjs';
import { Preferences } from "../services/preferences.mjs";

let onDataUpdated = debounce(() => {
	if (Connection.isServer() && Setup.isValid) {
		Connection.sendMessage('SetupData', Setup.toData());
	}
	else if (Connection.isClient() && Setup.isValid) {
		let player = Setup.getPlayerByPeerId(Connection.id);
		if (player) {
			Connection.sendMessage('SetupUpdatePlayer', player.data);
		}
	}
});

export default {
	data() {
		if (!Connection.id) {
			console.warn('Can only be played online! Redirect to home.');
			this.$router.push('/');
		}
		else {
			Setup.init(Connection.isClient());
		}
		return {
			Setup: Setup,
			Preferences: Preferences.getInstance(),
			server: Connection.isServer(),
			client: Connection.isClient(),
			peerId: Connection.id,
			start() {
				game.init();
				Connection.sendMessage('StartGame', game.data);
				this.$router.push('/play');
			}
		}
	},
	watch: {
		Setup: {
			deep: true,
			handler: onDataUpdated
		}
	},
	mounted() {
		let recursiveCheckInitialized = () => {
			if (this.$router.currentRoute.path === '/setup') {
				if (game.initialized) {
					this.$router.push('/play');
				}
				else {
					setTimeout(recursiveCheckInitialized, 100);
				}
			}
		};

		// Code that will run only after the entire view has been rendered.
		this.$nextTick(recursiveCheckInitialized);
	},
	template: `<div id="view-setup">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.setup.new_game' | trans }}</h1>
				</div>
				<div class="view-body">
					<div>
						<tc-setup-setup :client="client"></tc-setup-setup>
						<tc-players-setup :peer-id="peerId" :server="server"></tc-players-setup>
						<tc-pool-setup :client="client"></tc-pool-setup>
						<og-tile-pool-setup :client="client"></og-tile-pool-setup>
						<!--<tc-optional-rules-setup :client="client"></tc-optional-rules-setup>-->
					</div>
					<div>
						<tc-cards-list-setup :client="client"></tc-cards-list-setup>
						<tc-score-tiles-list-setup :client="client"></tc-score-tiles-list-setup>
					</div>
				</div>
				<div class="view-footer txtcenter" v-if="!client">
					<button type="button" class="btn btn--big" :disabled="!Setup.isValid" @click="start()">{{ 'c.setup.start_game' | trans }}</button>
				</div>
			</div>
		</div>`
}