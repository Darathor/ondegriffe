/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import '../services/all-for-game.mjs';
import { LocalStorage } from "../core/tools/local-storage.mjs";
import { I18n } from '../core/tools/i18n.mjs';
import { System } from "../core/tools/system.mjs";
import { Connection } from '../core/tools/connection.mjs';
import { ModelsLoader } from '../services/models/loader.mjs';
import { game } from "../services/game.mjs";

import home from '../views/home.mjs';
import about from '../views/about.mjs';
import setup from '../views/setup.mjs';
import play from '../views/play.mjs';
import extensions from '../views/extensions.mjs';
import join from '../views/join.mjs';
import reconnect from '../views/reconnect.mjs';

LocalStorage.setNamespace('og');

let Vue = window.Vue;
let VueRouter = window.VueRouter;

I18n.registerFilePaths([
	{ prefix: 'c.main.', path: 'i18n/main/' },
	{ prefix: 'c.timeline.', path: 'i18n/timeline/' },
	{ prefix: 'c.setup.', path: 'i18n/setup/' }
]);

I18n.init('fr_FR', () => {
	ModelsLoader.load(['core'], () => {
		let routes = [
			{ path: '/', component: home },
			{ path: '/about', component: about },
			{ path: '/setup', component: setup },
			{ path: '/play', component: play },
			{ path: '/extensions', component: extensions },
			{ path: '/join/:serverId', component: join },
			{ path: '/reconnect/:serverId/:selfId', component: reconnect }
		];

		let router = new VueRouter({
			routes: routes
		});
		System.router = router;

		let app = new Vue({
			router: router,
			data: {
				initialized: true,
				Connection: Connection,
				I18n: I18n
			}
		});

		app.$mount('#app');

		//region Event listeners.
		document.addEventListener('coreConnectionRemovePlayer', (event) => {
			let player = event.detail.data;
			if (player.peerId === Connection.id) {
				alert(I18n.trans('c.main.excluded_from_game'));
				router.push('/');
				window.location.reload();
			}
		});

		// Confirm before quitting the page when a game is running.
		window.addEventListener('beforeunload', (event) => {
			if (game && game.initialized && !game.ended) {
				event.returnValue = true;
			}
		});
		//endregion
	});
});