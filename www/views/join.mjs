/**
 * Copyright (C) 2022 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Connection } from "../core/tools/connection.mjs";

export default {
	data() {
		console.log(this.$route.params.serverId);
		Connection.startClient(this.$route.params.serverId).then(() => {
			this.$router.push('/setup');
		});
		return {};
	},
	template: `<div id="view-join">
			<div class="view-content">
				<div class="panel">
					<h2 class="panel-header">{{ 'c.main.connecting' | trans }}</h2>
					<div class="panel-body">
						{{ 'c.main.connecting_description' | trans }}
					</div>
				</div>
			</div>
		</div>`
}

