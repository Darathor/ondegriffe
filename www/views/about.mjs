/**
 * Copyright (C) 2021 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core/tools/i18n.mjs";

export default {
	data() {
		return {
			I18n: I18n
		}
	},
	computed: {
		LCID: () => {
			return I18n.LCID;
		}
	},
	template: `<div id="view-about">
			<div class="view-content">
				<div class="view-header">
					<h1 class="txtcenter">{{ 'c.main.about_ondegriffe' | trans }}</h1>
				</div>
				<div class="view-body" v-if="LCID === 'fr_FR'">
					<div class="panel">
						<h2 class="panel-header txtcenter">Crédits</h2>
						<div class="panel-body">
							<p>Ce jeu est réalisé par <a href="https://blog.darathor.net/" hreflang="fr" class="external">Darathor</a> et partagé sous licence <a href="https://mozilla.org/MPL/2.0/" hreflang="en" class="external">MPL</a> pour le code source et <a href="https://creativecommons.org/licenses/by/4.0/legalcode.fr" hreflang="fr" class="external">CC-BY</a> pour les éléments graphiques, à l'exception des éléments suivants.</p>
							<p>Le code repose sur les frameworks <a href="https://fr.vuejs.org/" hreflang="fr" class="external">VueJS 2</a> et <a href="https://www.knacss.com/doc-old.html" hreflang="fr" class="external">KNACSS 7</a>, ainsi que les bibliothèques <a href="https://peerjs.com/" hreflang="en" class="external">PeerJS</a> et <a href="https://vuejsprojects.com/v-tooltip" hreflang="en" class="external">v-tooltip</a>.</p>
							<p>Les textures d'arrière-plan de pierre et bois sont dérivées de textures réalisées par <a href="https://www.artstation.com/skripka" hreflang="en" class="external">Helen Skripka</a>.</p>
							<p>
								Les icônes suivantes proviennent du pack <a href="https://fontawesome.com/" hreflang="en" class="external">Font Awesome</a> (parfois modifiées) : 
								<img src="images/icons/add.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-down.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-left.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-right.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-up.svg" class="icon" alt="" />,
								<img src="images/icons/clock.svg" class="icon" alt="" />,
								<img src="images/icons/close.svg" class="icon" alt="" />,
								<img src="images/icons/copy.svg" class="icon" alt="" />,
								<img src="images/icons/delete.svg" class="icon" alt="" />,
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />,
								<img src="images/icons/edit.svg" class="icon" alt="" />,
								<img src="images/icons/info.svg" class="icon" alt="" />,
								<img src="images/icons/language.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-left-click.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-right-click.svg" class="icon" alt="" />,
								<img src="images/icons/redraw.svg" class="icon" alt="" />,
								<img src="images/icons/save.svg" class="icon" alt="" />,
								<img src="images/icons/select-all.svg" class="icon" alt="" />,
								<img src="images/icons/suggestion.svg" class="icon" alt="" />,
								<img src="images/icons/target.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-down.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-left.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-right.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-up.svg" class="icon" alt="" />,
								<img src="images/icons/volume-mute.svg" class="icon" alt="" /> et 
								<img src="images/icons/volume-up.svg" class="icon" alt="" />.
							</p>
							<p>
								Les différents sons proviennent de <a href="https://lasonotheque.org/" hreflang="fr" class="external">LaSonotheque.fr</a> :
								<a href="https://lasonotheque.org/detail-0283-chant-du-coq.html" hreflang="fr" class="external">début de partie</a> (par DenisChardonnet),
								<a href="https://lasonotheque.org/detail-1111-message-1.html" hreflang="fr" class="external">début de phase</a> (par Joseph SARDIN),
								<a href="https://lasonotheque.org/detail-0813-applaudissements-25-50-pers-2.html" hreflang="fr" class="external">fin de partie</a> (par Joseph SARDIN) et
								<a href="https://lasonotheque.org/detail-1588-marteau-de-president-1-coup.html" hreflang="fr" class="external">nouvelle alerte</a> (par Joseph SARDIN)
							</p>
							<p>Enfin, les illustrations des cartes sont des œuvres soit passées dans le domaine public, soit qui ont été publiées sous licence libre par leur auteur. Chaque carte mentionne l'auteur correspondant.</p>
						</div>
					</div>
					<div class="panel">
						<h2 class="panel-header txtcenter">Contribuer</h2>
						<div class="panel-body">
							<p class="no-margin-bottom">Si vous souhaitez contribuer au développement du jeu ou remonter des suggestions ou commentaires, vous pouvez passer par le dépôt <a href="https://framagit.org/Darathor/ondegriffe" class="external">GitLab</a> ou bien me contacter sur <a href="https://mamot.fr/@Darathor" hreflang="fr">Mastodon</a>.</p>
						</div>
					</div>
				</div>
				<div class="view-body" v-if="LCID !== 'fr_FR'">
					<div class="panel">
						<h2 class="panel-header txtcenter">Credits</h2>
						<div class="panel-body">
							<p>This game is made by <a href="https://blog.darathor.net/" hreflang="fr" class="external">Darathor</a> and shared under the <a href="https://mozilla.org/MPL/2.0/" hreflang="en" class="external">MPL</a> license for the source code and <a href="https://creativecommons.org/licenses/by/4.0/legalcode" hreflang="fr" class="external">CC-BY</a> for the graphics, except for the following elements.</p>
							<p>The code is based on the <a href="https://vuejs.org/" hreflang="en" class="external">VueJS 2</a> and <a href="https://www.knacss.com/doc-old.html" hreflang="fr" class="external">KNACSS 7</a> frameworks, as well as <a href="https://peerjs.com/" hreflang="en" class="external">PeerJS</a> and <a href="https://vuejsprojects.com/v-tooltip" hreflang="en" class="external">v-tooltip</a> libraries.</p>
							<p>The stone and wood background textures are derived from textures made by <a href="https://www.artstation.com/skripka" hreflang="en" class="external">Helen Skripka</a>.</p>
							<p>
								The following icons are from the <a href="https://fontawesome.com/" hreflang="en" class="external">Font Awesome</a> package (sometimes modified): 
								<img src="images/icons/add.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-down.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-left.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-right.svg" class="icon" alt="" />,
								<img src="images/icons/arrow-up.svg" class="icon" alt="" />,
								<img src="images/icons/clock.svg" class="icon" alt="" />,
								<img src="images/icons/close.svg" class="icon" alt="" />,
								<img src="images/icons/copy.svg" class="icon" alt="" />,
								<img src="images/icons/delete.svg" class="icon" alt="" />,
								<img src="images/icons/deselect-all.svg" class="icon" alt="" />,
								<img src="images/icons/edit.svg" class="icon" alt="" />,
								<img src="images/icons/info.svg" class="icon" alt="" />,
								<img src="images/icons/language.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-left-click.svg" class="icon" alt="" />,
								<img src="images/icons/mouse-right-click.svg" class="icon" alt="" />,
								<img src="images/icons/redraw.svg" class="icon" alt="" />,
								<img src="images/icons/save.svg" class="icon" alt="" />,
								<img src="images/icons/select-all.svg" class="icon" alt="" />,
								<img src="images/icons/suggestion.svg" class="icon" alt="" />,
								<img src="images/icons/target.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-down.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-left.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-right.svg" class="icon" alt="" />,
								<img src="images/icons/toggle-up.svg" class="icon" alt="" />,
								<img src="images/icons/volume-mute.svg" class="icon" alt="" /> et 
								<img src="images/icons/volume-up.svg" class="icon" alt="" />.
							</p>
							<p>
								The different sounds come from <a href="https://lasonotheque.org/" hreflang="fr" class="external">LaSonotheque.fr</a>:
								<a href="https://lasonotheque.org/detail-0283-chant-du-coq.html" hreflang="fr" class="external">game start</a> (by DenisChardonnet),
								<a href="https://lasonotheque.org/detail-1111-message-1.html" hreflang="fr" class="external">phase start</a> (by GlaneurDeSons),
								<a href="https://lasonotheque.org/detail-0813-applaudissements-25-50-pers-2.html" hreflang="fr" class="external">game end</a> (by Joseph SARDIN) and
								<a href="https://lasonotheque.org/detail-1588-marteau-de-president-1-coup.html" hreflang="fr" class="external">new alert</a> (par Joseph SARDIN)
							</p>
							<p>Finally, the illustrations on the cards are either in the public domain or have been published under a free license by their author. Each card mentions the corresponding author.</p>
						</div>
					</div>
					<div class="panel">
						<h2 class="panel-header txtcenter">Contribute</h2>
						<div class="panel-body">
							<p class="no-margin-bottom">If you would like to contribute to the development of the game or bring up suggestions or comments, you can go through the <a href="https://framagit.org/Darathor/ondegriffe" class="external">GitLab</a> repository or contact me on <a href="https://mamot.fr/@Darathor" hreflang="fr">Mastodon</a>.</p>
						</div>
					</div>
				</div>
			</div>
		</div>`
}