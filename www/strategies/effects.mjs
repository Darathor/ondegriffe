/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from "../core/tools/i18n.mjs";
import { TimelineItemTrigger } from '../services/core.mjs';
import { Strategies } from '../services/strategies.mjs';
import { Models } from "../services/models.mjs";
import { ArtworkCardModel } from "../services/models/card.mjs";
import { ArrayUtils } from "../core/tools/array.mjs";

class EffectStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_effect_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_effect_' + this.code + '_description');
	}

	/**
	 * @param {Object} configuration
	 * @param {Game} game
	 * @param {Player} player
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, game, player, source) {
		// Do nothing by default.
	}
}

/**
 * Default strategy: no effect.
 */
class NullEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.effects.null = new NullEffectStrategy();

/**
 * @typedef {Object} ScorePointsByCardCategoryEffectStrategyConfiguration
 * @property {string[]} categoryCodes
 * @property {number} value
 */

/**
 * Score points by card category: gain points for each card of a given category owned by the player.
 */
class ScorePointsByCardCategoryEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'scorePointsByCardCategory';
	}

	/**
	 * @param {ScorePointsByCardCategoryEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {Player} player
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, game, player, source) {
		let score = 0;
		player.cards.forEach((card) => {
			let model = Models.getCardByCode(card.code);
			if (!(model instanceof ArtworkCardModel)) {
				return;
			}
			model.categories.forEach((code) => {
				if (ArrayUtils.contains(configuration.categoryCodes, code)) {
					score += configuration.value;
				}
			});
		});
		player.addPoints(score, source);
	}
}

Strategies.effects.scorePointsByCardCategory = new ScorePointsByCardCategoryEffectStrategy();

/**
 * @typedef {Object} ScorePointsByDifferentCardCategoryTypeEffectStrategyConfiguration
 * @property {string} categoryType
 * @property {number} value
 */

/**
 * Score points by different card category of a given type: gain points for each different category of a given type on cards owned by the player.
 */
class ScorePointsByDifferentCardCategoryTypeEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'scorePointsByDifferentCardCategoryType';
	}

	/**
	 * @param {ScorePointsByDifferentCardCategoryTypeEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {Player} player
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, game, player, source) {
		let score = 0;
		let categoryCounts = player.cardCategories[configuration.categoryType] || {};
		Object.keys(categoryCounts).forEach((code) => {
			if (categoryCounts[code] > 0) {
				score += configuration.value;
			}
		});
		player.addPoints(score, source);
	}
}

Strategies.effects.scorePointsByDifferentCardCategoryType = new ScorePointsByDifferentCardCategoryTypeEffectStrategy();

/**
 * @typedef {Object} ScorePointsByCardCategoryWithNOrMoreEffectStrategyConfiguration
 * @property {string} categoryType
 * @property {number} value
 * @property {number} min
 */

/**
 * Score points by card category with N or more cards: gain points for each different category of a given type present on N or more cards owned by the player.
 */
class ScorePointsByCardCategoryWithNOrMoreEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'scorePointsByCardCategoryWithNOrMore';
	}

	/**
	 * @param {ScorePointsByCardCategoryWithNOrMoreEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {Player} player
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, game, player, source) {
		let score = 0;
		let categoryCounts = player.cardCategories[configuration.categoryType] || {};
		Object.keys(categoryCounts).forEach((code) => {
			if (categoryCounts[code] >= configuration.min) {
				score += configuration.value;
			}
		});
		player.addPoints(score, source);
	}
}

Strategies.effects.scorePointsByCardCategoryWithNOrMore = new ScorePointsByCardCategoryWithNOrMoreEffectStrategy();

/**
 * @typedef {Object} ScorePointsByCardInBiggestCategoryEffectStrategyConfiguration
 * @property {string} categoryType
 * @property {number} value
 */

/**
 * Score points by card in the biggest category: gain points for each card in the category with the more cards owned by the player.
 */
class ScorePointsByCardInBiggestCategoryEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'scorePointsByCardInBiggestCategory';
	}

	/**
	 * @param {ScorePointsByCardInBiggestCategoryEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {Player} player
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, game, player, source) {
		let biggestCount = 0;
		let categoryCounts = player.cardCategories[configuration.categoryType] || {};
		Object.keys(categoryCounts).forEach((code) => {
			if (categoryCounts[code] > biggestCount) {
				biggestCount = categoryCounts[code];
			}
		});
		player.addPoints(configuration.value * biggestCount, source);
	}
}

Strategies.effects.scorePointsByCardInBiggestCategory = new ScorePointsByCardInBiggestCategoryEffectStrategy();

/**
 * @typedef {Object} ScorePointsByCategoryWithMajorityEffectStrategyConfiguration
 * @property {string} categoryType
 * @property {number} value
 */

/**
 * Score points by card in the biggest category: gain points for each card in the category with the more cards owned by your neighbours.
 */
class ScorePointsByCategoryWithMajorityEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'scorePointsByCategoryWithMajority';
	}

	/**
	 * @param {ScorePointsByCategoryWithMajorityEffectStrategyConfiguration} configuration
	 * @param {Game} game
	 * @param {Player} player
	 * @param {TimelineItemTrigger} source
	 */
	apply(configuration, game, player, source) {
		let players = ArrayUtils.getNeighbours(game.players, player, true);
		players.push(player);

		// Calculate card counts by category and player.
		let categoryCodes = {};
		players.forEach((singlePlayer) => {
			let categoryCounts = singlePlayer.cardCategories[configuration.categoryType] || {};
			Object.keys(categoryCounts).forEach((code) => {
				if (!categoryCodes[code]) {
					categoryCodes[code] = {};
				}
				categoryCodes[code][singlePlayer.name] = categoryCounts[code];
			});
		});

		// Add points for each category where the current player has more category than each other.
		let score = 0;
		Object.keys(categoryCodes).forEach((code) => {
			let countsByPLayer = categoryCodes[code];
			let max = 0;
			let maxName = null;
			let equality = false;
			Object.keys(countsByPLayer).forEach((name) => {
				if (countsByPLayer[name] > max) {
					max = countsByPLayer[name];
					maxName = name;
					equality = false;
				}
				else if (countsByPLayer[name] === max) {
					equality = true;
				}
			});

			if (!equality && maxName === player.name) {
				score += configuration.value
			}
		});

		player.addPoints(score, source);
	}
}

Strategies.effects.scorePointsByCategoryWithMajority = new ScorePointsByCategoryWithMajorityEffectStrategy();