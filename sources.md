# Sites

- https://www.wikiart.org/fr


# Peintres

## Albert Bierstadt (1830-1902)

- https://www.wikiart.org/fr/albert-bierstadt
- https://www.meisterdrucke.fr/artiste/Albert-Bierstadt.html

## Albrecht D�rer (1471-1528)

- https://www.wikiart.org/fr/albrecht-durer
- https://fr.wikipedia.org/wiki/Albrecht_D%C3%BCrer
- https://www.wikiart.org/fr/albrecht-durer/courtyard-of-the-former-castle-in-innsbruck-without-clouds
- https://www.wikiart.org/fr/albrecht-durer/courtyard-of-the-former-castle-in-innsbruck-with-clouds-1494
- https://www.wikiart.org/fr/albrecht-durer/lot-s-escape
- https://www.wikiart.org/fr/albrecht-durer/deploration-du-christ-1498
- https://www.wikiart.org/fr/albrecht-durer/autoportrait-1500
- https://www.wikiart.org/fr/albrecht-durer/retable-des-dix-mille-martyrs-1508
- https://www.wikiart.org/fr/albrecht-durer/hare-1528-1

## Alexander Pope

- https://www.wikiart.org/fr/alexander-pope
- https://en.wikipedia.org/wiki/Alexander_Pope_Jr.
- https://www.wikiart.org/fr/alexander-pope/at-the-kennel-door-1905
- https://www.wikiart.org/fr/alexander-pope/a-pair-of-setters-1913
- https://www.wikiart.org/fr/alexander-pope/springer-spaniel-with-pheasant-1900
- https://www.wikiart.org/fr/alexander-pope/after-the-hunt-1900
- https://www.wikiart.org/fr/alexander-pope/weapons-of-war-1900

## Anne-Louis Girodet (1767-1824)

- https://www.wikiart.org/fr/anne-louis-girodet
- https://www.wikiart.org/fr/anne-louis-girodet/apotheose-des-heros-francais-morts-pour-la-patrie-pendant-la-guerre-de-la-liberte-1801
- https://www.wikiart.org/fr/anne-louis-girodet/self-portrait-1790

## Annibale Carracci

- https://www.wikiart.org/fr/annibale-carracci
- https://fr.wikipedia.org/wiki/Annibale_Carracci
- https://www.meisterdrucke.fr/artiste/Annibale-Carracci.html
- https://www.wikiart.org/fr/annibale-carracci/river-landscape-1590
  - https://www.meisterdrucke.fr/fine-art-prints/Annibale-Carracci/842/Paysage-fluvial.html
- https://www.wikiart.org/fr/annibale-carracci/crucifixion-1583
  - https://www.meisterdrucke.fr/fine-art-prints/Annibale-Carracci/33345/Crucifixion.html
- https://www.wikiart.org/fr/annibale-carracci/the-laughing-youth-1583
  - https://www.meisterdrucke.fr/fine-art-prints/Annibale-Carracci/33351/Homme-qui-rit.html
- https://www.wikiart.org/fr/annibale-carracci/self-portrait
  - https://www.meisterdrucke.fr/fine-art-prints/Annibale-Carracci/164309/Autoportrait-mont%C3%A9-sur-un-chevalet,-1605.html
- https://www.wikiart.org/fr/annibale-carracci/la-boucherie-1580
  - https://www.meisterdrucke.fr/fine-art-prints/Annibale-Carracci/1088574/Boucherie.html

## Antonietta Brandeis (1848-1926)

- https://www.wikiart.org/fr/antonietta-brandeis
- https://fr.wikipedia.org/wiki/Antonietta_Brandeis
- https://www.meisterdrucke.fr/artiste/Antonietta-Brandeis.html
- https://www.wikiart.org/fr/antonietta-brandeis/seufzerbreck-zu-venedeg-1910
  - https://www.meisterdrucke.fr/fine-art-prints/Antonietta-Brandeis/1315614/.html
- https://www.wikiart.org/fr/antonietta-brandeis/battistero-di-san-marco-1910
- https://www.wikiart.org/fr/antonietta-brandeis/canal-in-venice-with-view-of-the-back-of-the-palazzo-rocca-1926
  
## Arthur Wardle (1864�1949)

- https://en.wikipedia.org/wiki/Arthur_Wardle

## Bartolom� Esteban Murillo (1617-1682)

- https://www.wikiart.org/fr/bartolome-esteban-murillo
- https://fr.wikipedia.org/wiki/Bartolom%C3%A9_Esteban_Murillo
- https://www.wikiart.org/fr/bartolome-esteban-murillo/les-mangeurs-de-melon-et-de-raisin-1646
- https://www.wikiart.org/fr/bartolome-esteban-murillo/christ-the-good-shepherd
- https://www.wikiart.org/fr/bartolome-esteban-murillo/santa-rufina-1665
- https://www.wikiart.org/fr/bartolome-esteban-murillo/limmaculee-conception-1678

## Bertalan Sz�kely (1835-1910)

- https://www.wikiart.org/fr/bertalan-szekely
- https://www.wikiart.org/fr/bertalan-szekely/women-of-eger-1867
- https://www.wikiart.org/fr/bertalan-szekely/discovery-of-the-body-of-king-louis-the-second-1860
- https://www.wikiart.org/fr/bertalan-szekely/zrinyi-s-charge-from-the-fortress-of-szigetv-r-1885
- https://www.wikiart.org/fr/bertalan-szekely/femme-japonaise-1903
- https://www.wikiart.org/fr/bertalan-szekely/the-battle-of-moh-cs-1526-1866

## Caspar David Friedrich (1774-1840)

- https://www.wikiart.org/fr/caspar-david-friedrich

## Charles Marion Russell (1864-1926)

- https://www.wikiart.org/fr/charles-marion-russell
- https://fr.wikipedia.org/wiki/Charles_Marion_Russell
- https://www.wikiart.org/fr/charles-marion-russell/in-without-knocking-1909
  - https://www.meisterdrucke.fr/fine-art-prints/Charles-Marion-Russell/352468/En-Sans-Frapper.html
- https://www.wikiart.org/fr/charles-marion-russell/mandan-warrior-1906
- https://www.wikiart.org/fr/charles-marion-russell/the-beauty-parlor-1907
- https://www.wikiart.org/fr/charles-marion-russell/a-cree-indian-1905
  - https://www.meisterdrucke.fr/fine-art-prints/Charles-Marion-Russell/347627/Indien-Cri.html
- https://www.wikiart.org/fr/charles-marion-russell/free-trappers-1911
  
## Claude Gell�e (~1600-1682)

- https://www.wikiart.org/fr/claude-gellee

## Constantin Makovski (1839-1915)

- https://fr.wikipedia.org/wiki/Constantin_Makovski
- https://www.wikiart.org/fr/constantin-makovski
- https://www.wikiart.org/fr/constantin-makovski/allegorical-scene
  - https://www.meisterdrucke.fr/fine-art-prints/Konstantin-Egorovich-Makovsky/351023/Sc%C3%A8ne-all%C3%A9gorique.html
- https://www.wikiart.org/fr/constantin-makovski/ophelia
- https://www.wikiart.org/fr/constantin-makovski/kissing-ceremony
- https://www.wikiart.org/fr/constantin-makovski/boris-morozov-and-ivan-the-terrible
- https://www.meisterdrucke.fr/fine-art-prints/Konstantin-Egorovich-Makovsky/1315773/La-Nourriture-de-l'Amour-(huile-sur-toile).html
- https://www.meisterdrucke.fr/fine-art-prints/Konstantin-Egorovich-Makovsky/788147/Ivan-Susanin.html

## David Revoy (1981)

- https://fr.wikipedia.org/wiki/David_Revoy
- https://www.davidrevoy.com/
- https://www.davidrevoy.com/article872/sunday-painting-to-relax
- https://www.davidrevoy.com/article353/run
- https://www.davidrevoy.com/article351/a-witch-of-magmah
- https://www.davidrevoy.com/article772/mysterious
- https://www.davidrevoy.com/article786/the-main-temple-of-ah
- https://www.davidrevoy.com/article897/in-bloom
- https://www.davidrevoy.com/article871/brushwork-study-202110

## Domenico Ghirlandaio (1449-1494)

- https://www.wikiart.org/fr/domenico-ghirlandaio

## Edward Robert Hughes (1851-1914)

- https://www.wikiart.org/fr/edward-robert-hughes
- https://fr.wikipedia.org/wiki/Edward_Robert_Hughes
- https://www.wikiart.org/fr/edward-robert-hughes/the-valkyries-vigil
- https://www.wikiart.org/fr/edward-robert-hughes/midsummer-eve-1908
- https://www.wikiart.org/fr/edward-robert-hughes/he-lady-of-shalott-1905
- https://www.wikiart.org/fr/edward-robert-hughes/blondels-quest
- https://www.wikiart.org/fr/edward-robert-hughes/oh-whats-that-in-the-hollow

## Edwin Austin Abbey (1852-1911)

## �lisabeth Vig�e Le Brun (1755-1842)

- https://www.wikiart.org/fr/elisabeth-vigee-le-brun
- https://fr.wikipedia.org/wiki/%C3%89lisabeth_Vig%C3%A9e_Le_Brun
- https://www.wikiart.org/fr/elisabeth-vigee-le-brun/autoportrait-1781
x https://www.wikiart.org/fr/elisabeth-vigee-le-brun/st-genevieve-1821
- https://www.wikiart.org/fr/elisabeth-vigee-le-brun/portrait-of-a-young-woman
  - https://www.meisterdrucke.fr/fine-art-prints/Elisabeth-Louise-Vigee-Lebrun/727315/Portrait-de-la-princesse-Irina-Ivanovna-Vorontsova,-n%C3%A9e-Izmaylova-1768-1848,-ca-1797.html
- https://www.meisterdrucke.fr/fine-art-prints/Elisabeth-Louise-Vigee-Lebrun/846899/Julie-Le-Brun-1780-1819-se-regardant-dans-un-miroir,-1787..html
- https://www.meisterdrucke.fr/fine-art-prints/Elisabeth-Louise-Vigee-Lebrun/819926/Portrait-de-la-fille-de-l&39;artiste,-assis-aux-trois-quarts,-jouant-de-la-guitare,-c.1797.html
- https://www.wikiart.org/fr/elisabeth-vigee-le-brun/portrait-of-anna-pitt-as-hebe-1792

## Fede Galizia (1578-1630)

- https://fr.wikipedia.org/wiki/Fede_Galizia
- https://www.wikiart.org/fr/fede-galizia
- https://www.meisterdrucke.fr/search/Fede%20Galizia.html
- https://www.wikiart.org/fr/fede-galizia/judith-with-the-head-of-holofernes-1596
  - https://www.meisterdrucke.fr/fine-art-prints/Fede-Galizia/1199020/.html

## Francesco Solimena (1657-1747)

- https://www.wikiart.org/fr/francesco-solimena
- https://www.wikiart.org/fr/francesco-solimena/st-bonaventura-receiving-the-banner-of-st-sepulchre-from-the-madonna 
  - https://www.meisterdrucke.fr/fine-art-prints/Francesco-Solimena/1087974/Saint-Bonaventure-recevant-de-la-Madone-la-banni%C3%A8re-du-Saint-S%C3%A9pulcre,-par-Francesco-Solimena-(1657-1747),-Cath%C3%A9drale-de-San-Paolo,-Aversa,-Campanie,-Italie.html
- https://www.wikiart.org/fr/francesco-solimena/allegory-of-a-reign-1690
  - https://www.meisterdrucke.fr/fine-art-prints/Francesco-Solimena/56756/All%C3%A9gorie-d&39;un-r%C3%A8gne,-1690.html
- https://www.wikiart.org/fr/francesco-solimena/self-portrait
  - https://www.meisterdrucke.fr/fine-art-prints/Francesco-Solimena/322419/Autoportrait,-1730-31.html

## Fran�ois Boucher (1703-1770)

- https://www.wikiart.org/fr/francois-boucher

## Eug�ne Delacroix ()

- https://www.wikiart.org/fr/eugene-delacroix/la-liberte-guidant-le-peuple-1830
  - https://www.meisterdrucke.fr/fine-art-prints/Ferdinand-Victor-Eugene-Delacroix/28155/Le-28-Juillet.-La-Libert%C3%A9-guidant-le-peuple.html
- https://www.wikiart.org/fr/eugene-delacroix/noce-juive-dans-le-maroc-1841
  - https://www.meisterdrucke.fr/kunstdrucke/Ferdinand-Victor-Eugene-Delacroix/37685/Eine-j%C3%BCdische-Hochzeit-in-Marokko,-1841.html
- https://www.wikiart.org/fr/eugene-delacroix/la-mort-de-sardanapale-1827
  - https://www.meisterdrucke.fr/kunstdrucke/Ferdinand-Victor-Eugene-Delacroix/899698/Der-Tod-von-Sardanapalus.html
- https://www.wikiart.org/fr/eugene-delacroix/muley-abd-ar-rhaman-the-sultan-of-morocco-leaving-his-palace-of-meknes-with-his-entourage-1845
  - https://www.meisterdrucke.fr/fine-art-prints/Ferdinand-Victor-Eugene-Delacroix/95905/Muley-Abd-ar-Rhaman-(1789-1859),-Le-Sultan-du-Maroc,-quittant-son-palais-de-Mekn%C3%A8s-avec-son-entourage,-mars-1832,-1845.html
- https://www.wikiart.org/fr/eugene-delacroix/self-portrait
  - https://www.meisterdrucke.fr/fine-art-prints/Ferdinand-Victor-Eugene-Delacroix/787182/Autoportrait.html
- https://www.wikiart.org/fr/eugene-delacroix/vase-of-flowers-on-a-console-1849
  - https://www.meisterdrucke.fr/fine-art-prints/Ferdinand-Victor-Eugene-Delacroix/28120/Un-vase-de-fleurs-sur-une-console.html  
- https://www.wikiart.org/fr/hans-von-aachen/allegorie-de-la-paix-et-de-labondance-1602
- https://www.wikiart.org/fr/hans-von-aachen/matthias-holy-roman-emperor-as-king-of-bohemia-1612
- https://www.wikiart.org/fr/hans-von-aachen/cinq-allegories-des-guerres-turques-conquete-de-conquest-of-stuhlweissenburg-1604
  - https://www.meisterdrucke.fr/fine-art-prints/Hans-von-Aachen/101144/All%C3%A9gorie-des-guerres-turques:-La-prise-de-Stuhlweissenburg,-1603-4.html
- https://www.wikiart.org/fr/hans-von-aachen/bacchus-venus-et-cupidon-1595
  - https://www.meisterdrucke.fr/fine-art-prints/Hans-von-Aachen/80218/Bacchus,-V%C3%A9nus-et-Cupidon.html

## George Lambert (1700-1765)

- https://fr.wikipedia.org/wiki/George_Lambert_(peintre_anglais)
- https://www.wikiart.org/fr/george-lambert
- https://www.wikiart.org/fr/george-lambert/pastoral-scene-with-a-peasant-family-1753
- https://www.wikiart.org/fr/george-lambert/a-river-landscape-flanked-by-trees-1748
- https://www.wikiart.org/fr/george-lambert/ruins-of-leybourne-castle-kent-from-the-south-west-1737
- https://www.wikiart.org/fr/george-lambert/stormy-seashore-with-ruined-temple-shipwreck-and-figures-1747

## Giotto di Bondone (1266-1337)

- https://fr.wikipedia.org/wiki/Giotto_di_Bondone
- https://www.wikiart.org/fr/giotto-di-bondone
- https://www.wikiart.org/fr/giotto-di-bondone/injustice-1306
- https://www.wikiart.org/fr/giotto-di-bondone/infidelity-1306
- https://www.wikiart.org/fr/giotto-di-bondone/envy-1306
- https://www.wikiart.org/fr/giotto-di-bondone/faith
- https://www.wikiart.org/fr/giotto-di-bondone/hope
- https://www.wikiart.org/fr/giotto-di-bondone/temperance
- https://www.wikiart.org/fr/giotto-di-bondone/justice

## Gwenn Seemel (1981)

- https://gwennseemel.com/fr/
- https://en.wikipedia.org/wiki/Gwenn_Seemel
- https://gwennseemel.com/artwork/everythings-fine/practicing-death/
- https://gwennseemel.com/artwork/2009/gwenn-messy/
- https://gwennseemel.com/fr/oeuvres/2014/rouzic/
- https://gwennseemel.com/fr/oeuvres/2022/lapin/
- https://gwennseemel.com/fr/oeuvres/2021/serpentcolombe/

## Hans Hoffmann (1530-1591)

- https://www.wikiart.org/fr/hans-hoffmann
- https://fr.wikipedia.org/wiki/Hans_Hoffmann
- https://www.wikiart.org/fr/hans-hoffmann/lievre-entoure-de-plantes
  - https://www.meisterdrucke.fr/fine-art-prints/Hans-Hoffmann/164460/Li%C3%A8vre.html
- https://www.wikiart.org/fr/hans-hoffmann/squirrel
  - https://www.meisterdrucke.fr/fine-art-prints/Hans-Hoffmann/11585/%C3%89cureuil-roux.html
- https://www.wikiart.org/fr/hans-hoffmann/a-young-hare-after-durer

## Hans Memling (1430-1494)

- https://www.wikiart.org/fr/hans-memling
- https://fr.wikipedia.org/wiki/Hans_Memling
- https://www.wikiart.org/fr/hans-memling/allegory-with-a-virgin-1480
- https://www.wikiart.org/fr/hans-memling/still-life-with-a-jug-with-flowers-the-reverse-side-of-the-portrait-of-a-praying-man
- https://www.wikiart.org/fr/hans-memling/st-john-and-veronica-diptych-reverse-of-the-right-wing
- https://www.wikiart.org/fr/hans-memling/st-john-and-veronica-diptych-reverse-of-the-left-wing
- https://www.wikiart.org/fr/hans-memling/hell
- https://www.wikiart.org/fr/hans-memling/scenes-from-the-passion-of-christ-left-side-1471

## Hans von Aachen (1552-1615)

- https://fr.wikipedia.org/wiki/Hans_von_Aachen
- https://www.wikiart.org/fr/hans-von-aachen
- https://www.wikiart.org/fr/hans-von-aachen/cinq-allegories-des-guerres-turques-declaration-de-guerre-devant-constantinople-1604
  - https://www.meisterdrucke.fr/fine-art-prints/Hans-von-Aachen/109592/All%C3%A9gorie-des-guerres-turques:-La-d%C3%A9claration-de-guerre-%C3%A0-Constantinople,-1603-4.html
- https://www.wikiart.org/fr/hans-von-aachen/cinq-allegories-des-guerres-turques-bataille-de-hermannstadt-1604
  - https://www.meisterdrucke.fr/fine-art-prints/Hans-von-Aachen/53391/All%C3%A9gorie-des-guerres-turques:-la-bataille-d&39;hermannstadt.html
  
## Hatip Mehmed Efendi (~1680-1773)

- https://www.wikiart.org/fr/hatip-mehmed-efendi

## Henryk Siemiradzki (1843-1902)

- https://www.wikiart.org/fr/henryk-siemiradzki
- https://fr.wikipedia.org/wiki/Henryk_Siemiradzki
- https://www.wikiart.org/fr/henryk-siemiradzki/naiads
  - https://www.artrenewal.org/secureimages/artwork/498/498/78435/eabcf8f4-13e2-48dd-9c90-61a6112a443f..jpg?mode=crop&format=jpg&maxwidth=1000&maxheight=1000
- https://www.wikiart.org/fr/henryk-siemiradzki/scene-from-roman-life
- https://www.wikiart.org/fr/henryk-siemiradzki/joan-of-arc-kneeling-before-angel
  - https://3.bp.blogspot.com/-xyMTPQ59-EQ/Un_f1SR8ZLI/AAAAAAABsbI/2unoJXQLE6E/s1600/H0046-L03684799.jpg
- https://www.wikiart.org/fr/henryk-siemiradzki/prince-alexander-nevsky-receiving-papal-legates
- https://www.wikiart.org/fr/henryk-siemiradzki/1280px-siemiradzki-curtain-design-for-the-slowacki-theatre-1894

## Hokusai (1760-1849)

- https://www.wikiart.org/fr/hokusai
- https://fr.wikipedia.org/wiki/Hokusai
- https://www.meisterdrucke.fr/search/Hokusai.html
- https://www.wikiart.org/fr/hokusai/la-grande-vague-de-kanagawa-1831
- https://www.wikiart.org/fr/hokusai/a-merchant-making-up-the-account
- https://www.wikiart.org/fr/hokusai/oiran-and-kamuro
  - https://www.meisterdrucke.fr/fine-art-prints/Katsushika-Hokusai/32311/Oiran-et-Kamuro.html
- https://www.meisterdrucke.fr/fine-art-prints/Katsushika-Hokusai/784469/Le-dragon-parmi-les-nuages,-1849..html
  
## Horace Vernet (1789-1863)

- https://www.wikiart.org/fr/horace-vernet

## Ito Jakuchu (1716-1800)

- https://www.wikiart.org/fr/ito-jakuchu

## Jacques-Louis David (1748-1825)

- https://www.wikiart.org/fr/jacques-louis-david
- https://www.wikiart.org/fr/jacques-louis-david/bonaparte-franchissant-le-grand-saint-bernard-1801
  - https://www.meisterdrucke.fr/fine-art-prints/Jacques-Louis-David/696004/Napol%C3%A9on-traversant-les-Alpes.html
- https://www.wikiart.org/fr/jacques-louis-david/the-tennis-court-oath
  - https://www.meisterdrucke.fr/fine-art-prints/Jacques-Louis-David/45045/Le-serment-de-la-cour-de-tennis,-20-juin-1789,-1791.html
- https://www.wikiart.org/fr/jacques-louis-david/le-sacre-de-napoleon-1807
  - https://www.meisterdrucke.fr/fine-art-prints/Jacques-Louis-David/27909/Le-couronnement-de-Napol%C3%A9on.html
- https://www.wikiart.org/fr/jacques-louis-david/autoportrait-1794
- https://www.wikiart.org/fr/jacques-louis-david/la-douleur-dandromaque-1783
  - https://www.meisterdrucke.fr/fine-art-prints/Jacques-Louis-David/779942/Andromaque-pleure-Hector,-1783..html
- https://www.meisterdrucke.fr/fine-art-prints/Jacques-Louis-David/13952/Marat-assassin%C3%A9.html
 
## Jan Brueghel l'Ancien (1568-1625)

- https://www.wikiart.org/fr/jan-brueghel-lancien
- https://fr.wikipedia.org/wiki/Jan_Brueghel_l'Ancien
- https://www.wikiart.org/fr/jan-brueghel-lancien/landscape-with-diana-and-actaeon
- https://www.wikiart.org/fr/jan-brueghel-lancien/allegory-of-sight-and-smell-1620
- https://www.wikiart.org/fr/jan-brueghel-lancien/allegory-of-water-1614
- https://www.wikiart.org/fr/jan-brueghel-lancien/vase-of-flowers-1625
- https://commons.wikimedia.org/wiki/File:Brueghel_lair.jpg
- https://www.wikiart.org/fr/jan-brueghel-lancien/allegory-of-earth
  - https://commons.wikimedia.org/wiki/File:Brueghel_laterre.jpg

## Johan August Malmstr�m (1829�1901) 

## John Collier (1850-1934)

- https://www.wikiart.org/fr/john-collier
- https://fr.wikipedia.org/wiki/John_Collier
- https://www.meisterdrucke.fr/artiste/John-Collier.html
- https://www.wikiart.org/fr/john-collier/priestess-of-delphi-1891
  - https://www.meisterdrucke.fr/fine-art-prints/John-Collier/14040/Pr%C3%AAtresse-de-Delphes.html
- https://www.wikiart.org/fr/john-collier/lady-godiva-1897
  - https://www.meisterdrucke.fr/fine-art-prints/John-Collier/549343/Lady-Godiva.html
- https://www.wikiart.org/fr/john-collier/the-laboratory-1895
  - https://www.meisterdrucke.fr/fine-art-prints/John-Collier/212602/Le-laboratoire,-1895.html

## John Singer Sargent (1856-1925)

- https://fr.wikipedia.org/wiki/John_Singer_Sargent

## Jos� Ferraz de Almeida J�nior (1850-1899)

- https://www.wikiart.org/fr/jose-ferraz-de-almeida-junior
- https://fr.wikipedia.org/wiki/Jos%C3%A9_Ferraz_de_Almeida_J%C3%BAnior
- https://www.wikiart.org/fr/jose-ferraz-de-almeida-junior/la-chute-deau-de-votorantim-1893

## Joseph Ducreux (1735-1802)

- https://www.wikiart.org/fr/joseph-ducreux
- https://fr.wikipedia.org/wiki/Joseph_Ducreux
- https://www.meisterdrucke.fr/artiste/Joseph-Ducreux.html
- https://www.wikiart.org/fr/joseph-ducreux/self-portrait-1793
  - https://www.meisterdrucke.fr/fine-art-prints/Joseph-Ducreux/1204086/.html
- https://www.wikiart.org/fr/joseph-ducreux/self-portrait-1793
  - https://www.meisterdrucke.fr/fine-art-prints/Joseph-Ducreux/14333/Autoportrait,-b%C3%A2illement.html

## Juan Bautista Ma�no (1581-1649)

- https://fr.wikipedia.org/wiki/Juan_Bautista_Ma%C3%ADno
- https://www.wikiart.org/fr/juan-bautista-maino
- https://www.wikiart.org/fr/juan-bautista-maino/les-larmes-de-saint-pierre-1610
- https://www.wikiart.org/fr/juan-bautista-maino/the-conversion-of-saint-paul-1614
- https://www.wikiart.org/fr/juan-bautista-maino/magdalena-penitente-1615
https://www.wikiart.org/fr/juan-bautista-maino/friar-alonso-de-sant-tomas-1649
  
## Lavinia Fontana (1552-1614)

- https://www.wikiart.org/fr/lavinia-fontana
- https://www.meisterdrucke.fr/artiste/Lavinia-Fontana.html
- https://www.wikiart.org/fr/lavinia-fontana/assumption-of-the-virgin-with-saints-peter-chrysologus-and-cassian-1584
- https://www.wikiart.org/fr/lavinia-fontana/self-portrait-at-the-clavichord-with-a-servant-1577
  - https://www.meisterdrucke.fr/fine-art-prints/Lavinia-Fontana/292201/Autoportrait-au-Spinet,-1578.html
- https://www.wikiart.org/fr/lavinia-fontana/minerva-dressing-1613
  - https://www.meisterdrucke.fr/fine-art-prints/Lavinia-Fontana/1199205/.html
- https://www.wikiart.org/fr/lavinia-fontana/portrait-of-gerolamo-mercuriale-1589
  
## L�onard de Vinci (1452-1519)

- https://www.wikiart.org/fr/leonard-de-vinci
- https://www.wikiart.org/fr/leonard-de-vinci/self-portrait-1505
- https://www.wikiart.org/fr/leonard-de-vinci/la-joconde-1504
- https://www.wikiart.org/fr/leonard-de-vinci/leda-and-the-swan-1

## Leysan

- https://www.deviantart.com/leysan

## Li Cheng (919-967)

- https://www.wikiart.org/fr/li-cheng

## Lilla Cabot Perry (1848-1933)

- https://www.wikiart.org/fr/lilla-cabot-perry
- https://fr.wikipedia.org/wiki/Lilla_Cabot_Perry
- https://www.wikiart.org/fr/lilla-cabot-perry/a-fairy-tale-1912
- https://www.wikiart.org/fr/lilla-cabot-perry/the-pearl-1913
- https://www.wikiart.org/fr/lilla-cabot-perry/portrait-of-a-young-girl-with-an-orange-1901
- https://www.wikiart.org/fr/lilla-cabot-perry/the-violoncellist-1907
- https://www.wikiart.org/fr/lilla-cabot-perry/in-a-japanese-garden-1901

## Lorenzo Lotto (~1480-1556)

- https://www.wikiart.org/fr/lorenzo-lotto

## Lucas Cranach l'Ancien (1472-1553)

- https://www.meisterdrucke.fr/artiste/Lucas-Cranach-the-Elder.html
- https://fr.wikipedia.org/wiki/Lucas_Cranach_l%27Ancien
- https://www.wikiart.org/fr/lucas-cranach-lancien
- https://www.wikiart.org/fr/lucas-cranach-lancien/the-crucifixion-1503
  - https://www.meisterdrucke.fr/fine-art-prints/Lucas-Cranach-the-Elder/756160/La-crucifixion,-1501.html
- https://www.meisterdrucke.fr/fine-art-prints/Lucas-Cranach-the-Elder/29782/Lucr%C3%A8ce.html

## Luis Paret y Alc�zar (1746-1799)

- https://www.wikiart.org/fr/luis-paret-y-alcazar
- https://fr.wikipedia.org/wiki/Luis_Paret_y_Alc%C3%A1zar
- https://www.wikiart.org/fr/luis-paret-y-alcazar/jura-de-fernando-vii-como-pr-ncipe-de-asturias-1791
- https://www.wikiart.org/fr/luis-paret-y-alcazar/self-portrait-in-the-studio-1780
- https://www.wikiart.org/fr/luis-paret-y-alcazar/village-scene-1786
- https://www.wikiart.org/fr/luis-paret-y-alcazar/la-carta-1772
- https://www.wikiart.org/fr/luis-paret-y-alcazar/still-life-with-fruit
- https://www.wikiart.org/fr/luis-paret-y-alcazar/charles-iii-dining-before-the-court-1775

## Marcello Bacciarelli (1731-1818)

- https://www.wikiart.org/fr/marcello-bacciarelli
- https://fr.wikipedia.org/wiki/Marcello_Bacciarelli
- https://www.wikiart.org/fr/marcello-bacciarelli/sobieski-at-the-battle-of-vienna-1796
- https://www.wikiart.org/fr/marcello-bacciarelli/anna-lampel-1801
- https://www.wikiart.org/fr/marcello-bacciarelli/union-of-lublin-1796
- https://www.wikiart.org/fr/marcello-bacciarelli/self-portrait-in-polish-national-costume

## Motonobu Kano (1476-1559)

- https://www.wikiart.org/fr/kano-motonobu
- https://fr.wikipedia.org/wiki/Kan%C5%8D_Motonobu
- https://www.wikiart.org/fr/kano-motonobu/flowers-and-birds-of-the-four-seasons-1513
- https://www.wikiart.org/fr/kano-motonobu/flowers-and-birds-of-the-four-seasons-1513-0
- https://www.meisterdrucke.fr/fine-art-prints/attributed-to-Kano-Motonobu/1307137/Fleurs-et-oiseaux-dans-un-paysage-printanier.html
- https://www.wikiart.org/fr/kano-motonobu/painting-on-zen-enlightenment-sanping-baring-his-chest-and-shigong-stretching-his-bow-0

## Mustafa Rakim (1757-1826)

- https://www.wikiart.org/fr/mustafa-rakim

## Otto Marseus van Schrieck (1613-1678)

- https://www.wikiart.org/fr/otto-marseus-van-schrieck
- https://fr.wikipedia.org/wiki/Otto_Marseus_van_Schrieck
- https://www.wikiart.org/fr/otto-marseus-van-schrieck/a-forest-floor-still-life-with-various-fungi-thistles-an-aspic-viper-etc-1660
- https://www.wikiart.org/fr/otto-marseus-van-schrieck/still-life-with-snakes-frogs-mushrooms-flowers-and-butterflies-1662
- https://www.wikiart.org/fr/otto-marseus-van-schrieck/plants-frogs-butterflies-and-a-snake-on-a-forest-ground-1670
- https://www.wikiart.org/fr/otto-marseus-van-schrieck/still-life-with-thistle-1662
- https://www.wikiart.org/fr/otto-marseus-van-schrieck/plants-and-insects-1665

## Pedro Am�rico (1843-1905)

- https://www.wikiart.org/fr/pedro-americo
- https://fr.wikipedia.org/wiki/Pedro_Am%C3%A9rico
- https://www.wikiart.org/fr/pedro-americo/the-night-escorted-by-the-geniuses-of-study-and-love-1885
- https://www.wikiart.org/fr/pedro-americo/arab-fiddler-1884
- https://www.wikiart.org/fr/pedro-americo/tiradentes-esquartejado-1893
- https://www.wikiart.org/fr/pedro-americo/david-and-abisag-1879
- https://fineartamerica.com/featured/heloises-vow-1880-pedro-americo.html

## Qian Xuan (1235-1305)

- https://www.wikiart.org/fr/qian-xuan

## Ravi Varm� (1848-1906)

- https://www.wikiart.org/fr/ravi-varma

## Rodolfo Amoedo (1857-1941)

- https://www.wikiart.org/fr/rodolfo-amoedo
- https://fr.wikipedia.org/wiki/Rodolfo_Amoedo
- https://www.fineartphotographyvideoart.com/2019/03/Rodolfo-Amoedo-Brasilian-painter.html
- https://www.wikiart.org/fr/rodolfo-amoedo/self-portrait-1921
- https://www.wikiart.org/fr/rodolfo-amoedo/the-departure-of-jacob-1884
- https://www.wikiart.org/fr/rodolfo-amoedo/the-cicle-of-the-fold-1893

## Sesshu (1420-1506)

- https://fr.wikipedia.org/wiki/Sessh%C5%AB
- https://www.wikiart.org/fr/sesshu
- https://www.meisterdrucke.fr/artiste/Toyo-Sesshu.html

## Shitao (1642-1707)

- https://www.wikiart.org/fr/shitao

## Sophie Gengembre Anderson (1823-1903)

- https://www.wikiart.org/fr/sophie-gengembre-anderson
- https://fr.wikipedia.org/wiki/Sophie_Gengembre_Anderson
- https://www.wikiart.org/fr/sophie-gengembre-anderson/foundling-girls-at-prayer-in-the-chapel-1877
- https://www.wikiart.org/fr/sophie-gengembre-anderson/the-song-of-the-lark-1903
- https://www.wikiart.org/fr/sophie-gengembre-anderson/the-song-1881
- https://www.wikiart.org/fr/sophie-gengembre-anderson/the-turtle-dove-small

## Uemura Tsune (1875-1949)

- https://www.wikiart.org/fr/uemura-shoen
- https://fr.wikipedia.org/wiki/Sh%C5%8Den_Uemura
- https://www.wikiart.org/fr/uemura-shoen/daughter-miyuki-1914
- https://www.wikiart.org/fr/uemura-shoen/springtime-of-life-1899
- https://www.wikiart.org/fr/uemura-shoen/yang-gui-fei-1922

## Utagawa Sadatora (~1825-?)

- https://www.wikiart.org/fr/utagawa-sadatora

## Viktor Vasnetsov (1848-1926)

- https://fr.wikipedia.org/wiki/Viktor_Vasnetsov
- https://www.wikiart.org/fr/viktor-vasnetsov
- https://www.meisterdrucke.fr/fine-art-prints/Viktor-Mikhaylovich-Vasnetsov/743708/Pimen.-Illustration-au-drame-Boris-Godunov-de-A.-Pushkin.html
- https://www.wikiart.org/fr/viktor-vasnetsov/the-unsmiling-tsarevna-1926
  - https://www.meisterdrucke.fr/fine-art-prints/Viktor-Mikhaylovich-Vasnetsov/764766/La-princesse-qui-n&39;a-jamais-souri-Nesmeyana,-1914-1916.html
- https://www.wikiart.org/fr/viktor-vasnetsov/la-princesse-grenouille-1918
  - https://www.meisterdrucke.fr/fine-art-prints/Viktor-Mikhaylovich-Vasnetsov/717736/La-princesse-grenouille,-1901-1918.html
- https://www.wikiart.org/fr/viktor-vasnetsov/kochtchei-limmortel-1917
  - https://www.meisterdrucke.fr/fine-art-prints/Viktor-Mikhaylovich-Vasnetsov/796012/Koschei-l&39;Immortel,-1917-1926.html
- https://www.wikiart.org/fr/viktor-vasnetsov/fight-dobrynya-nikitich-with-seven-headed-serpent-hydra-1918

## William Dobson (1610-1646)

- https://www.wikiart.org/fr/william-dobson
- https://fr.wikipedia.org/wiki/William_Dobson
- https://www.wikiart.org/fr/william-dobson/the-executioner-with-the-head-of-john-the-baptist-1643
  - https://www.meisterdrucke.fr/fine-art-prints/William-Dobson/6925/L'ex%C3%A9cuteur-avec-le-chef-de-Jean-Baptiste.html
- https://www.wikiart.org/fr/william-dobson/self-portrait
  - https://www.mutualart.com/Artwork/Portrait-of-the-artist--bust-length--in-/D37ABB7FE546D668

## William Bouguereau (1825�1905)

- https://fr.wikipedia.org/wiki/William_Bouguereau
- https://fr.wikipedia.org/wiki/%C5%92uvre_peint_de_Bouguereau
- https://www.wikiart.org/fr/william-bouguereau/nymphes-et-satyre-1873
  - https://www.meisterdrucke.fr/fine-art-prints/William-Adolphe-Bouguereau/27692/Nymphes-et-Satyre.html
- https://www.wikiart.org/fr/william-bouguereau/the-virgin-with-angels-1881
  - https://www.meisterdrucke.fr/fine-art-prints/William-Adolphe-Bouguereau/770159/Le-chant-des-anges,-1881.html
- https://www.wikiart.org/fr/william-bouguereau/italian-boy-with-mandolin
  - https://www.meisterdrucke.fr/fine-art-prints/William-Adolphe-Bouguereau/838116/Italien-avec-une-mandoline,-1870.html
- https://www.wikiart.org/fr/william-bouguereau/portrait-of-the-artist-1879
- https://www.wikiart.org/fr/william-bouguereau/la-gardeuse-doies-1891
- https://www.wikiart.org/fr/william-bouguereau/la-vierge-aux-anges-1900
- https://www.wikiart.org/fr/william-bouguereau/the-bird-ch-ri-1867
