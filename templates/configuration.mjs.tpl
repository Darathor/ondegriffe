export let Configuration = {
	// PeerJS server configuration.
	peerjs: {
		host: '%%PEERJS_HOST%%',
		port: %%PEERJS_PORT%%
	}
}