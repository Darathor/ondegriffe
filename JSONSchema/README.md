# Setup in PHP Storm

Go to *File > Settings > Languages & Frameworks > Schemas and DTDs > JSON Schema Mappings* and add a mapping with:
* "Schema file or URL" = "JSONSchema\extension.schema.json"
* "Schema version" = "JSON Schema version 7"
* "File path pattern" = "extension.json"